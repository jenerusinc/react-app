import d, * as e from '@react-navigation/drawer';

// Export the default
export default d;

// Cutsom creator
function createDrawerNavigator( ...opts ) {

}

// Export everything else
export {
	...e,
	createDrawerNavigator
};
