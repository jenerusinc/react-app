import React from 'react';

import { Body, Header, Left, Right, Icon, Title, Button, Text } from "native-base";

import { createDrawerNavigator as createDrawerNavigatorOriginal } from '@react-navigation/drawer';
import { createStackNavigator as createStackNavigatorOriginal } from '@react-navigation/stack';

const debug = require( '../debug' )( "custom-navigation:index" );

/**
 * Our default header moduley
 */
export function DefaultHeader( { title, left, right, body, back, navigation } ) {
	//debug( "We have back?", back );
	
	// By default do the menu
	if( left==undefined ) {
		left = 
		  <Left>
		    {back ? 
		    <Button
		      transparent
		      onPress={()=>{
		      	debug( "Going back" );
		      	navigation.goBack();
			}}>
		      <Icon name="arrow-back" />
		    </Button>
		    :
		    <Button
		      transparent
		      onPress={()=>{
		      	debug( "Opening drawere" );
		      	navigation.openDrawer();
			}}>
		      <Icon name="menu" />
		    </Button>}
		  </Left>;
	}
	else
		left = <Left>{left}</Left>;

	if( right==undefined )
		right = <Right />
	else
		right = <Right>{right}</Right>

	if( body==undefined )
		body = <Body>{title ? <Title>{title}</Title> : null}</Body>;
	else
		body = <Body>{body}</Body>;

	return(
		<Header>
		  {left}
		  {body}
		  {right}
		</Header>
	);
}

export const headerScreenOptions = {
	'header' : ( { scene, previous, navigation } ) => {
	  const { options } = scene.descriptor;

	  //debug( "We have options", options );

	  const title =
	    options.headerTitle !== undefined
	      ? options.headerTitle
	      : options.title !== undefined
	      ? options.title
	      : scene.route.name;

	  const { headerLeft, headerRight } = options;

	  //debug( "We have previous", previous, headerLeft, headerRight );

	  return (
	    <DefaultHeader
	      title={title}
	      back={previous}
	      left={headerLeft}
	      right={headerRight}
	      style={options.headerStyle}
	    />
	  );
	}
};

export function createHeaderScreenOptions( opts ) {
	return( {
		...headerScreenOptions,
		...opts
	} );
}

/**
 * Custom function creator with custom classies
 */
export function wrapCreate( fn ) {
	/**
	 * REturn the thunk
	 */
	return( function( ...opts ) {
		// Call it to get our class
		const Stack = fn( ...opts );

		// Override some stack stuff
		const OriginalNavigation = Stack.OriginalNavigation = Stack.Navigation;
		const OriginalScreen = Stack.OriginalScreen = Stack.Screen;

		/**
		 * Our special navigation class
		 */
		Stack.Navigation = function( { screenOptions, ...others } ) {
			return(
				<OriginalNavigation headerMode="screen" screenOptions={createHeaderScreenOptions( screenOptions )} {...others} />
			);
		};

		/**
		 * Our special screen
		 */
		Stack.Screen = function( { title, noHeader, options, ...others } ) {
			// Do we have a title?
			if( title!=null ) {
				// Apend to the options
				options = { ...options, title };
			}

			// Do we have no header?
			if( noHeader )
				options = { ...options, 'headerShown' : false };

			return(
				<OriginalScreen options={options} {...others} />
			);
		};

		// Return the modified stack
		return( Stack );
	} );
}

/**
 * Export the defaults we'd use
 */
export const createDrawerNavigator = wrapCreate( createDrawerNavigatorOriginal );
export const createStackNavigator = wrapCreate( createDrawerNavigatorOriginal );

// Exort the originals
export { createDrawerNavigatorOriginal, createStackNavigatorOriginal };
