import React, { useState, useRef, useEffect } from 'react';

//import { Text, Button } from 'galio-framework';
import { View, Container, Content, Text, Button, Root, Header, StyleProvider, Icon } from 'native-base';

import BlackLogoNoBackground from './assets/pngs/black-logo-no-background.png';

import * as Analytics from 'expo-firebase-analytics';

import * as atoms from './atoms';
import { useRecoilValue } from 'recoil';

import { Image, SafeAreaView, StyleSheet } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import Constants from 'expo-constants';

import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';


import EnsureLoggedIn from './views/EnsureLoggedIn';
//import Shop from "./views/shop"
import Signup from "./views/signup"
import Story from "./views/story"
import Settings from "./views/Settings"
import ForgotPass from "./views/ForgotPass"
import EditStory from "./views/edit_story"
import SignOut from './views/SignOut';
import Home from './views/Home';
import Single from './views/SingleStory';
import Checkout from './views/Checkout';
//import Test from './Test';

import CustomPopupModal from './CustomPopupModal';

const styles = StyleSheet.create( {
	  'dialogText' : {
	  	'marginBottom' : 16,
		'alignSelf' : "stretch",
		'fontSize' : 18
	  },
	  'dialogInner' : {
		'marginLeft' : 16,
		'marginRight' : 16
	  }
} );

const Drawer = createDrawerNavigator();

// Get the current screen from the navigation state
function getActiveRouteName(navigationState) {
  if (!navigationState) return null;
  const route = navigationState.routes[navigationState.index];
  // Parse the nested navigators
  if (route.routes) return getActiveRouteName(route);
  return route.routeName;
}

/**
 * Functionally bingo
 */
function onNavigatinStateChange( prevState, currentState ) {
		const currentScreen = getActiveRouteName(currentState);
		const prevScreen = getActiveRouteName(prevState);

		if (prevScreen !== currentScreen) {
			// Update Firebase with the name of your screen
			Analytics.setCurrentScreen(currentScreen);
		}
}

/**
 * Our drawer renderer
 */
function drawerContent( props ) {
	return(

	<SafeAreaView style={{flex: 1}}>
          <Image
					resizeMode="contain"
            source={BlackLogoNoBackground}
            style={{
              height: 50,
							width : null,
							alignSelf : "stretch",
            }} />
	<DrawerContentScrollView>
	<DrawerItemList {...props} />
	</DrawerContentScrollView>
	<Text style={{ 'color' : "grey", 'fontSize' : 12, 'alignSelf' : "stretch", 'textAlign' : "center" }}>v{Constants.manifest.version}</Text>
	</SafeAreaView>

	);
}

/**
 * The control and model for checkout alerts
 */
function ShouldAlertAboutCheckout( { hasIssuedCard, navigation } ) {
	// Show the modal?
	const [ isCheckoutNotificationVisible, setIsCheckoutNotificationVisible ] = useState( false );
	
	// When this is true, that means we've already checked
	// for the issued card on mount. So we can otherwise use the popup.
	const alreadyCheckedForIssuedCard = useRef( false );
	// Did we already ask?
	const alreadyAsked = useRef( false );

	// Use effect here
	useEffect( ()=>{
		// Is this the initial check?
		if( !alreadyCheckedForIssuedCard.current ) {
			// We have now
			alreadyCheckedForIssuedCard.current = true;
			// No more
			return;
		}

		// Did we ask already?
		if( alreadyAsked.current )
			return;

		// Are we asking now?
		if( hasIssuedCard )
			alreadyAsked.current = true;

		// Is it true?
		setIsCheckoutNotificationVisible( hasIssuedCard );
	}, [ hasIssuedCard ] );

	// Naught if not visible
	if( !isCheckoutNotificationVisible )
		return( null ); // Don't bother
	
	// Get rid of us
	const hideUs = ()=>setIsCheckoutNotificationVisible( false );
	const onUseNow = ()=>{
		// Call this anyway
		hideUs();
		// Nervigate
		navigation.navigate( "Checkout" );
	};
	
	// The actual perpup
	return(
		<CustomPopupModal onCancel={hideUs} visible={true} innerStyle={styles.dialogInner}>
			<Button transparent onPress={hideUs} style={{ 'position' : "absolute", 'right' : 0, 'top' : 0 }}><Icon type="FontAwesome" name="times" /></Button>
			<Text style={styles.dialogText}>You have a Jenerus one-time credit card waiting for you.</Text>
			<Text style={styles.dialogText}>Would you like to use it now to receive your groceries?</Text>
			<Button block onPress={onUseNow}><Text>Use It Now</Text></Button>
		</CustomPopupModal>
	);
}

/**
 * Actual application
 */
export default function() {
	// Are we logged in?
	const isLoggedIn = useRecoilValue( atoms.IsLoggedIn );
	const hasAsk = useRecoilValue( atoms.HasAsk );
	const hasIssuedCard = useRecoilValue( atoms.HasIssuedCard );
	const navigationRef = useRef();

	// The intial route
	const initialRouteName = (function() {
		// Use this nifty inline function to do what we need deducing-wise
		if( hasIssuedCard )
			return( "Checkout" );
		else if( hasAsk )
			return( "Home" );

		// Lastly right to the story
		return( "Story" );
	})();

	return(
		<NavigationContainer ref={navigationRef} onStateChange={onNavigatinStateChange}>
			{/* SOme modals */}
			<ShouldAlertAboutCheckout hasIssuedCard={hasIssuedCard} navigation={navigationRef.current} />

			{/* <Header /> */}
			<Drawer.Navigator initialRouteName={initialRouteName} drawerContent={drawerContent}>
	   
				   {hasIssuedCard ? 
				   <Drawer.Screen
					name="Checkout"
					component={Checkout}
					options={{ title: 'Start Checkout' }}

					/> : null}

				{/* Don't show the home screen if we have no ask */}
				   {hasAsk ?
				   <Drawer.Screen
					   name="Home"
					   component={Home}
					   options={{ title: 'View Your Story' }}
				   /> : null}

				{hasAsk ? 
					<Drawer.Screen
						name="Story"
						component={Story}
						options={{ title: 'Revise Your Story' }}
					/>
					:
					<Drawer.Screen
						name="Story"
						component={Story}
						options={{ title: 'Tell Your Story' }}
					/>}
				{/*
				<Drawer.Screen
					name="Shop"
					component={Shop}
					options={{ title: 'Shop now' }}
				/>
				*/}

				{isLoggedIn ?
				<>


					<Drawer.Screen
						name="Setting"
						component={Settings}
						options={{ title: 'Settings' }}
					/>
					<Drawer.Screen
						name="SignOut"
						component={SignOut}
						options={{ title: 'Sign Out' }}
					/>

				</>
				: 
				<>
					<Drawer.Screen
						name="Forgot Password"
						component={ForgotPass}
						options={{ title: 'Reset Password' }}
					/>
					<Drawer.Screen
						name="Signin"
						component={EnsureLoggedIn}
						options={{ title: 'Sign In' }}
					/>

				</>
				}

				{/*
				<Drawer.Screen
					name="Test"
					component={Test}
					options={{ title: 'Show Me' }}
				/>
				*/}

			</Drawer.Navigator>
		</NavigationContainer>
  );
}
