import React, { useEffect } from 'react';

// Import Firebase
import * as Analytics from 'expo-firebase-analytics';

import { useRecoilValue } from 'recoil';
import { User } from './atoms';

const debug = require( './debug' )( "AnalyticsHelper" );

/**
 * Our helper
 */
export default function() {
	// Set the user
	const user = useRecoilValue( User );


	// Update user
	const updateUser = async u=>{
		try {
			// The values
			let username=null;
			let name=null;
			let id=null;

			// The user
			if( u ) {
				// Shorthand these
				( {
					username,
					name,
					id
				} = u );
			}

			debug( "Registering analytics user", username, name, id );

			// Wait for it
			await Promise.all( [
				Analytics.setUserId( id ),
				Analytics.setUserProperty( "username", username ),
				Analytics.setUserProperty( "name", name )
			] );
		}
		catch( e ) {
			debug( "Update user helper", e );
		}
	};

	/**
	 * When these things change
	 */
	useEffect( ()=>{
		// Update the user async
		updateUser( user );
	}, [ user ] );

	return( null );
}
