// What we're going to export
const that = module.exports = {};

/**
 * The list of stores
 */
const stores = that.stores = { //!< List all of the accepted stores and their IDs
	'instacart.com' : {
		'name' : "Instacart",
		'url' : "http://instacart.com",
		'regexp' : new RegExp( "instacart", "i" ),
		'checkoutRegExp' : new RegExp( "checkout_v3", "i" )
	},
	'grocery.walmart.com' : {
		'name' : "Walmart Grocery",
		'url' : "http://walmart.com/grocery",
		'regexp' : new RegExp( "walmart", "i" ),
		'checkoutRegExp' : new RegExp( "checkout", "i" )
	}
};

// Get the store
that.getStore = function( s ) {
	return( stores[ s ] );
};


// Get the store for a cart
that.getStoreName = function( store_name ) {
	// Is it an object?
	if( store_name==null )
		return( null ); // What?
	
	// Is it an object that has the store name in it
	if( store_name.store_name )
		store_name = store_name.store_name;

	//debug( "Checking accepted stores for name", AcceptedStores, store_name );

	// Do we have it?
	const acceptedStore = stores[ store_name ];

	return( acceptedStore && acceptedStore.name || store_name );
};
