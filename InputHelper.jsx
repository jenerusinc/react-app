import React from 'react';
import { Text, Item, Label, Input, View } from 'native-base';

/**
 * Form helper
 */
export default function InputHelper( { label, errors, touched, handleBlur, values, handleChange, name, itemProps, inputProps, prepend, ...others } ) {
	// Do we have an error?
	const error = errors[ name ];
	const value = values[ name ];
	touched = touched[ name ];
	const isInvalid = error&&touched;

	return(
		<View {...others}>
        <Item fixedLabel error={isInvalid} {...itemProps}>
          {label ? <Label>{label}</Label> : null}
	  {prepend}
          <Input
           onChangeText={handleChange( name )}
           onBlur={handleBlur( name )}
           value={value}
					 {...inputProps}
					/>
        </Item>
				{isInvalid && <Text style={{ 'color' : "#721c24", 'fontSize' : 12, 'marginLeft' : 16, 'marginBottom' : 8 }}>{error}</Text>}
		</View>
	);
}
