import React, { useEffect, useState, useRef } from 'react';
import { AppLoading } from 'expo';

import * as Updates from 'expo-updates';

import { Animated } from 'react-native';

import { timeout } from './util';

import { Ionicons, FontAwesome5, MaterialIcons } from '@expo/vector-icons';
import * as Font from 'expo-font';

import { View, ActivityIndicator, Image, StyleSheet } from 'react-native';

import * as SplashScreen from 'expo-splash-screen';
import NetInfo from '@react-native-community/netinfo';

import SplashPng from './assets/splash.png';

const debug = require( './debug' )( "AppLoader" );

const CheckForUpdatesTimeout = 15000; // Millis

// Our styles
const styles = StyleSheet.create( {
	'background' : {
		'position' : "absolute",
		'top' : 0,
		'left' : 0,
		'right' : 0,
		'bottom' : 0,
		'backgroundColor' : "#0dceda",
		'justifyContent' : "center"
	},
	'image' : {
		'width' : "100%",
		'height' : "50%"
	}
} );

/**
 * Check for updaties
 */
function useCheckForUpdates() {
	// Did the timer time out?
	const isTimedOut = useRef( false );
	const timer = useRef();
	const finished = useRef( false );

	// Are we complete?
	const [ isDone, setIsDone ] = useState( false );

	// Cancel the timer
	const cancelTimer = ()=>{
		debug( "cancelTimer()" );

		clearTimeout( timer.current );
	};

	// Timer goes off
	const onTimer = ()=>{
		// Did we finished
		if( finished.current )
			return; // Don't bother
		
		debug( "Operation timed out." );

		// We are finished
		setIsDone( true );
		// Whoops
		finished.current = true;
	};

	// Cancel now
	const cancel = ()=>{
		// Are we already finished?
		if( finished.current ) {
			debug( "No double finished." );
			return;
		}

		// Finished
		setIsDone( true );
		finished.current = true;
		cancelTimer();
	};

	// our checker
	const doCheck = async ()=>{
		try {
			// no done
			setIsDone( false );

			// Do we have a timer already running?
			cancelTimer();

			// Now set the timeout
			timer.current = setTimeout( onTimer, CheckForUpdatesTimeout );

			// Get the net info stuffs
			const netInfo = await NetInfo.fetch();

			debug( "Got netinfo", netInfo );

			// Are we connected?
			if( !netInfo.isConnected ) {
				debug( "We are not connected. Forget it." );
				return( cancel() );
			}

			// Are we on wifi?
			if( !netInfo.type=="wifi" ) {
				debug( "We aren't on wifi. Forget it." );
				return( cancel() );
			}

			//await timeout( 3000 );

			// Too bad
			if( finished.current ) {
				debug( "We timed out so we're not doing this." );
				return;
			}

			// Did we do okay?
			debug( "We can continue execution." );

			// Check for the update
			const update = await Updates.checkForUpdateAsync();

			// No new update
			if( !update.isAvailable ) {
				debug( "No new update." );
				return( cancel() );
			}

			// Do this
			debug( "Fetching the update actual." );

			// Fetch it
			await Updates.fetchUpdateAsync();

			// Did we take too long?
			if( finished.current ) {
				debug( "We took too long to do everything. Not updating." );
				return;
			}

			// Clear the timer so we don't proceed
			cancelTimer();

			// We're about to update...

			// Nothing will come after this
			await Updates.reloadAsync();
		}
		catch( e ) {
			debug( "Error fetching updates.", e );

			cancel();
		}
	};

	// Return the done and check status
	return( [ isDone, doCheck ] );
}

/**
 * Our font loader
 */
async function loadFonts() {
	debug( "Entering loadFonts()" );

	try {

		// Load some fonts
		await Font.loadAsync( {
				// The Jenerus font
				'Pier-Bold' : require( './assets/fonts/PierSans-Bold-700.otf' ),
				'Pier-Bold-Italic' : require( './assets/fonts/Pier-Sans-Bold-Italic-700.otf' ),
				'Pier-Italic' : require( './assets/fonts/Pier-Sans-Italic-400.otf' ),
				'Pier' : require( './assets/fonts/PierSans-Regular-400.otf' ),

				// NativeBase fonts
				Roboto: require('native-base/Fonts/Roboto.ttf'),
				Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

				// VectorIcons fonts
				...Ionicons.font,
				...FontAwesome5.font,
				...MaterialIcons.font,
			} );
	}
	catch( e ) {
		debug( "Error loading fonts", e );
	}

     debug( "Exiting loadFonts()" );
}

/**
 * Our app loader hook
 */
export default function( { children } ) {
	const animation = useRef( new Animated.Value( 0 ) ).current;
	//const [ height, setHeight ] = useState( 0 );
	const height = useRef( 0 );
	const [ showChildren, setShowChildren ] = useState( false );
	const [ showLoading, setShowLoading ] = useState( true );

	const [ isUpdaterDone, checkForUpdates ] = useCheckForUpdates();
	const [ weAreDone, setWeAreDone ] = useState( false );

	// Are we finished loading?
	const [ isFinished, setIsFinished ] = useState( false );

	// When we layout
	const onViewLayout = event => {
	    // Get the height
	    const { 'height' : h } = event.nativeEvent.layout;

		debug( "Got new height", height );

	    // Save it
	    //setHeight( height );
	    height.current = h;
	};

	// Start animation
	const animateOut = ()=>{
		// Put the children underneath
		setShowChildren( true );

		const toValue = height.current;

		debug( "Triggering animate out.", toValue );

	    Animated.timing(
	      animation,
	      {
		toValue,
		duration: 500,
		'useNativeDriver' : true
	      }
	    ).start( ()=>setShowLoading( false ) );
	};

	// hooky on mounty
	useEffect( ()=>{
		// Don't hide yet
		SplashScreen.preventAutoHideAsync();
	}, [] );

	// load now
	const doLoad = async ()=>{
		// Loader
		SplashScreen.hideAsync();

		// Check for updates also
		checkForUpdates();

		// Our stuff
		await loadFonts();

		// We are done
		setWeAreDone( true );
	};

	// How did we do?
	useEffect( ()=>{
		if( weAreDone && isUpdaterDone )
			animateOut();

	}, [ weAreDone, isUpdaterDone ]  );

	// Loader screen
	return(
		<>
			{showChildren ? children : null}
			{showLoading ?
			<Animated.View
				onLayout={onViewLayout}
				style={[
					styles.background,
					{ 'transform' : [ { 'translateY' : animation } ] }
					]}>
				<Image
					source={SplashPng}
					resizeMode="contain" style={styles.image}
					onLoad={doLoad}
				/>
				<ActivityIndicator size="large" color="#ffffff" />
			</Animated.View> : null}
		</>
	);
}
