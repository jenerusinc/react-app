import React from 'react';

import { StyleSheet } from 'react-native';
import { Text, Button, Icon, Toast } from 'native-base';
import * as Google from 'expo-google-app-auth';

import { createCookieRequest } from './request';

import { pick } from 'lodash';

import { GoogleSignInOpts } from './constants';

const debug = require( './debug' )( "GoogleSignInButton" );

import { useOnSuccess, LoginSuccessPopup } from './views/LoginOrRegister';

//import G from './svgs/g.svg';
import G from './svgs/btn_google_light_normal_ios.svg';

const styles = StyleSheet.create( {
	'button' : {
		'backgroundColor' : "#4285F4",
	},
	'text' : {
		'color' : "#ffffff"
	}
} );

/**
 * Export default button
 */
export default function( { navigation, route, style, ...others } ) {
	// On our success
	const [ onSuccess, modalProps ] = useOnSuccess();

	// When we press
	const onPress = async ()=>{
		try {
			// Create a cookie request
			const cookieRequest = createCookieRequest();

			// Try to do it
			let result = await Google.logInAsync( GoogleSignInOpts );

			debug( "We got this result", result );

			// Shorthand this
			const params = pick( result, "refreshToken", "accessToken", "idToken" );
			
			// Now post it
			result = await cookieRequest.post( "auth/google/loginWithIdToken", params );

			// Call on success
			await onSuccess( result, cookieRequest, navigation, route );
		}
		catch( e ) {
			// Let's toast
			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	};

	return(
		<>
			<LoginSuccessPopup {...modalProps} />
			<Button block onPress={onPress} style={[ styles.button, style ]} {...others}><G width={60} /><Text style={styles.text}>Sign-In with Google</Text></Button>
		</>
	);
};
