import React from 'react';

import ErrorBoundary from './ErrorBoundary';
import Suspense from './Suspense';

/**
 * Wrap this
 */
export default function( Component ) {
	return( ( props )=>
		<ErrorBoundary>
			<Suspense>
				<Component {...props} />
			</Suspense>
		</ErrorBoundary>
	);
}
