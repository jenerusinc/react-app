import { atom, selector } from 'recoil';

const debug = require( './debug' )( "atoms" );

import { PersistentKeyStorageKey, SlideShowSeenKey, IsIOS } from './constants';

import * as AppleAuthentication from 'expo-apple-authentication';

import { setItemAsync, getItemAsync } from 'expo-secure-store';

const UserKeyAtom = atom( {
	'key' : "UserKeyAtom",
	'default' : (async function() {
		try {
			const userKey = await getItemAsync( PersistentKeyStorageKey );

			debug( "Data from storage", userKey );

			return( userKey );
		}
		catch( e ) {
			debug( e );
		}

		// Otherwise naught
		return( null );
	})() // Run now to get the promise
} );

export const UserKey = selector( {
	'key' : "UserKeySelector",
	'get' : ( { get } )=>get( UserKeyAtom ),
	'set' : async ( { set }, value )=>{
		debug( "Setting key to", UserKeyAtom, value );

		// Set the atom and in storage
		set( UserKeyAtom, value );

		// Storage
		await setItemAsync( PersistentKeyStorageKey, value||"" );
	}
} );

/**
 * Is the user logged in?
 */
export const IsLoggedIn = selector( {
	'key' : "IsLoggedIn",
	'get' : ( { get } )=>Boolean( get( UserKey ) )
} );

/**
 * Our actual user atom
 */

export const User = atom( {
	'key' : "User",
	'default' : null
} )

/**
 * Our user application
 */
export const UserApplication = atom( {
	'key' : "UserApplication",
	'default' : null
} );

/**
 * Use ask
 */
export const Ask = atom( {
	'key' : "UserAsk",
	'default' : null
} );

/**
 * Whether or not the user has seen the slide show
 */
export const SeenSlideShowAtom = atom( {
	'key' : "SeenSlideShowAtom",
	'default' : (async function() {
		try {
			let seenSlideShow = await getItemAsync( SlideShowSeenKey );

			debug( "User seen slide show", seenSlideShow );

			// Parse it
			seenSlideShow = JSON.parse( seenSlideShow );

			debug( "We seen the slide show", seenSlideShow );

			return( seenSlideShow );
		}
		catch( e ) {
			debug( e );
		}

		// Otherwise naught
		return( null );
	})() // Run now to get the promise
} );

export const SeenSlideShow = selector( {
	'key' : "SeenSlideShow",
	'get' : ( { get } )=>get( SeenSlideShowAtom ),
	'set' : async ( { set }, value )=>{
		debug( "Setting key to", SeenSlideShowAtom, value );

		// Set the atom and in storage
		set( SeenSlideShowAtom, value );

		// Storage
		await setItemAsync( SlideShowSeenKey, JSON.stringify( value||false ) );
	}
} );

/**
 * Whether or not the user has an ask
 */
export const HasAsk = selector( {
	'key' : "HasAsk",
	'get' : ( { get } )=>Boolean( get( Ask ) )
} );

/**
 * Whether or not the user has an ask
 */
export const HasAppleAuthentication = selector( {
	'key' : "HasAppleAuthentication",
	'get' : async ()=>{
		try {
			// Not ios
			if( !IsIOS )
				return( false ); // Not even iOS

			// Return it
			return( await AppleAuthentication.isAvailableAsync() );
		}
		catch( e ) {
			debug( "Error determining if there is apple authentication", e );
		}

		// Nossir
		return( false );
	}
} );

/**
 * The issued card
 */
export const IssuedCard = atom( {
	'key' : "IssuedCard",
	'default' : null
} );

/**
 * Whether or not the user has an ask
 */
export const HasIssuedCard = selector( {
	'key' : "HasIssuedCard",
	'get' : ( { get } )=>Boolean( get( IssuedCard ) )
} );

/**
 * The ask categories
 */
export const AskCategories = atom( {
	'key' : "AskCategories",
	'default' : null
} );

/**
 * Do we have categories?
 */
export const HaveAskCategories = selector( {
	'key' : "HaveAskCategories",
	'get' : ( { get } )=>{
		// Get the ask categorie
		const askCategories = get( AskCategories );

		// Not an array? No have.
		if( !(askCategories instanceof Array) )
			return( false );

		// DO we have items?
		return( askCategories.length>0 );
	}
} );
