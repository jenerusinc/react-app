import React from 'react';
import { Text, Textarea, View, Label, Item } from 'native-base';

/**
 * Form helper
 */
export default function InputHelper( { textareaProps, errors, touched, handleBlur, values, handleChange, name, ...others } ) {
	// Do we have an error?
	const error = errors[ name ];
	const value = values[ name ];
	touched = touched[ name ];
	const isInvalid = error&&touched;

	return(
		<View {...others}>
          <Textarea
           onChangeText={handleChange( name )}
           onBlur={handleBlur( name )}
           value={value}
					 {...textareaProps}
					/>
				{isInvalid && <Text style={{ 'color' : "#721c24", 'fontSize' : 12, 'marginLeft' : 16, 'marginBottom' : 8 }}>{error}</Text>}
		</View>
	);
}
