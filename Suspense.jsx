import React, { Suspense } from 'react';

import LoadingScreen from './LoadingScreen';

/**
 * Our suspenseful
 */
export default function( { children, fallback, text } ) {
	return(
		<Suspense children={children} fallback={fallback ? fallback : <LoadingScreen text={text} />} />
	);
}
