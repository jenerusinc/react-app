import React, { useEffect } from 'react';

import request from './request';

import { useRecoilValueLoadable } from 'recoil';
import { UserKey } from './atoms';

const debug = require( './debug' )( "RegisterForPushes" );

import * as Permissions from 'expo-permissions';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import { Platform } from 'react-native';

/**
 * Register to send pushes
 */
async function registerToken( token ) {
	try {
		// Build our payload
		const params = {
			token,
			'installationID' : Constants.installationId,
			'platform' : Platform.OS
		};

		debug( "Sending payload", params );

		// Now actually send it
		const r = await request.post( "user/me/registerDevice", params );

		debug( "Device is registerd", r );
	}
	catch( e ) {
		debug( "Error registering token", e );
	}
}

/**
 * Regster for push notifications via expo
 */
export default function() {
	// Get the recoil key
	const userKey = useRecoilValueLoadable( UserKey );

	// Our actual register function
	const registerForPushes = async () => {
	  if (Constants.isDevice) {
	    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
	    
		debug( "Got existing permissions", existingStatus );

	    let finalStatus = existingStatus;
	    if (existingStatus !== 'granted') {
	      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);

		debug( "New permissions", status );

	      finalStatus = status;
	    }

		debug( "Final permissions", finalStatus );

	    if (finalStatus !== 'granted') {
	      debug( 'Failed to get push token for push notification!' );
	      return;
	    }

	    const token = await Notifications.getExpoPushTokenAsync();

	    debug( "Got the pushes token", token );

		// Register for it
		registerToken( token );
	  }
	  else
	    debug( 'Must use physical device for Push Notifications' );

	  if (Platform.OS === 'android') {
	    Notifications.setNotificationChannelAsync('default', {
	      name: 'default',
	      importance: Notifications.AndroidImportance.MAX,
	      vibrationPattern: [0, 250, 250, 250],
	      lightColor: '#FF231F7C',
	    });
	  }
	};

	// Now hook the key for changes
	useEffect( ()=>{
		// Get the stuff we care about
		const { state, contents } = userKey;

		debug( "User key state=", state, "contents=", contents );

		// Only get pushes if we have a user key.
		// Otherwise we don't care
		if( state=="hasValue" ) {
			if( contents ) {
				debug( "Attempting to get pushes." );

				// Now register
				// Async
				registerForPushes();
			}
		}

	}, [ userKey ] );

	// We don't actually render anything
	return( null );
}
