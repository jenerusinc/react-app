import React, { useEffect, useRef } from 'react';

import request, { fetchUser, fetchAsk, fetchIssuedCard, fetchAskCategories } from './request';

import { useRecoilValueLoadable, useSetRecoilState } from 'recoil';
import { UserKey, User, Ask, IssuedCard, AskCategories } from './atoms';

const debug = require( './debug' )( "DataLoader" );

/**
 * Regster for push notifications via expo
 */
export default function() {
	// Get the recoil key
	const userKey = useRecoilValueLoadable( UserKey );
	const isMounted = useRef( false );

	// Our setters
	const setUser = useSetRecoilState( User );
	const setAsk = useSetRecoilState( Ask );
	const setIssuedCard = useSetRecoilState( IssuedCard );
	const setAskCategories = useSetRecoilState( AskCategories );

	
	// Fetch all
	const fetchUserStuff = async ()=>{
		try {
			debug( "Fetching all data." );

			const [
				{ user },
				{ ask },
				{ 'card' : issuedCard },
			] = await Promise.all( [
				fetchUser(),
				fetchAsk(),
				fetchIssuedCard(),
			] );

			// Set them all
			setUser( user );
			setAsk( ask );
			setIssuedCard( issuedCard );
		}
		catch( e ) {
			debug( "Error fetching for use everything.", e );
		}
	};
	const fetchOtherStuff = async ()=>{
		try {
			debug( "Fetching rest data." );

			const [
				{ categories }
			] = await Promise.all( [
				fetchAskCategories()
			] );

			// Set them all
			setAskCategories( categories );
		}
		catch( e ) {
			debug( "Error fetching for everything else.", e );
		}
	};

	// Now hook the key for changes
	useEffect( ()=>{
		// Get the stuff we care about
		const { state, contents } = userKey;

		debug( "User key state=", state, "contents=", contents );

		// Only get pushes if we have a user key.
		// Otherwise we don't care
		if( state=="hasValue" ) {
			// We've been mounted
			isMounted.current = true;

			// Do we have anything?
			if( contents ) {
				// Fetch everything
				fetchUserStuff();
			}
			else if( isMounted.current ) {
				// No user. Kill it all.
				setUser( null );
				setAsk( null );
				setIssuedCard( null );
			}
		}

	}, [ userKey ] );

	// Things that we don't need to be logged in for
	useEffect( ()=>{
		fetchOtherStuff();
	}, [] );

	// We don't actually render anything
	return( null );
}
