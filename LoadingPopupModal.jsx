import React from "react";
import {
  Modal,
  StyleSheet
} from "react-native";

import { Text, View, H2, Spinner } from 'native-base';

import { JenerusBlue } from './constants';

const styles = StyleSheet.create( {
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    'backgroundColor' : "rgba( 0, 0, 0, 0.75 )"
  },
  modalView: {
    'backgroundColor' : "white",
    borderRadius: 20,
    padding: 35,
    'borderColor' : JenerusBlue,
    'borderWidth' : 1,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  'marginBottom' : {
  	'marginBottom' : 16
  }
} );

/**
 * Show the popppy uppy
 */
export default function( { visible, onClick, title, text } ) {
  return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={onClick}
      >
	  <View style={styles.centeredView}>
		  <View style={styles.modalView}>

			{title ? <H2 style={styles.marginBottom}>{title}</H2> : null}
			<Spinner style={styles.marginBottom} />
			{text!=null ? <Text style={styles.marginBottom} >{text}</Text> : null}

		  </View>
	</View>
      </Modal>
  );
};
