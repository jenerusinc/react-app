import React from 'react';

import { Label, Item, Picker, Text } from 'native-base';

export default function( { errors, values, touched, setFieldValue, label, pickerProps, name, children, itemProps } ) {
	// Do we have an error?
	const error = errors[ name ];
	const value = values[ name ];
	touched = touched[ name ];
	const isInvalid = error&&touched;

	return(
		<Item picker {...itemProps}>
			{label&&<Label>{label}</Label>}
			<Picker
				selectedValue={value}
				onValueChange={v=>setFieldValue( name, v )}
				{...pickerProps}
			>
				{children}
			</Picker>
			{isInvalid && <Text style={{ 'color' : "#721c24", 'fontSize' : 12, 'marginLeft' : 16, 'marginBottom' : 8 }}>{error}</Text>}
		</Item>
	);
}
