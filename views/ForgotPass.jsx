import React, { useRef } from 'react';

import { Container, Content, Text, Form, Item, Label, Input, Toast, H2, Button, View } from 'native-base';
import CustomButton from '../CustomButton';
import InputHelper from '../InputHelper';
import { StyleSheet } from 'react-native';

import DefaultHeader from '../DefaultHeader';

import request from '../request';

import { FormSchemas } from '../constants';

import { Formik } from 'formik';
import * as Yup from 'yup';

const debug = require( '../debug' )( "views:ForgotPass" );

const defaultItemProps = { 'floatingLabel' : true };

// Our form schemerr
const forgotPasswordFormSchema = Yup.object( {
  'email'    : FormSchemas.email
} );

// Styles
const styles = StyleSheet.create( {
	'container': {
		'padding': 8
	},
	'control': {
		'marginTop': 8
	},
	'header': {
		'marginBottom': 8
	}
} );

/**
 * Thing it
 */
function makeForgotPassScreen( hasHeader ) {
	return( function( { navigation, route } ) {

	// Form ref
	const formRef = useRef();

	/**
	 * When the user requests a reset password linky
	 */
	const onSubmit = async values=>{
		try {
			// Do the reset with captcha
			await request.postCaptcha( "forgotPassword", "user/password/forgot", values );

			// Yay
			Toast.show({
				'text': `We have sent you instructions on how to reset your password to '${values.email}'.`,
				'buttonText': "Dismiss",
				'type': "success",
				'duration': 0
			});

			// Reset the form
			if( formRef.current ) {
				debug( "Resetting the form" );

				formRef.current.resetForm();
			}

			// Should we go back after the action?
			const { params } = route;

			// Did our parent want us to go back?
			if( params && params.shouldGoBack )
				navigation.goBack();
		}
		catch( e ) {
			// Pop up
			Toast.show({
				'text': e.message || e,
				'buttonText': "Dismiss",
				'type': "danger",
				'duration': 0
			});
		}
	};

  return (
    <Container>
	{hasHeader ?
	     <DefaultHeader title="Password Recovery" /> : null}
	<Content style={styles.container}>

	    <Formik
	      validationSchema={forgotPasswordFormSchema}
	      onSubmit={onSubmit}
	      initialValues={{ 'email' : "" }}
	      innerRef={formRef}
	    >
	      {({
		handleSubmit,
		handleChange,
		handleBlur,
		values,
		touched,
		isValid,
		errors,
		isSubmitting
	      }) => (

	    <Form>

          <H2 style={styles.header}>Forgot Your Password?</H2>

          <Text>No problem! Let's get it reset.</Text>

			<InputHelper
				style={styles.control}
				label="Your E-mail Address"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="email"
				values={values}
				itemProps={{ ...defaultItemProps, 'last' : true }}
				inputProps={{ 'autoCompleteType' : "email", 'autoCapitalize' : "none", 'keyboardType' : "email-address" }}
			/>
        
	<CustomButton
	style={styles.control}
	isLoading={isSubmitting}
	block 
	onPress={handleSubmit}>
        <Text>E-mail me a reset link</Text>
      </CustomButton>
		</Form>
			
			)}
			</Formik>

    </Content>
  </Container>
   
  
  );
} );

}

/**
 * Default with header
 */
export default makeForgotPassScreen( true );


/**
 * Export without header
 */
export const EmbeddedForgotPass = makeForgotPassScreen( false );
