import React from 'react';

import Settings from './Settings';

import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../../DefaultHeader';

const debug = require( '../../debug' )( "views:Settings:index" );

import makeEdits from './Edit';
import Avatar from './Avatar';

//import SlideShow from './SlideShow';

const Stack = createStackNavigator();

// Make the edits
const EditScreens = makeEdits( Stack );

/**
 * Container
 */
export default function( { navigation } ) {

	return(
		<Stack.Navigator {...defaultNavigationOptions}>
			<Stack.Screen name="Settings"
				component={Settings}
				options={{ 'title' : "Settings" }}
				/>

			<Stack.Screen name="Avatar"
				component={Avatar}
				options={{ 'headerShown' : false }}
				/>

			{EditScreens}
		</Stack.Navigator>
	);
}
