import React, { useEffect } from 'react';

import { FormSchemas } from '../../constants';

import CustomButton from '../../CustomButton';
import InputHelper from '../../InputHelper';

import { Formik } from 'formik';
import * as Yup from 'yup';

import _ from 'lodash';

import { Content, Container, Form, Toast, H2, Text } from 'native-base';

import * as atoms from '../../atoms';
import { useSetRecoilState } from 'recoil';

import request from '../../request';

// The default item properties for the inputs
const itemProps = { 'stackedLabel' : true };

const debugMaker = require( '../../debug' );

/**
 *
 * Our control for this
 */
export function makeEditScreen( { name, schema, requirePassword, requireConfirm, title, label, inputProps } ) {
	const debug = debugMaker( "views:Settings:Edit:"+name );

	// Make the form schema
	let formSchema = {
		// Start with the control
		[ name ] : schema
	};
	let confirmField;

	// Build our initial values
	const initialValues = {};

	// Does it require a password?
	if( requirePassword ) {
		formSchema.currentPassword = FormSchemas.password;
		initialValues.currentPassword = "";
	}

	// Does it need to be confirmed?
	if( requireConfirm ) {
		// Shorthand
		confirmField = name+"Confirm";

		// Bang
		formSchema[ confirmField ] = schema.clone().oneOf( [ Yup.ref( name ), null ], "Please confirm your "+name ); // Clone the other
		// Boom
		initialValues[ confirmField ] = "";
	}
	
	// Now make it into an official yup object
	formSchema = Yup.object( formSchema );

	// Our actual edit screen react will evaluate
	// Basically returning a thunk
	return( function( { navigation, route } ) {

		// When mount
		useEffect( ()=>{
			// Set the title
			navigation.setOptions( { title } );
		}, [] );

		// The user recoil
		const setUser = useSetRecoilState( atoms.User );

		// My initial values
		// Grab the field value from the route
		const myInitialValues = { ...initialValues, [ name ] : route.params&&route.params[ name ]||"" };

		// On submission
		const onSubmit = async values=>{
			try {
				debug( "We will send values.", values );

				// Post the edited values
				const { user } = await request.post( "user/me", values );

				debug( "New saved user is", user );

				// Save
				setUser( user );

				// Go berk and close us
				navigation.goBack();
			}
			catch( e ) {
				// Toast it
				Toast.show( {
						'text' : e.message||e,
						'buttonText' : "Dismiss",
						'type' : "danger",
						'duration' : 0
					} );
			}
		};

		// OUtput the form
		return(
		<Container style={{ 'padding' : 8 }}>
			<Content>
			    <Formik
			      validationSchema={formSchema}
			      onSubmit={onSubmit}
			      initialValues={myInitialValues}
			    >
			      {({
				handleSubmit,
				handleChange,
				handleBlur,
				values,
				touched,
				isValid,
				errors,
				isSubmitting
			      }) => (

			      <Form>
					  <H2 style={{ 'marginBottom' : 8 }}>{title}</H2>

								{requirePassword ?
								<InputHelper
									label="Your current password"
									errors={errors}
									touched={touched}
									handleBlur={handleBlur}
									handleChange={handleChange}
									name="currentPassword"
									values={values}
									inputProps={{ 'autoCompleteType' : "password", 'secureTextEntry' : true }}
									itemProps={itemProps}
								/> : null}

								<InputHelper
									label={label}
									errors={errors}
									touched={touched}
									handleBlur={handleBlur}
									handleChange={handleChange}
									name={name}
									values={values}
									inputProps={inputProps}
									itemProps={{ ...itemProps, 'last' : !confirmField }}
								/>

								{confirmField ?
								<InputHelper
									label="Please retype"
									errors={errors}
									touched={touched}
									handleBlur={handleBlur}
									handleChange={handleChange}
									name={confirmField}
									values={values}
									inputProps={inputProps}
									itemProps={{ ...itemProps, 'last' : confirmField }}
								/> : null}

							<CustomButton
								style={{ 'marginTop' : 16 }}
								isLoading={isSubmitting}
								onPress={handleSubmit}
								block
							>
								<Text>Save</Text>
							</CustomButton>
					</Form>

			      )}
			    </Formik>
			</Content>
		</Container>
		);

	} );
}

// Our screens
const screens = {};

// Make all of the screens we need
screens.EditNameScreen = makeEditScreen( {
	'name' : "name",
	'label' : "New Name",
	'schema' : FormSchemas.name,
	'title' : "Update your name",
	'inputProps' : { 'autoCapitalize' : "words", 'autoCompleteType' : "name" }
} );

screens.EditUsernameScreen = makeEditScreen( {
	'name' : "username",
	'label' : "New Username",
	'schema' : FormSchemas.username,
	'title' : "Update your username",
	'inputProps' : { 'autoCapitalize' : "none", 'autoCompleteType' : "username" }
} );

/*
screens.EditEmailScreen = makeEditScreen( {
	'name' : "email",
	'label' : "New E-mail Address",
	'schema' : FormSchemas.email,
	'title' : "Update your e-mail",
	'inputProps' : { 'autoCompleteType' : "email", 'autoCapitalize' : "none", 'keyboardType' : "email-address" },
	'requirePassword' : true
} );
*/

screens.EditPasswordScreen = makeEditScreen( {
	'name' : "newPassword",
	'label' : "New Password",
	'schema' : FormSchemas.password,
	'title' : "Change password",
	'inputProps' : { 'autoCompleteType' : "password", 'secureTextEntry' : true },
	'requirePassword' : true,
	'requireConfirm' : true
} );

/**
 * Make all of the routes
 */
export default function( Stack ) {
	return( _.map( screens, ( Component, screen )=>
			<Stack.Screen name={screen}
				key={screen}
				component={Component}
				/>
		)
	);
}
