import React from 'react';


const debug = require( '../../../debug' )( "views:Settings:Avatar:Object" );

import { View } from 'native-base';

import { SvgUri } from 'react-native-svg';

import { JenerusAvatarURL } from '../../../constants';
import qs from 'qs';


/**
 * Our object
 */
export default function( { params, url } ) {

	// Make the avatar URL
	const avatarURL = url||`${JenerusAvatarURL}?${qs.stringify( params )}`;

	debug( "We fetching this avatar.", avatarURL );

	return(
		<View style={{ 'flex' : 1, 'justifyContent' : "center", 'marginBottom' : 16 }}>
			<SvgUri width="60%" uri={avatarURL} style={{ 'alignSelf' : "center" }} />
		</View>
	);
}
