import React, { useState, useEffect } from 'react';

//import LoadingScreen from '../../LoadingScreen';
//import ErrorRetryScreen from '../../ErrorRetryScreen';

import * as atoms from '../../../atoms';
import { useRecoilState } from 'recoil';

import request from '../../../request';
import O from './Object';

import _ from 'lodash';

import wrapForRecoil from '../../../wrapForRecoil';

const debug = require( '../../../debug' )( "views:Settings:Avatar:Main" );

import { View, Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right, Spinner, Toast } from 'native-base';

import qs from 'qs';

import options from './options';

/**
 * Our item type
 */
function Item( { item, params, navigation, ...others } ) {
	// Expand these
	const {
		label,
		field,
		options
	} = item;

	// Get our current
	const param = params[ field ];

	// Get the option we use
	let option = options.find( ( { key } )=>key==param );

	debug( "Got field for param", field, param );

	return(
		<ListItem icon onPress={()=>navigation.navigate( `EditPart${field}`, { params } )} {...others}>
		    <Body>
		      <Text>{label}</Text>
		    </Body>
		    <Right>
		      <Text>{option&&option.value||""}</Text>
		      <Icon active name="arrow-forward" />
		    </Right>
		  </ListItem>
	);
}

/**
 * Our settings page
 */
export default wrapForRecoil( function( { navigation, route } ) {
	const [ user, setUser ] = useRecoilState( atoms.User );

	const [ saving, setSaving ] = useState( false );
	const [ hasChanged, setHasChanged ] = useState( false );

	// Get the initial state of params
	const initialParams = function() {
		const { params } = route;

		// Do we have in params?
		if( params&&params.avatar )
			return( qs.parse( params.avatar ) );

		// A user?
		if( user&&user.avatar )
			return( qs.parse( user.avatar ) );

		// Default
		return( {} );
	}

	// Our variable for current avatar params
	// Default our avatar params
	const [ params, setParams ] = useState( initialParams() );
	
	// The route changed?
	useEffect( ()=>{
		const { 'params' : newParams } = route;

		debug( "Got route", newParams );

		if( _.size( newParams ) && _.size( newParams.params ) ) {
			// Merge
			setParams( newParams.params );
			// We have changed
			setHasChanged( true );
		}
	}, [ route ] );

	// When we be saving
	const onSave = async ()=>{
		// Yes
		setSaving( true );

		// We are now loading
		try {
			debug( "Sending avatar params", params );

			// Save the user
			const { user } = await request.post( "user/me", { 'avatar' : params } );

			debug( "Got user user", user );

			// Save him
			setUser( user );

			// Get out of here
			navigation.navigate( "Settings" );
		}
		catch( e ) {
			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
		      } );
		}

		// Now loading
		setSaving( false );
	};

	// When we had a change
	useEffect( ()=>{
		if( !hasChanged )
			return; // Don't care

		navigation.setOptions( {
			'headerLeft' : <Button transparent onPress={()=>navigation.goBack()}><Icon type="FontAwesome" name="times" /></Button>,
			'headerRight' : saving ? <Spinner /> : <Button transparent onPress={onSave}><Icon type="FontAwesome" name="check" /></Button>,
		} );
	}, [ hasChanged, saving, params ] );

  return(
    <Container>
        <Content>

	<O params={params} />

	{options.map( ( item, index )=><Item item={item} navigation={navigation} params={params} key={index} /> )}

    </Content>
  </Container>
   
   
  
  );
} );
