import React from 'react';

import Main from './Main';

import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../../../DefaultHeader';

const debug = require( '../../../debug' )( "views:Settings:Avatar:index" );

import makeEditParts from './EditPart';

//import SlideShow from './SlideShow';

const Stack = createStackNavigator();

// Make the edits
const EditPartScreens = makeEditParts( Stack );

/**
 * Container
 */
export default function( { navigation } ) {

	return(
		<Stack.Navigator {...defaultNavigationOptions}>
			<Stack.Screen name="Main"
				component={Main}
				options={{ 'title' : "Your Avatar" }}
				/>

			{EditPartScreens}
		</Stack.Navigator>
	);
}
