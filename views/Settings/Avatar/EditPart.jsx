import React, { useState, useEffect } from 'react';

//import LoadingScreen from '../../LoadingScreen';
//import ErrorRetryScreen from '../../ErrorRetryScreen';

import * as atoms from '../../../atoms';
import { useRecoilState } from 'recoil';

import request from '../../../request';
import O from './Object';

import wrapForRecoil from '../../../wrapForRecoil';

const debugMaker = require( '../../../debug' );

import { View, Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right, Radio } from 'native-base';

import { SvgUri } from 'react-native-svg';

import qs from 'qs';

import Options from './options';

import { CommonActions } from '@react-navigation/native';

import { BackButton } from '../../../DefaultHeader';

/**
 * Our radio selector
 */
function MyRadio( { selected, text, ...others } ) {
	return(
		<ListItem selected={selected} {...others}>
		    <Left>
		      <Text>{text}</Text>
		    </Left>
		    <Right>
		      <Radio selected={selected} />
		    </Right>
		  </ListItem>
	);
}


/**
 * Make the options component
 */
function makeEditPart( part ) {
	// Now break out the parts
	const {
		label,
		field,
		options
	} = part;

	// Make our debugger
	const debug = debugMaker( "views:Settings:Avatar:EditPart:"+field );

	// Return the thunk
	return( function( { navigation, route } ) {
		debug( "We gout route", route );

		// The params
		const [ params, setParams ] = useState( route.params&&route.params.params||{} );
		// Our value
		const [ value, setValue ] = useState( params[ field ]||options[ 0 ].key );

		debug( "We got value", value, params );

		// Hook on back
		useEffect( ()=>{
			// Set our left header
			navigation.setOptions( {
				'headerLeft' : <BackButton onPress={()=>navigation.navigate( "Main", { params } )} />
			} );
		}, [ value ] );

		/**
		 * When it changes
		 */
		const onChange = key=>{
			// Change the value
			setValue( key );
			// Now in the params
			setParams( { ...params, [ field ] : key } );
		};

		return(
			<Container>
				<O params={params} />

				<Content>

					{/* The options */}
					{options.map( ( { key, 'value' : v} )=><MyRadio onPress={()=>onChange( key )} key={key} text={v} selected={key==value} /> )}
				</Content>
			</Container>
		);
	} );
}

/**
 * Make our navs
 */
export default function( Stack ) {
	return(
		Options.map( option=>
			<Stack.Screen
				name={`EditPart${option.field}`}
				key={option.field}
				component={makeEditPart( option )}
				options={{ 'title' : option.label }}
			/>
		)
	);
}
