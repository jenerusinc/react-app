import React, { useState, useEffect } from 'react';

import LoadingScreen from '../../LoadingScreen';
import ErrorRetryScreen from '../../ErrorRetryScreen';

import * as atoms from '../../atoms';
import { useRecoilState } from 'recoil';

import wrapForRecoil from '../../wrapForRecoil';

import { fetchUser } from '../../request';

const debug = require( '../../debug' )( "views:Settings:Settings" );

import { View, Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right } from 'native-base';

import { SvgUri } from 'react-native-svg';

/**
 * Our settings page
 */
export default wrapForRecoil( function( {  navigation } ) {
	const [ user, setUser ] = useRecoilState( atoms.User );

	const [ loading, setLoading ] = useState( false );
	const [ errorLoading, setErrorLoading ] = useState();

	// Fetch the ask
	const getUser = async ()=>{
		debug( "Loading our ask." );

		// Loading start
		setLoading( true );

		try {
			// There is no error loading anymore
			setErrorLoading( null );

			// Fetch the user ask
			const { user } = await fetchUser();

			debug( "We got the user.", user );
			
			// Now set the ask
			setUser( user );
		}
		catch( e ) {
			// Ruh roh
			setErrorLoading( e );
		}

		// Loading end
		setLoading( false );
	};

	// We've mounted. This will be when a user logs in or the app is loaded
	useEffect( ()=>{
		// Do we have no ask?
		if( !user )
			getUser(); // Get it
	}, [] );

	// Are we loading?
	if( loading )
		return( <LoadingScreen text="Getting your info..." /> );
	
	// Set the error
	if( errorLoading )
		return( <ErrorRetryScreen error={errorLoading} onRetry={getUser} /> );
	
	// No user?
	if( !user )
		return( <ErrorRetryScreen error="We don't have any user information for you!" onRetry={getUser} /> );

  return(
    <Container>
        <Content>

	{/*
	Won't render vector
	<Thumbnail large source={{ 'uri' : user.avatarURL }} />
	*/}

	<View style={{ 'flex' : 1, 'justifyContent' : "center", 'marginBottom' : 16 }}>
		<SvgUri width="60%" uri={user.avatarURL} style={{ 'alignSelf' : "center" }} />
		<Button style={{ 'marginTop' : 8, 'alignSelf' : "center" }} onPress={()=>navigation.navigate( "Avatar" )}><Text>Change Avatar</Text></Button>
	</View>

	<ListItem icon onPress={()=>navigation.navigate( "EditNameScreen", { 'name' : user.name } )}>
            <Body>
              <Text>Full Name</Text>
            </Body>
            <Right>
              <Text>{user.name}</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>

	<ListItem icon onPress={()=>navigation.navigate( "EditUsernameScreen", { 'username' : user.username } )}>
            <Body>
              <Text>User Name</Text>
            </Body>
            <Right>
              <Text>{user.username}</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>

	<ListItem icon>
            <Body>
              <Text>E-mail</Text>
            </Body>
            <Right>
              <Text>{user.email}</Text>
              {/* Currently not changable
	      	<Icon active name="arrow-forward" />
		*/}
            </Right>
          </ListItem>

	<ListItem icon onPress={()=>navigation.navigate( "EditPasswordScreen" )}>
            <Body>
              <Text>Change Password</Text>
            </Body>
            <Right>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>

	  {/*
	  The sample from the nativebase website
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="wifi" />
              </Button>
            </Left>
            <Body>
              <Text>Wi-Fi</Text>
            </Body>
            <Right>
              <Text>GeekyAnts</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="bluetooth" />
              </Button>
            </Left>
            <Body>
              <Text>Bluetooth</Text>
            </Body>
            <Right>
              <Text>On</Text>
              <Icon active name="arrow-forward" />
            </Right>
          </ListItem>
	  */}

    </Content>
  </Container>
   
   
  
  );
} );
