import React, { useState } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Grid, Col, View, Spinner, Container, Text, Button } from 'native-base';
import DefaultHeader from "../DefaultHeader"
import * as atoms from '../atoms';
import { useSetRecoilState } from 'recoil';
import CustomButton from '../CustomButton';

import request from '../request';

import Constants from 'expo-constants';

import GoodbyePng from '../assets/pngs/goodbye.png';

const debug = require( '../debug' )( "views:SignOut" );

/**
 * Simple loading view
 */
export default function( { navigation } ) {
	// The button loading state
	const [ isLoggingOut, setIsLoggingOut ] = useState( false );

	// Reset everything
	const setAsk = useSetRecoilState( atoms.Ask );
	const setUser = useSetRecoilState( atoms.User );
	const setUserKey = useSetRecoilState( atoms.UserKey );

	/**
	 * Force a logout
	 */
	const logout = async ()=>{
		// Loading indicator
		setIsLoggingOut( true );

		// Try and unregister us
		try {
			// Make our payload
			const params = {
				'installationID' : Constants.installationId
			};

			// Unregister
			await request.post( "user/me/unregisterDevice", params );
		}
		catch( e ) {
			debug( "Error unregistering device.", e );
		}

		// Set all
		setAsk( null );
		setUser( null );
		setUserKey( null );

		// No loading indicator
		setIsLoggingOut( false );

		// Navigate home
		navigation.navigate( "Home" );
	};

	return(
		<Container>
			<DefaultHeader title="Log out" />
			<Text style={{ 'fontSize' : 30, 'fontWeight' : "bold",'fontFamily' : "Pier-Bold",   textAlign: 'center', marginTop: 5}}>Until next time.</Text>
			<Image source={GoodbyePng} style={{width: "70%", height: "50%", marginTop: '5%'}} resizeMode="contain" />
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignItems' : "center" }}>
						<CustomButton isLoading={isLoggingOut} rounded warning style={{ 'alignSelf' : "center" }} onPress={logout}><Text>Logout Now</Text></CustomButton>
					</View>
				</Col>
			</Grid>
		</Container>
	);
}
