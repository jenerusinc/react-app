import React, { useState, useRef, useEffect } from 'react';
import { Container, Content, Text, Form, Item, Label, Input, Toast, H2, Button, View } from 'native-base';
import CustomButton from '../CustomButton';
import InputHelper from '../InputHelper';
import { StyleSheet } from 'react-native';

import { isObject, pick } from 'lodash';

import { getFullNameFromAppleObject } from '../util';

import { createCookieRequest } from '../request';

import { PersistentKeyStorageKey, UserNameConfirmRegExp, UserNameMinSize, FormSchemas } from '../constants';

import WeirdThumbsUpSvg from '../svgs/weird-thumbs-up.svg';

import * as atoms from '../atoms';
import { useSetRecoilState } from 'recoil';

import LoadingScreen from '../LoadingScreen';

import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../DefaultHeader';
import { EmbeddedForgotPass } from './ForgotPass';

import { setItemAsync } from 'expo-secure-store';

const debug = require('../debug')("views:LoginOrRegister.");

const Stack = createStackNavigator();

import ImagePopupModal from '../ImagePopupModal';

// Styles
const styles = StyleSheet.create( {
	'container': {
		'padding': 8
	},
	'control': {
		'marginTop': 8
	},
	'header': {
		'marginBottom': 8
	}
} );

//debug( "Request in", request );

import { Formik } from 'formik';
import * as Yup from 'yup';

const loginFormSchema = Yup.object( {
  'email'    : FormSchemas.email,
  'password' : FormSchemas.password
} );

const registerFormSchema = Yup.object({
	'name' : FormSchemas.name,
	'username' : FormSchemas.username,
  'email'    : FormSchemas.email,
  'password' : FormSchemas.password,
  'passwordConfirm' : Yup.string().oneOf( [ Yup.ref( 'password' ), null ], 'Passwords do not match' ).required( "Please confirm your password" )
});

const appleFormSchema = Yup.object({
	'name' : FormSchemas.name,
  //'email'    : FormSchemas.email
});

// The default item props
const defaultItemProps = { 'floatingLabel': true };

/**
 * When we are logged in or registered
 */
export function useOnSuccess() {
	// Our user stuff
	const setUser = useSetRecoilState(atoms.User);
	const setUserKey = useSetRecoilState(atoms.UserKey);

	// Our modal properties
	const [ modalProps, setModalProps ] = useState( { 'visible' : false } );

	// Our function
	const fn = async function ( { user }, cookieRequest, navigation, route ) {
		// Get the key
		const { key } = await cookieRequest.get("user/me/createPersistentSession");

		debug("Got key", key);

		// Now set and save everywhere
		setUser(user);
		setUserKey(key);

		// Our params to pass to navigation
		const params = {
			user,
			'loginSuccess': true
		};

		// The next screen we're going to
		let nextScreen = "Home"; // Defaults to home

		// Do we have?
		if (route && route.params) {
			// Shorthand some things
			const {
				screen // Our next screen
			} = route.params;

			// Do we have it?
			if (screen)
				nextScreen = screen;
		}

		// Set the properties
		setModalProps( {
			'visible' : true,
			'onClick' : ()=>{
				// Kill teh modal
				setModalProps( { ...modalProps, 'visible' : false } );
				// Navigation
				navigation.navigate( nextScreen, params );
			}
		} );
	};

	return( [
		fn,
		modalProps
	] );
}

/**
 * Login success popup
 */
export function LoginSuccessPopup( { visible, onClick } ) {

	return(
		<ImagePopupModal
			visible={visible}
			control={WeirdThumbsUpSvg}
			text="You are now logged into Jenerus!"
			buttonText="Let's go!"
			title="Success!"
			onClick={onClick}
		/>
	);
}

/**
 * Make the navigation function thunk
 */
function makeNavigation( to, navigation, route ) {
	// The params we're pushing
	let params;

	// Do we have params to push
	if( route && route.params )
		params = { ...route.params }; // Copy them

	// Now do
	return( ()=>navigation.navigate( to, params ) );
}

/**
 * The login form
 */
export function LoginForm( { navigation, route } ) {
	debug("LoginForm route", route);

	// On our success
	const [ onSuccess, modalProps ] = useOnSuccess();

	/**
	 * When they submit
	 */
	const onSubmit = async values => {
		try {
			debug( "Values", values );

			// Create a cookie request
			const cookieRequest = createCookieRequest();

			// Request to get a recaptcha JWT
			const result = await cookieRequest.postCaptcha( "login", "auth/login", values );

			debug("Got login result", result);

			// Call on success
			await onSuccess(result, cookieRequest, navigation, route)
		}
		catch( e ) {
			console.error("Error submitting", e);

			// Pop up
			Toast.show( {
				'text': e.message || e,
				'buttonText': "Dismiss",
				'type': "danger",
				'duration': 0
			} );
		}
	};

  return (
    <Container>
    
	<LoginSuccessPopup {...modalProps} />

    <Content style={styles.container}>

		
    <Formik
      validationSchema={loginFormSchema}
      onSubmit={onSubmit}
      initialValues={{ 'email' : "", 'password' : "" }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting
      }) => (

    <Form>
    	<H2 style={styles.header}>Sign-In To Jenerus</H2>

		<Button transparent onPress={makeNavigation( "RegisterScreen", navigation, route )}><Text>No account? Tap here to register.</Text></Button>

			<InputHelper
				style={styles.control}
				label="E-mail"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="email"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "email", 'autoCapitalize' : "none", 'keyboardType' : "email-address" }}
			/>

			<InputHelper
				style={styles.control}
				label="Password"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="password"
				values={values}
				itemProps={{ ...defaultItemProps, 'last' : true }}
				inputProps={{ 'autoCompleteType' : "password", 'secureTextEntry' : true }}
			/>
        
<CustomButton
				style={styles.control}
	isLoading={isSubmitting}
	block 
	onPress={handleSubmit}>
        <Text>Sign-In</Text>
      </CustomButton>
		<Button style={{ 'alignSelf' : "flex-end" }} transparent onPress={()=>navigation.navigate( "LoginForgotPassScreen", { 'shouldGoBack' : true } )}><Text style={{ 'color' : "red" }}>I forgot my password</Text></Button>
		</Form>
			
			)}
			</Formik>

</Content>
</Container>
   
  
  );
}

/**
 * The apple register form
 */
export function AppleRegisterForm({ navigation, route }) {
	debug("Apple RegisterForm route", route);

	// On our success
	const [ onSuccess, modalProps ] = useOnSuccess();
	// Keep track of whether or not we've already processed the route
	const isMounted = useRef( false );
	const formRef = useRef();


	// Custom getter for what the initial values should be
	const getInitialValues = function( route ) {
		const r = {
			//'email' : "",
			'name' : ""
		};
		
		if( route && route.params ) {
			// Split
			const { fullName } = route.params;

			// Email
			//r.email = email||"";

			// Name
			r.name = getFullNameFromAppleObject( fullName )||"";
		}

		debug( "Got initial values", r );

		return( r );
	};

	// Now our states
	const [ initialValues, setInitialValues ] = useState( getInitialValues( route ) );

	// Now some effects
	useEffect( ()=>{
		// WHen the route changes onlt he second time
		if( isMounted.current ) {
			setInitialValues( route ); // Set the new values

			// Reset the form
			formRef.current.resetForm();
		}
		else
			isMounted.current = true; // Next time we'll do tis
	}, [ route ] );

	/**
	 * When they submit
	 */
	const onSubmit = async values => {
		try {
			debug( "Values", values );

			// Create a cookie request
			const cookieRequest = createCookieRequest();

			// Parse from the params
			if( route && route.params )
				values = { ...pick( route.params, "identityToken", "nonce", "email" ), ...values };
			
			debug( "Posting values", values );
			

			// Request to get a recaptcha JWT
			const result = await cookieRequest.post( "auth/apple/loginWithIdToken", values );

			debug( "Got result", result );

			// Call on success
			await onSuccess( result, cookieRequest, navigation, route );
		}
		catch (e) {
			console.error("Error submitting", e);

			// Pop up
			Toast.show({
				'text': e.message || e,
				'buttonText': "Dismiss",
				'type': "danger",
				'duration': 0
			});
		}
	};

  return (
    <Container>

	<LoginSuccessPopup {...modalProps} />

    <Content style={styles.container}>
		
    <Formik
      validationSchema={appleFormSchema}
      onSubmit={onSubmit}
      initialValues={initialValues}
      innerRef={formRef}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting
      }) => (

    <Form>
    	<H2 style={styles.header}>Just a little bit more info</H2>
			<InputHelper
				style={styles.control}
				label="Your Full Name"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="name"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "name", 'autoCapitalize' : "words" }}
			/>

			{/*
			<InputHelper
				style={styles.control}
				label="E-mail"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="email"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "email", 'autoCapitalize' : "none", 'keyboardType' : "email-address" }}
			/>
			*/}

				<CustomButton
					style={styles.control}
					isLoading={isSubmitting}
					block 
					onPress={handleSubmit}>
					<Text>Sign-Up</Text>
				</CustomButton>

		</Form>
			
			)}
			</Formik>

		</Content>
		</Container>
	);
}

/**
 * The register form
 */
export function RegisterForm({ navigation, route }) {
	debug("RegisterForm route", route);

	// On our success
	const [ onSuccess, modalProps ] = useOnSuccess();

	/**
	 * When they submit
	 */
	const onSubmit = async values => {
		try {
			debug("Values", values);

			// Create a cookie request
			const cookieRequest = createCookieRequest();

			// Request to get a recaptcha JWT
			const result = await cookieRequest.postCaptcha( "register", "auth/signup", values );

			// Call on success
			await onSuccess(result, cookieRequest, navigation, route);
		}
		catch (e) {
			console.error("Error submitting", e);

			// Pop up
			Toast.show({
				'text': e.message || e,
				'buttonText': "Dismiss",
				'type': "danger",
				'duration': 0
			});
		}
	};

  return (
    <Container>

	<LoginSuccessPopup {...modalProps} />

    <Content style={styles.container}>
		
    <Formik
      validationSchema={registerFormSchema}
      onSubmit={onSubmit}
      initialValues={{ 'name' : "", 'username' : "", 'email' : "", 'password' : "", 'passwordConfirm' : "" }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting
      }) => (

    <Form>
    	<H2 style={styles.header}>Register With Jenerus</H2>
			<InputHelper
				style={styles.control}
				label="Your Full Name"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="name"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "name", 'autoCapitalize' : "words" }}
			/>

			<InputHelper
				style={styles.control}
				label="Desired Username"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="username"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "username", 'autoCapitalize' : "none" }}
			/>

			<InputHelper
				style={styles.control}
				label="E-mail"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="email"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "email", 'autoCapitalize' : "none", 'keyboardType' : "email-address" }}
			/>

			<InputHelper
				style={styles.control}
				label="Password"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="password"
				values={values}
				itemProps={defaultItemProps}
				inputProps={{ 'autoCompleteType' : "password", 'secureTextEntry' : true }}
			/>

			<InputHelper
				style={styles.control}
				label="Re-enter Password"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="passwordConfirm"
				values={values}
				itemProps={{ ...defaultItemProps, 'last' : true }}
				inputProps={{ 'autoCompleteType' : "password", 'secureTextEntry' : true }}
			/>
        
				<CustomButton
					style={styles.control}
					isLoading={isSubmitting}
					block 
					onPress={handleSubmit}>
					<Text>Sign-Up</Text>
				</CustomButton>
		<Button transparent block onPress={makeNavigation( "LogInScreen", navigation, route )}><Text>Already have an account? Tap here to log in.</Text></Button>
		</Form>
			
			)}
			</Formik>

		</Content>
		</Container>
	);
}

/**
 * The login or register navigator. Gatorgator.
 */
export default function() {
	return(
		<Stack.Navigator {...defaultNavigationOptions}>
			<Stack.Screen name="RegisterScreen"
				options={{ 'title' : "Register" }}
				component={RegisterForm}
			/>
			<Stack.Screen name="LogInScreen"
				options={{ 'title' : "Login" }}
				component={LoginForm}
			/>
			<Stack.Screen name="LoginForgotPassScreen"
				options={{ 'title' : "Recover Password" }}
				component={EmbeddedForgotPass}
			/>
			<Stack.Screen name="AppleRegisterFormScreen"
				options={{ 'title' : "More Info" }}
				component={AppleRegisterForm}
			/>
		</Stack.Navigator>
	);
}
