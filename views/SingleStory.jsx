import React, { Component } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Text, Container, Content, Button, Card, CardItem, Left, Body, Right, Thumbnail, Icon, View,Header, Footer, Segment, H2} from 'native-base';
import DefaultHeader from '../DefaultHeader';
import { Col, Row, Grid } from 'react-native-easy-grid';
import GoodbyePng from '../assets/pngs/goodbye.png';
export default class LayoutExample extends Component {
  render() {
    return (
      <Container>
       <DefaultHeader title="Story" />
       <Card transparent>
            <CardItem header>
              <Text>NativeBase</Text>
              <Right header>
                <H2>$34.00</H2>
              </Right>
            </CardItem>
        </Card>
        <Grid>
          <Row style={{height: 250 }}><Image source={GoodbyePng} style={{width: "100%", height: "100%", marginTop: '5%'}} resizeMode="contain" /></Row>
          <Card transparent
          
          >
            <CardItem header>
            <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="person" />
                  <Text>4 donors</Text>
                </Button>
              </Body>
              <Right>
                <Text>(Funds need) 2 days</Text>
              </Right>
            </CardItem>
        </Card>
          <Button  style={{margin:10 }} block success>
          
            <Text>Donate now</Text>
          </Button>
          <Segment style={{width:'100%'}}>
          <Button first>
            <Text>Info</Text>
          </Button>
          <Button>
            <Text>Cart</Text>
          </Button>
          <Button>
            <Text>Boosts</Text>
          </Button>
        </Segment>
        <Content>
          <Card>
            <CardItem header>
              <Text>NativeBase</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  //Your text here
                </Text>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text>GeekyAnts</Text>
            </CardItem>
         </Card>
        </Content>
        </Grid>
      </Container>
    );
  }
}