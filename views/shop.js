import React, { useRef, useEffect } from 'react';
import { View, SafeAreaView  } from 'react-native';
import { Container, Content, Button, Text, Form, Label, Item, Input ,Header, Left,Body, Title,Right  } from 'native-base';
//import WebView from 'react-native-webview';
import Draggable from 'react-native-draggable';
import { Ionicons } from '@expo/vector-icons';

import MessagingWebView from '../MessagingWebView';

import { code as InjectedJavaScript } from '../injected-scripts/script.json';

export default function ShopScreen( { navigation } ) {
/*
    const runFirst = `
      document.body.style.backgroundColor = 'red';
			console.log( "Runned." );
			window.ReactNativeWebView.postMessage( "We runned!" );
			document.addEventListener("message", console.log );
			window.addEventListener("message", console.log );
      true; // note: this is required, or you'll sometimes get silent failures
    `;
	*/

	// We'll need this
	const webViewRef = useRef();

	useEffect( ()=>{
		/*
		setTimeout( ()=>{
			const c = webViewRef.current.command( "Help me" );
			c.set( "friend", true );
			c.set( "foe", "toast" );
			c.set( "give", 5 );
			c.send();
		}, 5000 );
		*/

		// When we have a message
		webViewRef.current.on( "command", o=>{
			if( o.get( 'command' )=="documentReady" ) {
				// Sending bum command for timeout purpose
				webViewRef.current.command( "tester" ).sendPromise().then( console.log ).catch( console.error );

			}
		} );
	}, [] );

	// Notify the webview of loaded
	const onLoad = ()=>{
		// Send this out
		if( webViewRef.current )
			webViewRef.current.command( "loaded" ).send();
	};

	// When we are touched (oooh lala)
	const onBubbleTouch = async ()=>{
		try {
			const result = await webViewRef.current.command( "syncCart" ).sendPromise();

			console.log( "Got this cart", result );
		}
		catch( e ) {
			console.error( "Error syncing cart", e );
		}
	};

  return (

	<Container>
       <Header>
          <Left>
          <Button hasText transparent  onPress={() => navigation.toggleDrawer()}>
            <Ionicons name="menufold" />
            </Button>
            </Left>
          <Body>
            <Title>Shop</Title>
          </Body>
          <Right />
        </Header>

    <SafeAreaView style={{ 'flex' : 1 }}>
			<MessagingWebView
				ref={webViewRef}
				source={{ uri: 'http://instacart.com' }}
				onLoad={onLoad}
				onError={e=>console.error( "Errored", e )}

				injectedJavaScriptBeforeContentLoadedForMainFrameOnly={false}
				injectedJavaScriptForMainFrameOnly={false}

				injectedJavaScript={InjectedJavaScript}

				sharedCookiesEnabled={true}

				userAgent="Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148"
			/>

			<Draggable
				x={75}
				y={100}
				renderSize={56}
				renderColor='black'
				renderText='Jenerus'
				isCircle
				onShortPressRelease={onBubbleTouch}
			/>
		</SafeAreaView>

		</Container>
  
  );
}
