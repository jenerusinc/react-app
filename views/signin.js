import React from 'react';
import { Container, Content, Text,Form, Item, Label, Input, Toast,Header,Left, Body, Title, Right, Button  } from 'native-base';
import CustomButton from '../CustomButton';
import InputHelper from '../InputHelper';

import { createCookieRequest } from '../request';

import { PersistentKeyStorageKey } from '../constants';

import * as SecureStore from 'expo-secure-store';



//console.log( "Request in", request );

import { Formik } from 'formik';
import * as Yup from 'yup';

const loginFormSchema = Yup.object( {
  'email'    : Yup.string().required( "Please enter your e-mail" ).trim(),
  'password' : Yup.string().required( "Please enter your password" )
} );


export default function SignInScreen( { navigation } ) {
	/**
	 * When they submit
	 */
	const onSubmit = async values=>{
		try {
			console.log( "Values", values );

			// Create a cookie request
			const cookieRequest = createCookieRequest();

			// Request to get a recaptcha JWT
			const result = await cookieRequest.postCaptcha( "signin", "auth/login", values );

			console.log( "Got login result", result );

			// Let's create the persistent key
			const { key } = await cookieRequest.get( "user/me/createPersistentSession" );

			console.log( "Got the persistent key", key );

			// Store it
			await SecureStore.setItemAsync( PersistentKeyStorageKey, key );
		}
		catch( e ) {
			console.error( "Error submitting", e );

			// Pop up
			Toast.show( {
                'text' : e.message||e,
                'buttonText' : "Dismiss",
                'type' : "danger",
								'duration' : 0
              } );
		}
	};

  return (
    <Container>
       <Header>
          <Left>
            </Left>
          <Body>
            <Title>Sign up</Title>
          </Body>
          <Right />
        </Header>
    <Content>

		
    <Formik
      validationSchema={loginFormSchema}
      onSubmit={onSubmit}
      initialValues={{ 'email' : "", 'password' : "" }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting
      }) => (

    <Form>
			<InputHelper
				label="E-mail"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="email"
				values={values}
				inputProps={{ 'autoCompleteType' : "email" }}
			/>

			<InputHelper
				label="Password"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="password"
				values={values}
				inputProps={{ 'autoCompleteType' : "password", 'secureTextEntry' : true }}
			/>
        
<CustomButton
	isLoading={isSubmitting}
	block 
	onPress={handleSubmit}>
        <Text>Sign in</Text>
      </CustomButton>
		</Form>
			
			)}
			</Formik>

</Content>
</Container>
   
  
  );
}
