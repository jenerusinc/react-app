import React, { useState, useEffect } from 'react';

import * as atoms from '../atoms';
import { useRecoilState, useRecoilValue } from 'recoil';
import {PencilIcon} from '@primer/octicons-react'
import { Text, Container, Content, Button, Card, CardItem, Left, Body, Right, Thumbnail, Icon, View } from 'native-base';
import {Image} from "react-native";
import wrapForRecoil from '../wrapForRecoil';

import BackgroundPng from '../assets/pngs/background.png';

const debug = require( '../debug' )( "views:Home" );

import DefaultHeader from '../DefaultHeader';
import LoadingScreen from '../LoadingScreen';
import ErrorRetryScreen from '../ErrorRetryScreen';

import { locationToString, formatCurrency } from '../util';

import * as Progress from 'react-native-progress';

import { SvgUri } from 'react-native-svg';

import { fetchAsk } from '../request';

import { useNavigation } from '@react-navigation/native';

/**
 * The not logged in screen
 */
function NoAsk() {
	// Get the navigation from the context
	const navigation = useNavigation();

	return(
		<Content style={{marginTop: 15}}>
			
			<Text style={{ 'fontSize' : 30, 'fontWeight' : "bold",'fontFamily' : "Pier-Bold",   textAlign: 'center'}}>No story yet</Text>
			<Image source={BackgroundPng} style={{width: '100%', height:300, marginTop: 50, marginBottom: 10}} resizeMode="contain" />
			<Button rounded style={{alignSelf:'center'}} onPress={() => navigation.navigate('Story')}><Text style={{'fontWeight' : "bold",'fontFamily' : "Pier-Bold" }}>Start a story</Text></Button>
		</Content>
	);
}

/**
 * The logged in screen
 */
function AskCard() {
	// Get the navigation from the context
	const navigation = useNavigation();

	// The settable ask
	const [ ask, setAsk ] = useRecoilState( atoms.Ask );

	const [ loading, setLoading ] = useState( false );
	const [ errorLoading, setErrorLoading ] = useState();

	// Fetch the ask
	const getAsk = async ()=>{
		debug( "Loading our ask." );

		// Loading start
		setLoading( true );

		try {
			// There is no error loading anymore
			setErrorLoading( null );

			// Fetch the user ask
			const { ask } = await fetchAsk();

			debug( "We got the ask.", ask );
			
			// Now set the ask
			setAsk( ask );
		}
		catch( e ) {
			// Ruh roh
			setErrorLoading( e );
		}

		// Loading end
		setLoading( false );
	};

	// We've mounted. This will be when a user logs in or the app is loaded
	useEffect( ()=>{
		// Do we have no ask?
		if( !ask )
			getAsk(); // Get it
	}, [] );

	// Are we loading?
	if( loading )
		return( <LoadingScreen text="Getting your info..." /> );
	
	// Set the error
	if( errorLoading )
		return( <ErrorRetryScreen error={errorLoading} onRetry={getAsk} /> );

	// Do we not have an ask?
	if( !ask )
		return( <NoAsk /> );
	
	const {
		user,
		cart
	} = ask;
	
	debug( "Got uesr info.", user, ask );

	// Progress. Make sure it's not more than 1.
	const progress = Math.min( 1, cart.total_donated/cart.total_cost );

	debug( "Progress is", progress );

	return(
		<Content>

          <Card>
            <CardItem bordered>
              <Left>
		<SvgUri uri={user.avatarURL} width={50} height={50} />
                <Body>
                  <Text>{ask.anonymous ? user.username : user.name}</Text>
                  <Text note>{ locationToString( user.location) }</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody bordered>
              <Image source={{ 'uri' : ask.picture }} style={{height: 200, width: null, flex: 1}} resizeMode="contain" />
            </CardItem>
	    <CardItem>
		<Body>
			<Text>{ask.story}</Text>

			<View style={{ 'marginTop' : 16, 'width' : "100%" }}>
			<Progress.Bar color="green" borderColor="green" progress={progress} width={null} />
			</View>
		</Body>
	    </CardItem>
            <CardItem>
              <Body style={{ 'alignItems' : "center" }}>
                <Text>Donations</Text>
		<Text>{formatCurrency( cart.total_donated )}</Text>
              </Body>
              <Body style={{ 'alignItems' : "center" }}>
                <Text>Remaining</Text>
		<Text>{formatCurrency( cart.remaining )}</Text>
              </Body>
              <Body>
                <Button style={{ 'alignSelf' : "center" }} transparent onPress={()=>navigation.navigate( "Story" )}><Text>Revise</Text></Button>
              </Body>
            </CardItem>
          </Card>
        </Content>
	);
}

/**
 * Our home page.
 * Wrap it in suspense just in case
 */
export default wrapForRecoil( function() {
	// To see if the user is logged in
	const userKey = useRecoilValue( atoms.UserKey );
	const hasIssuedCard = useRecoilValue( atoms.HasIssuedCard );

	let component;

	// Use the userKey
	if( userKey ) 
		component = <AskCard />;
	else
		component = <NoAsk />;

	return(
		<Container>
			<DefaultHeader title="Your Story" />
			{component}
		</Container>
	);
} );
