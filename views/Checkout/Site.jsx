import React, { useRef, useEffect, useState } from 'react';

import { Animated, Easing } from 'react-native';

import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { Footer, Container, Content, Text, Toast, View, Button, Icon, FooterTab, Spinner, Tab, Tabs } from 'native-base';

import { Keyboard, ScrollView, TouchableOpacity, StyleSheet, Clipboard } from "react-native";

import { getStore } from '../../accepted-stores';

const debug = require( '../../debug' )( "views:Checkout:Site" );

import { formatCurrency, formatCreditCard } from '../../util';

import MessagingWebView from '../../MessagingWebView';

import { code as InjectedJavaScript } from '../../injected-scripts/script.json';

import { JenerusBlue, JenerusAquamarine, IsIOS, IsAndroid } from '../../constants';

import wrapForRecoil from '../../wrapForRecoil';
import CustomPopupModal from '../../CustomPopupModal';

//import UserAgent from 'react-native-user-agent';

const styles = StyleSheet.create( {
	'jenerusButton' : {
		'backgroundColor' : JenerusBlue,
		'color' : "white",
		'borderRadius' : 0,
		'flexDirection' : "row"
	},
	'jenerusButtonText' : {
		'color' : "black"
	},
	'popupText' : {
		'fontSize' : 24
	},
	'barColor' : {
		'color' : IsIOS ? "black" : "white"
	},
	'barColorDisabled' : {
		'color' : "grey"
	},

	  modalBackground: {
	    flex: 1,
	    justifyContent: "center",
	    alignItems: "center",
	    'backgroundColor' : "rgba( 0, 0, 0, 0.75 )"
	  },
	  modalView: {
	  	'padding' : 32,
		'backgroundColor' : "white",
		'borderRadius' : 8,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	  },
	  'dialogText' : {
	  	'marginBottom' : 16,
		'alignSelf' : "stretch",
		'fontSize' : 18
	  },
	  'dialogInner' : {
		'marginLeft' : 16,
		'marginRight' : 16
	  },
	  'autoFillButtonText' : {
	  	'color' : "white"
	  },
	  'autoFillButtonTextBold' : {
	  	'color' : "white",
		'fontWeight' : "bold"
	  },
	  'autoFillButton' : {
		  'margin' : 2,
		  'borderRadius' : 10,
		  'padding' : 4,
		  'alignItems' : "center",
		  'justifyContent' : "center",
		  'paddingLeft' : 8,
		  'paddingRight' : 8,
	  },
	  'autoFillButtonHeader' : {
	  	'color' : "black",
		'fontWeight' : "bold"
	  },
} );

styles.jenerusButtonDisabled = StyleSheet.compose(
	styles.jenerusButton,
	{ 'backgroundColor' : "grey" }
);

/**
 * The card hiding control
 */
/*
function HideCardInfo( { visible, onReveal } ) {
	// Should we hide?
	if( !visible )
		return( null );

	return(

			<View style={{ 'backgroundColor' : "white", 'position' : "absolute", 'top' : 0, 'bottom': 0, 'left' : 0, 'right' : 0, 'alignItems' : "center", 'justifyContent' : "center" }}>
				<Button onPress={onReveal} block transparent><Text>Reveal Info</Text></Button>
			</View>
	);
}
*/

/**
 * The card info row
 */
/*
function CardInfoRow( { left, right } ) {
	return(
	<>
			<Text style={{ 'fontWeight' : "bold", 'color' : "grey", 'width' : "50%" }}>
				{left}
			</Text>
			<Text style={{ 'width' : "50%" }}>
				{right}
			</Text>
			</>
	);
}
*/

/**
 * The information block
 */
/*
function InformationBlock( { info } ) {

	return(

		<View style={{ 'paddingLeft' : 16, 'paddingRight' : 16, 'flex' : 1, 'justifyContent' : "center" }}>
			<View style={{ 'flexDirection' : "row", 'flexWrap' : "wrap", 'alignItems' : "flex-start" }}>
				{info.map( ( [ left, right ], index )=><CardInfoRow left={left} right={right} key={index} /> )}
			</View>
		</View>
	);
}
*/

/**
 * Card info view
 */
/*
function CardInfoSlideUp( { onAutoComplete, visible, onClose, issuedCard } ) {
	// Our initial state
	const [ isVisible, setIsVisible ] = useState( visible );
	const [ isAnimating, setIsAnimating ] = useState( false );
	const [ shouldShowCard, setShouldShowCard ] = useState( false );
	const [ currentTab, setCurrentTab ] = useState( 0 );

	  const animation = useRef( new Animated.Value( 0 ) ).current  // Initial value for opacity: 0

	  const animateIn = ()=>{
	  	setIsAnimating( true );

	    Animated.timing(
	      animation,
	      {
	      	fromValue : 0,
		toValue: 1,
		duration: 250,
		'useNativeDriver' : false
	      }
	    ).start( ()=>setIsAnimating( false ) );
	  };

	  const animateOut = ()=>{
	  	setIsAnimating( true );

	    Animated.timing(
	      animation,
	      {
	      	fromValue : 1,
		toValue: 0,
		duration: 250,
		'useNativeDriver' : false
	      }
	    ).start( ()=>setIsAnimating( false ) );
	  };

		// Visibility change
	  useEffect( ()=>{
		if( visible==isVisible )
			return; // not an actual visibility change

		// new visibility
		setIsVisible( visible );

		if( visible )
			animateIn();
		else
			animateOut();

		// Set this
	  }, [ visible ] );

	const top = animation.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ "100%", "50%" ]  // 0 : 150, 0.5 : 75, 1 : 0
	      } );
	const opacity = animation.interpolate( {
		'inputRange' : [ 0, 1 ],
		'outputRange' : [ 0.0, 0.8 ]
	} );

	  // Are we visible?
	 if( !isVisible && !isAnimating )
	 	return( null ); // None control

	return(
		<>
			<Animated.View style={{ 'position' : "absolute", 'left' : 0, 'right' : 0, 'top' : 0, 'bottom' : 0, 'backgroundColor' : "black", opacity }}>
				<TouchableOpacity style={{ 'width' : "100%", 'height' : "100%" }} onPress={onClose} />
			</Animated.View>
				<Animated.View style={{ 'position' : "absolute", 'width' : "100%", 'height' : "50%", 'backgroundColor' : "white", top, 'borderTopColor' : JenerusBlue, 'borderTopWidth' : 2  }}>
					<TouchableOpacity style={{ 'alignSelf' : "stretch", 'justifyContent' : "center", 'alignItems' : "center", 'padding' : 8 }} onPress={onClose}><Icon type="FontAwesome" name="chevron-down" /></TouchableOpacity>
					<Tabs tabBarPosition="bottom" initialPage={currentTab} onChangeTab={tab=>setCurrentTab( tab.i )}>
						<Tab heading="Info">
							<View style={{ 'flex' : 1, 'justifyContent' : "center", 'alignItems' : "center" }}>
								<Text style={{ 'fontWeight' : "bold", 'color' : "grey" }}>Balance</Text>
								<Text>{formatCurrency( issuedCard.remaining )}</Text>
								{onAutoComplete ? <Button onPress={onAutoComplete}><Text>Autocomplete Now</Text></Button> : null}
							</View>
						</Tab>
						<Tab heading="Card">
							<InformationBlock info={[
									[ "Name", issuedCard.name ],
									[ "Number", formatCreditCard( issuedCard.number ) ],
									[ "Expiration", issuedCard.expiration ],
									[ "CVC", issuedCard.cvv ],
								]} />
							<HideCardInfo visible={!shouldShowCard} onReveal={()=>setShouldShowCard( true )} />
						</Tab>
						<Tab heading="Address">
							<InformationBlock info={[
									[ "Address", issuedCard.address1 ],
									[ "City", issuedCard.city ],
									[ "State", issuedCard.state ],
									[ "ZIP", issuedCard.zip ],
								]} />
							<HideCardInfo visible={!shouldShowCard} onReveal={()=>setShouldShowCard( true )} />
						</Tab>
					</Tabs>
				</Animated.View>
		</>
		);
}
*/

/**
 * Card info view
 */
function AutoCompleteSlideUp( { onAutoComplete, visible, onClose, canAutoComplete } ) {
	// Our initial state
	const [ isVisible, setIsVisible ] = useState( visible );
	const [ isAnimating, setIsAnimating ] = useState( false );

	  const animation = useRef( new Animated.Value( 0 ) ).current  // Initial value for opacity: 0

	  const animateIn = ()=>{
	  	setIsAnimating( true );

	    Animated.timing(
	      animation,
	      {
	      	fromValue : 0,
		toValue: 1,
		duration: 250,
		'useNativeDriver' : false
	      }
	    ).start( ()=>setIsAnimating( false ) );
	  };

	  const animateOut = ()=>{
	  	setIsAnimating( true );

	    Animated.timing(
	      animation,
	      {
	      	fromValue : 1,
		toValue: 0,
		duration: 250,
		'useNativeDriver' : false
	      }
	    ).start( ()=>setIsAnimating( false ) );
	  };

		// Visibility change
	  useEffect( ()=>{
		if( visible==isVisible )
			return; // not an actual visibility change

		// new visibility
		setIsVisible( visible );

		if( visible )
			animateIn();
		else
			animateOut();

		// Set this
	  }, [ visible ] );

	const top = animation.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ "100%", "50%" ]  // 0 : 150, 0.5 : 75, 1 : 0
	      } );
	const opacity = animation.interpolate( {
		'inputRange' : [ 0, 1 ],
		'outputRange' : [ 0.0, 0.8 ]
	} );

	  // Are we visible?
	 if( !isVisible && !isAnimating )
	 	return( null ); // None control

	return(
		<>
			<Animated.View style={{ 'position' : "absolute", 'left' : 0, 'right' : 0, 'top' : 0, 'bottom' : 0, 'backgroundColor' : "black", opacity }}>
				<TouchableOpacity style={{ 'width' : "100%", 'height' : "100%" }} onPress={onClose} />
			</Animated.View>
				<Animated.View style={{ 'position' : "absolute", 'width' : "100%", 'height' : "50%", 'backgroundColor' : "white", top, 'borderTopColor' : JenerusBlue, 'borderTopWidth' : 2  }}>
					<TouchableOpacity style={{ 'alignSelf' : "stretch", 'justifyContent' : "center", 'alignItems' : "center", 'padding' : 8 }} onPress={onClose}><Icon type="FontAwesome" name="chevron-down" /></TouchableOpacity>

					<View style={{ 'flex' : 1, 'justifyContent' : "center", 'alignItems' : "center" }}>
						<Text style={[ styles.popupText, { 'textAlign' : "center" } ]}>We are ready to auto fill in your Jenerus one-time credit card for you.</Text>
						{canAutoComplete ?
							<Text style={[ styles.popupText, { 'textAlign' : "center" } ]}>Tap the button below and we'll autocomplete now.</Text>
							:
							<Text style={[ styles.popupText, { 'textAlign' : "center" } ]}>At the checkout payment screen, add a new credit card and we'll autocomplete it for you.</Text>
							}
					</View>
					{canAutoComplete ?
						<CreditCardButton onPress={onAutoComplete} block style={styles.jenerusButton} text="Autocomplete Now" /> : null}

				</Animated.View>
		</>
		);
}

/**
 * Our autocomplete bar
 */
function AutoCompleteBar( { issuedCard, onAutoComplete, visible } ) {
	// Belt of visibility
	if( !visible )
		return( null );
	
	const card = [
			[ "Name", issuedCard.name ],
			[ "Number", formatCreditCard( issuedCard.number ) ],
			[ "Expiration", issuedCard.expiration ],
			[ "CVC", issuedCard.cvv ],
	];
	const address = [
		[ "Address1", issuedCard.address1 ],
		[ "City", issuedCard.city ],
		[ "State", issuedCard.state ],
		[ "ZIP", issuedCard.zip ],
	];

	const copyValue = value=>{
		Clipboard.setString( value );

		Toast.show( {
			'text' : "Copied!",
			'type' : "success",
			'position' : "top"
	      } );
	};

	// Return it all
	return(
		<View style={{ 'height' : 60 }}>
			<ScrollView horizontal={true}>
				{onAutoComplete ? <TouchableOpacity
					style={[ styles.autoFillButton, { 'backgroundColor' : "red" } ]}
					onPress={onAutoComplete}>
						<Text style={styles.autoFillButtonTextBold}>Autofill</Text>
					</TouchableOpacity> : null}
				<View style={[ styles.autoFillButton, { 'alignItems' : "flex-end" } ]}>
					<Text style={styles.autoFillButtonHeader}>Card</Text>
					<Text style={styles.autoFillButtonHeader}>Info</Text>
				</View>
				{card.map( ( [ label, value ], index )=><TouchableOpacity onPress={()=>copyValue( value )} style={[ styles.autoFillButton, { 'backgroundColor' : "blue" } ]} key={index}>
					<Text style={{ 'color' : "white" }}>{label}</Text>
					<Text style={{ 'color' : "white", 'fontWeight' : "bold" }}>{value}</Text>
				</TouchableOpacity> )}
				<View style={[ styles.autoFillButton, { 'alignItems' : "flex-end" } ]}>
					<Text style={styles.autoFillButtonHeader}>Address</Text>
					<Text style={styles.autoFillButtonHeader}>Info</Text>
				</View>
				{address.map( ( [ label, value ], index )=><TouchableOpacity onPress={()=>copyValue( value )} style={[ styles.autoFillButton, { 'backgroundColor' : "green" } ]} key={index}>
					<Text style={styles.autoFillButtonText}>{label}</Text>
					<Text style={styles.autoFillButtonTextBold}>{value}</Text>
				</TouchableOpacity> )}
			</ScrollView>
		</View>
	);
}

/**
 * Button for card
 */
function CreditCardButton( props ) {
	// Split
	const {
		text,
		style,
		...others
		} = props;
	
	// Go
	return(
		<Button style={[ { 'backgroundColor' : "green", 'borderRadius' : 0 }, style ]} {...others}>
			<Icon name="credit-card" type="FontAwesome" style={{ 'color' : "white" }} />
			{text ? <Text>{text}</Text> : null}
		</Button>
	);
}

/**
 * Our checkout
 */
export default wrapForRecoil( function( { navigation, route } ) {
	// We'll need this
	const webViewRef = useRef();
	const [ isLoading, setIsLoading ] = useState( false );
	const [ canGoBack, setCanGoBack ] = useState( false );
	const [ canGoForward, setCanGoForward ] = useState( false );
	const [ currentURL, setCurrentURL ] = useState();

	// Page state properties
	const [ isCartVisible, setIsCartVisible ] = useState( false );
	const [ canAutoComplete, setCanAutoComplete ] = useState( false );
	const [ isPaymentPageVisible, setIsPaymentPageVisible ] = useState( false );

	// Break out all of this stuff
	const { issuedCard } = route.params;
	const { ask } = issuedCard;
	const { cart } = ask;

	debug( "We have issuedCard=", issuedCard, "ask=", ask, "cart=", cart );

	// Card stuff
	const [ store ] = useState( getStore( cart.store_name ) );
	const [ webViewSourceURI, setWebViewSourceURI ] = useState( store.url );

	// Modals
	const [ isTapForCartVisible, setIsTapCartVisible ] = useState( false );
	const [ isTapCheckoutVisible, setIsTapCheckoutVisible ] = useState( false );
	const [ isSlideUpVisible, setIsSlideUpVisible ] = useState( false );
	const [ isPaymentFormVisible, setIsPaymentFormVisible ] = useState( false );
	const alreadySawPaymentModal = useRef( false );

	// We only want to see the other modal once
	const alreadySawFinishCheckoutModal = useRef( false );
	const [ isFinishCheckoutModalVisible, setIsFinishCheckoutModalVisible ] = useState( false );
	//const [ isAutoCompleteModalVisible, setIsAutoCompleteModalVisible ] = useState( false );
	
	const alreadySawDisclaimer = useRef( false );
	const [ isDisclaimerVisible, setIsDisclaimerVisible ] = useState( false );
	
	// Keep track of the keyboard
	const [ isKeyboardVisible, setIsKeyboardVisible ] = useState( false);
	

	// Got it
	useEffect( () => {
		const keyboardShown = ()=>{
			debug( "Keyboard on" );
			setIsKeyboardVisible( true );
		};
		const keyboardHidden = ()=>{
			debug( "Keyboard off" );
			setIsKeyboardVisible( false );
		};

		Keyboard.addListener( 'keyboardDidShow', keyboardShown );
		Keyboard.addListener( 'keyboardDidHide', keyboardHidden );

		// cleanup function
		return( () => {
			Keyboard.removeListener( 'keyboardDidShow', keyboardShown );
			Keyboard.removeListener( 'keyboardDidHide', keyboardHidden );
		} );
	}, [] );

	// The dreadded autocomplete function
	const onAutoComplete = async ()=>{
		try {
			// Shorthand
			const { 'current' : webView } = webViewRef;

			// not doing much autocompleting if this happens
			if( !webView )
				return;

			// Now issue a command
			await webView.command( "autoCompletePayment" ).set( 'issuedCard', issuedCard ).sendPromise();

			// Say bye bye
			setIsSlideUpVisible( false );

			// Did we not already see this?
			if( !alreadySawFinishCheckoutModal.current ) {
				// Don't let it get shown again
				alreadySawFinishCheckoutModal.current = true;
				// Show the other on
				setIsFinishCheckoutModalVisible( true );
			}
		}
		catch( e ) {
			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
		      } );
		}
	};

	// On mount
	useEffect( ()=>{
		navigation.setOptions( {
			'headerLeft' : <Button transparent onPress={()=>navigation.goBack()}><Icon type="FontAwesome" name="times" /></Button>
		} );

		// Show the credit card tooltip
		setIsTapCartVisible( true );
	}, [] );
	
	useEffect( ()=>{
		debug( "webViewSourceURI=", webViewSourceURI );
	}, [ webViewSourceURI ] );

	// Is the slide up now visible?
	useEffect( ()=>{
		// Make sure we dismiss the keyboard
		if( isSlideUpVisible )
			Keyboard.dismiss();
	}, [ isSlideUpVisible ] );

	// Can we autocomplete?
	useEffect( ()=>{
		// Autocompelte change. Thing it.
		if( canAutoComplete && !alreadySawDisclaimer.current ) {
			alreadySawDisclaimer.current = true;
			setIsDisclaimerVisible( true );
		}
	}, [ canAutoComplete ] );

	// When the URL changes
	useEffect( ()=>{
		// Cart is not visible if the URL changes
		setIsCartVisible( false );
		// If the page changed chances are these are no more
		setCanAutoComplete( false );
		setIsPaymentPageVisible( false );

		// Current URL has changed
	}, [ currentURL ] );

	// When the cart view has changed
	useEffect( ()=>{
		// Just chain these values closely together
		setIsTapCheckoutVisible( isCartVisible );
	}, [ isCartVisible ] );

	// Make sure we only show the payment modal once
	useEffect( ()=>{
		// Is it visible now?
		if( isPaymentPageVisible && !alreadySawPaymentModal.current ) {
			alreadySawPaymentModal.current = true; // Mark as already seen
			setIsPaymentFormVisible( true );
		}
	}, [ isPaymentPageVisible ] );

	// On command
	const onCommand = o=>{
		switch( o.get( "command" ) ) {
			case "cartVisibilityChange":
				setIsCartVisible( o.get( "visible" ) );
				break;
			case "paymentPageVisibilityChange":
				setCanAutoComplete(
					// Is this not instacart.com
					cart.store_name!="instacart.com" ||
					IsIOS &&
					o.get( "canAutoComplete" )
				);
				setIsPaymentPageVisible( o.get( "visible" ) );
				break;
		}
	};


	  return (
  	<Container>
			<View style={{ 'height' : 1, 'width' : "100%", 'borderWidth' : 1 }} />
		
		<Container style={{ 'flex' : 1 }}>
    {/*<SafeAreaView style={{ 'flex' : 1 }}>*/}
    			<View style={{ 'flex' : 1 }}>
				<MessagingWebView
					ref={webViewRef}
					source={{ 'uri' : webViewSourceURI }}
					onCommand={onCommand}
					onError={e=>debug( "Errored", e )}
					onLoadStart={()=>setIsLoading( true )}
					onLoadEnd={()=>setIsLoading( false )}
					onNavigationStateChange={( { canGoBack, canGoForward, url } )=>{
						debug( "New current URL", url );

						// Their current URL
						setCurrentURL( url );
						// Set the back and forward stats
						setCanGoBack( canGoBack );
						setCanGoForward( canGoForward );
					}}


					injectedJavaScript={InjectedJavaScript}


				/>
			</View>
			<View style={{ 'height' : 1, 'width' : "100%", 'borderWidth' : 1 }} />
			
			<AutoCompleteBar issuedCard={issuedCard} visible={true} onAutoComplete={canAutoComplete ? onAutoComplete : null} visible={isPaymentPageVisible} />

			{isKeyboardVisible ? null :
			<Footer>
				<FooterTab>
						<Button transparent disabled={!canGoBack} onPress={()=>webViewRef.current.native.goBack()}><Icon name="arrow-back" type="Ionicons" style={canGoBack ? styles.barColor : styles.barColorDisabled } /></Button>
						{isLoading ?
						<Button transparent><Spinner /></Button>
						:
						<Button transparent><Icon name="refresh" style={styles.barColor} onPress={()=>webViewRef.current.native.reload()} type="Ionicons" /></Button>}
						<Button transparent disabled={!canGoForward}><Icon name="arrow-forward" onPress={()=>webViewRef.current.native.goForward()} type="Ionicons" style={canGoForward ? styles.barColor : styles.barColorDisabled } /></Button>
						{/*
						<CreditCardButton onPress={()=>setIsSlideUpVisible( true )} />
						*/}
				</FooterTab>
			</Footer>}

			{/* When they first open */}
			<CustomPopupModal onCancel={()=>setIsTapCartVisible( false )} visible={isTapForCartVisible} innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>You have {formatCurrency( issuedCard.remaining )} to spend.</Text>
				<Text style={styles.dialogText}>Open your cart to get started.</Text>
				<Button block onPress={()=>setIsTapCartVisible( false )}><Text>Okay</Text></Button>
			</CustomPopupModal>
			{/* When they open the cart */}
			<CustomPopupModal onCancel={()=>setIsTapCheckoutVisible( false )} visible={isTapCheckoutVisible} innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>Confirm your cart and then check out.</Text>
				<Text style={styles.dialogText}>We'll see you again when it's time to enter your Jenerus one-time credit card.</Text>
				<Button block onPress={()=>setIsTapCheckoutVisible( false )}><Text>Okay</Text></Button>
			</CustomPopupModal>
			{/* When the payment form is visible */}
			<CustomPopupModal onCancel={()=>setIsPaymentFormVisible( false )} visible={isPaymentFormVisible} innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>Click to add a new payment method.</Text>
				<Text style={styles.dialogText}>This way you can enter your Jenerus one-time credit card information.</Text>
				{/*
				<Text style={styles.dialogText}>We'll put your information on the bottom. Tap an item to copy. Swipe left to reveal more.</Text>
				*/}
				<Button block onPress={()=>setIsPaymentFormVisible( false )}><Text>Okay</Text></Button>
			</CustomPopupModal>
			{/* The autocompleter modal */}
			{/*
			<CustomPopupModal onCancel={()=>setIsAutoCompleteModalVisible( false )} visible={isAutoCompleteModalVisible} innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>Would you like us to autocomplete this with your Jenerus credit card info?</Text>
				<Text style={styles.dialogText}>There may be some additional things you'll have to enter manually, but we can fill our what we can.</Text>
				<Text style={styles.dialogText}>You can always view your Jenerus credit card by tapping the</Text>
				<CreditCardButton style={{ 'alignSelf' : "center", 'marginBottom' : 16 }} />
				<Text style={styles.dialogText}>button on the bottom of the screen.</Text>
				<Button block styles={{ 'marginBottom' : 16 }} onPress={onAutoComplete}><Text>Autocomplete</Text></Button>
				<Button block onPress={()=>setIsAutoCompleteModalVisible( false )}><Text>Cancel</Text></Button>
			</CustomPopupModal>
			*/}
			{/* The final message */}
			<CustomPopupModal onCancel={()=>setIsFinishCheckoutModalVisible( false )} visible={isFinishCheckoutModalVisible} innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>We have autocompleted your credit card information.</Text>
				<Text style={styles.dialogText}>From here, please make sure that all of your other information is correct and finish your checkout.</Text>
				<Button block onPress={()=>setIsFinishCheckoutModalVisible( false )}><Text>Okay</Text></Button>
			</CustomPopupModal>
			
			{/* the payment disclaimer thingy */}
			<CustomPopupModal onCancel={()=>setIsDisclaimerVisible( false )} visible={isDisclaimerVisible} innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>Your one-time Jenerus credit card info is below.</Text>
				<Text style={styles.dialogText}>Tap on an item to copy. Swipe left to reveal more.</Text>
				<Button block onPress={()=>setIsDisclaimerVisible( false )}><Text>Okay</Text></Button>
			</CustomPopupModal>
		</Container>

		{/*
		<CardInfoSlideUp onClose={()=>setIsSlideUpVisible( false )} visible={isSlideUpVisible} issuedCard={issuedCard} />
		*/}

		{/*
		<AutoCompleteSlideUp onClose={()=>setIsSlideUpVisible( false )} visible={isSlideUpVisible} onAutoComplete={onAutoComplete} canAutoComplete={canAutoComplete} />
		*/}
 	</Container> 
  );
} )
