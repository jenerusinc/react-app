import React from 'react';

import { Container, Header, Content, Card, CardItem, Text, Body, Button, Icon, H1 } from "native-base";

import { useRecoilValue } from 'recoil';
import * as atoms from '../../atoms';

import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../../DefaultHeader';
import wrapForRecoil from '../../wrapForRecoil';

import { getStoreName } from '../../accepted-stores';

import { formatCurrency } from '../../util';

const debug = require( '../../debug' )( "views:Checkout:index" );

import Site from './Site';

const Stack = createStackNavigator();


/**
 * Go to the cherkout
 */
function GoToCheckout( { navigation } ) {
	// Get this global
	const issuedCard = useRecoilValue( atoms.IssuedCard );

	debug( "issuedCard", issuedCard );

	// Shorthands
	const { ask } = issuedCard;
	const { cart } = ask;

	// Shorthand this
	const storeName = getStoreName( cart.store_name );

	const onGo = ()=>{
		navigation.navigate( "CheckoutWebsite", { issuedCard } );
	};

	// Now show
	return(
		<Container style={{ 'flexDirectin' : "column", 'alignItems' : "stretch", 'justifyContent' : "center", 'padding' : 16 }}>
			{/*
			<Text>You have {formatCurrency( issuedCard.remaining )} in a one-time use Jenerus credit card to spend at {storeName}.</Text>
			*/}
			<H1 style={{ 'marginBottom' : 16, 'textAlign' : "center" }}>Checkout at {storeName} with Jenerus in a few easy steps</H1>
			<Button block success onPress={onGo}><Text>Get Started</Text><Icon type="FontAwesome" name="angle-right" /></Button>
		</Container>
	);
}

//import SlideShow from './SlideShow';


/**
 * Container
 */
export default wrapForRecoil( function( { navigation } ) {
	//return( <SlideShow /> );
	// Get the user key
	const userKey = useRecoilValue( atoms.UserKey );

	// If we get logged out then don't render anything
	if( !userKey )
		return( null );

	return(
		<Stack.Navigator {...defaultNavigationOptions}>
			<Stack.Screen name="GoToCheckout"
				options={{ 'title' : "Start Checkout" }}
				component={GoToCheckout}
				/>
			<Stack.Screen name="CheckoutWebsite"
				options={{ 'title' : "Website" }}
				component={Site}
				/>
		</Stack.Navigator>
	);
} )
