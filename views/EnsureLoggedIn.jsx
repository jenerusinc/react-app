import React from 'react';

import LoginOrRegister from './LoginOrRegister';

import { Container, Text, H2, Button, View, Grid, Col, H1 } from 'native-base';

import { StyleSheet, Image } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createHeaderScreenOptions } from '../DefaultHeader';

import FamilyValuesPng from '../assets/pngs/family-values.png';

import GoogleSignInButton from '../GoogleSignInButton';
import AppleSignInButton from '../AppleSignInButton';

const debug = require( '../debug' )( "views:EnsureLoggedIn" );

const Stack = createStackNavigator();

import wrapForRecoil from '../wrapForRecoil';


/**
 * Confirm register
 */
export const EnsureLoggedIn = wrapForRecoil( function( { navigation, route } ) {
	debug( "We got route", route );

	// Navigate away
	const navigateTo = function( to ) {
		const params = {
			'params' : {
				'screen' : route.params&&route.params.screen
			}
		};
	
		// Which screen go we to?
		if( to=="register" )
			params.screen = "RegisterScreen";
		else
			params.screen = "LogInScreen";

		// Now navigation
		navigation.navigate( "LoginOrRegister", params );
	}

	return(
		<Container style={{ 'padding' : 16 }}>
			<Image source={FamilyValuesPng} resizeMode="contain" style={{width: '100%', height: '45%', marginBottom: "0%"}} />
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignItems' : "center" , 'marginTop': '0%'}}>
						<H1 style={{ 'textAlign' : "center", 'marginBottom' : 16 }}>Let's get started!</H1>
						<Button rounded success block onPress={()=>navigateTo( "register" )} style={{  'marginBottom' : 8 }}><Text>Sign-Up</Text></Button>
						<H2 style={{ 'marginBottom' : 8 }}>or</H2>
						<Button rounded success block onPress={()=>navigateTo( "signin" )} style={{ 'marginBottom' : 16 }}><Text>Sign-In</Text></Button>
						<GoogleSignInButton navigation={navigation} route={route} />
						<AppleSignInButton style={{ 'marginTop' : 16 }} navigation={navigation} route={route} />
					</View>
				</Col>
			</Grid>

		</Container>
	);
} );

/**
 * Our default UI
 */
export default function( { route } ) {
	debug( "Got initial params", route );

	return(
		<Stack.Navigator>
			<Stack.Screen name="EnsureLoggedInScreen"
				options={createHeaderScreenOptions( { 'title' : "Sign-In" } )}
				component={EnsureLoggedIn}
				initialParams={route.params}
				/>
			{/* Can we add this? */}
			<Stack.Screen name="LoginOrRegister"
				options={{ 'headerShown' : false }}
				component={LoginOrRegister}
				/>
		</Stack.Navigator>
	);
}
