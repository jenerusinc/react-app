import React, { useRef, useEffect, useState } from 'react';

import { Container, Content, Text, Toast, H1, H2, H3, View, Grid, Col, Button } from 'native-base';
import { defaultNavigationOptions } from '../../../DefaultHeader';

import Site from './Web';
import {Image} from 'react-native'
import * as atoms from '../../../atoms';
import { useRecoilValue } from 'recoil';

import ShoppingPng from '../../../assets/pngs/shopping.png';

import { createStackNavigator } from '@react-navigation/stack';

import SlideShow from '../SlideShow';

const debug = require( '../../../debug' )( "views:shop:index" );

// For the modal
const ModalNavigator = createStackNavigator();

function ListAndItems( { items, ...others } ) {
	return( <View {...others}>{
		items.map( ( text, i )=>
				<View key={i} style={{ 'flexDirection' : "row", 'paddingLeft' : 8 }}>
					{/*<Text>{'\u2022'}</Text> */}
					<Text>{i+1}.</Text>
					<Text style={{ 'flex' : 1, 'paddingLeft' : 5 }}>{text}</Text>
				</View> )
		}</View>
	);
}

/**
 * Our default text showerererer
 */
function ShowDetails( { navigation, route } ) {
	debug( "We have route", route );

	// Get this
	const hasSeenSlideShow = useRecoilValue( atoms.SeenSlideShow );

	let cartID, goNext;

	if( route&&route.params ) {
		// Get the params
		( {
			cartID,
			goNext
		} = route.params );
	}

	// On first open
	useEffect( ()=>{
		// I fwe haven't seen it then we must
		if( !hasSeenSlideShow )
			navigation.navigate( "SlideShow", { 'screen' : route.name } );
	}, [] );

	// Should we progress?
	useEffect( ()=>{
		debug( "Should we go?", cartID, goNext );

		if( cartID&&goNext )
			navigation.navigate( "AskForm", { cartID } );
	}, [ cartID, goNext ] );

	const open = site=>{
		switch( site ) {
			case "grocery.walmart.com":
				navigation.navigate( "ShopSyncDialog", {
					'screen' : "Shop",
					'params' : { 'site' : "grocery.walmart.com" }
				} );
				break;
			case "instacart.com":
				navigation.navigate( "ShopSyncDialog", {
					'screen' : "Shop",
					'params' : { 'site' : "instacart.com" }
				} );
				break;
		}
	};

	return(
		<Container style={{ 'padding' : 8, }}>
			<View style={{ 'flexDirection' : "column", 'flex' : 1 }}>
				<View style={{ 'flex' : 1, 'justifyContent' : "center" }}>
					<Image source={ShoppingPng} style={{ 'width' : "100%", 'height' : "100%" }} resizeMode="contain" />
				</View>

				<View style={{ 'flex' : 1 }}>
					<View style={{ 'justifyContent' : "center", 'alignItems' : "center", 'flex' : 1 }}>
							<H1 style={{ 'marginBottom' : 8 }}>Shop Now</H1>

							<Button block onPress={()=>open( "grocery.walmart.com" )}><Image source={{uri: 'https://logo.clearbit.com/walmart.com'}} /><Text> Walmart Grocery</Text></Button>
							<Text>or</Text>
							<Button block onPress={()=>open( "instacart.com" )}><Text>Instacart</Text></Button>

					</View>
					<View>
						<Button block transparent onPress={()=>navigation.navigate( "AskForm" )}><Text>Already did this step</Text></Button>
					</View>
				</View>
			</View>
		</Container>
	);
}

/**
 * Export the stack screen
 */
export default function() {
	return(
			<ModalNavigator.Navigator mode="modal" {...defaultNavigationOptions}>
				<ModalNavigator.Screen name="ShopSyncDetails" component={ShowDetails} options={{ 'title' : "Shop Now" }} />
				<ModalNavigator.Screen name="ShopSyncDialog" component={Site} options={{ headerShown: false }} />
				<ModalNavigator.Screen name="SlideShow" component={SlideShow} options={{ headerShown: false }} />
			</ModalNavigator.Navigator>
	);
}
