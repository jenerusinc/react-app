import React, { useRef, useEffect, useState } from 'react';

import { Animated, Easing } from 'react-native';

import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { Footer, Container, Content, Text, Toast, View, Button, Icon, FooterTab, Spinner } from 'native-base';
import { headerScreenOptions } from '../../../../DefaultHeader';
//import WebView from 'react-native-webview';
//import Draggable from 'react-native-draggable';
import BlackLogoNoBgSvg from '../../../../assets/svgs/black-icon-no-bg.svg';

import { getStore } from '../../../../accepted-stores';

const debug = require( '../../../../debug' )( "views:shop" );

import MessagingWebView from '../../../../MessagingWebView';

import { code as InjectedJavaScript } from '../../../../injected-scripts/script.json';

import { JenerusBlue, JenerusAquamarine, IsIOS, MinimumCartAmount } from '../../../../constants';

import { StyleSheet, TouchableOpacity, Modal } from 'react-native';

import CustomPopupModal from '../../../../CustomPopupModal';

import { formatCurrency } from '../../../../util';
import { sumBy } from 'lodash';

//import UserAgent from 'react-native-user-agent';

const styles = StyleSheet.create( {
	'jenerusButton' : {
		'backgroundColor' : JenerusBlue,
		'color' : "white",
		'borderRadius' : 0,
		'flexDirection' : "row"
	},
	'jenerusButtonText' : {
		'color' : "black"
	},
	'popupText' : {
		'fontSize' : 24
	},
	'barColor' : {
		'color' : IsIOS ? "black" : "white"
	},
	'barColorDisabled' : {
		'color' : "grey"
	},

	  modalBackground: {
	    flex: 1,
	    justifyContent: "center",
	    alignItems: "center",
	    'backgroundColor' : "rgba( 0, 0, 0, 0.75 )"
	  },
	  modalView: {
	  	'padding' : 32,
		'backgroundColor' : "white",
		'borderRadius' : 8,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	  },
	  'dialogText' : {
	  	'marginBottom' : 16,
		'alignSelf' : "stretch"
	  },
	  'dialogInner' : {
		'marginLeft' : 16,
		'marginRight' : 16
	  }
} );

styles.jenerusButtonDisabled = StyleSheet.compose(
	styles.jenerusButton,
	{ 'backgroundColor' : "grey" }
);

/**
 * Our sync view
 */
function SlideUpSync( { onSyncNow, visible, onClose } ) {
	// Our initial state
	const [ isVisible, setIsVisible ] = useState( visible );
	const [ isAnimating, setIsAnimating ] = useState( false );

	  const animation = useRef( new Animated.Value( 0 ) ).current  // Initial value for opacity: 0

	  const animateIn = ()=>{
	  	setIsAnimating( true );

	    Animated.timing(
	      animation,
	      {
	      	fromValue : 0,
		toValue: 1,
		duration: 250,
		'useNativeDriver' : false
	      }
	    ).start( ()=>setIsAnimating( false ) );
	  };

	  const animateOut = ()=>{
	  	setIsAnimating( true );

	    Animated.timing(
	      animation,
	      {
	      	fromValue : 1,
		toValue: 0,
		duration: 250,
		'useNativeDriver' : false
	      }
	    ).start( ()=>setIsAnimating( false ) );
	  };

		// Visibility change
	  useEffect( ()=>{
		if( visible==isVisible )
			return; // not an actual visibility change

		// new visibility
		setIsVisible( visible );

		if( visible )
			animateIn();
		else
			animateOut();

		// Set this
	  }, [ visible ] );

	const top = animation.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ "100%", "50%" ]  // 0 : 150, 0.5 : 75, 1 : 0
	      } );
	const opacity = animation.interpolate( {
		'inputRange' : [ 0, 1 ],
		'outputRange' : [ 0.0, 0.8 ]
	} );

	  //debug( "Got bg value", backgroundColor );

	  // Are we visible?
	 if( !isVisible && !isAnimating )
	 	return( null ); // None control

	return(
		<>
			<Animated.View style={{ 'position' : "absolute", 'left' : 0, 'right' : 0, 'top' : 0, 'bottom' : 0, 'backgroundColor' : "black", opacity }}>
				<TouchableOpacity style={{ 'width' : "100%", 'height' : "100%" }} onPress={onClose} />
			</Animated.View>
				<Animated.View style={{ 'position' : "absolute", 'width' : "100%", 'height' : "50%", 'backgroundColor' : "white", top, 'borderTopColor' : JenerusBlue, 'borderTopWidth' : 2  }}>
					<TouchableOpacity style={{ 'alignSelf' : "stretch", 'justifyContent' : "center", 'alignItems' : "center", 'padding' : 8 }} onPress={onClose}><Icon type="FontAwesome" name="chevron-down" /></TouchableOpacity>
					<View style={{ 'flex' : 1, 'justifyContent' : "center", 'alignItems' : "center" }}>
						<Text style={[ styles.popupText, { 'textAlign' : "center" } ]}>Are you done shopping?</Text>
						<Text style={[ styles.popupText, { 'textAlign' : "center" } ]}>Tap below to sync your cart to Jenerus.</Text>
					</View>
					<Button onPress={onSyncNow} style={{ 'borderRadius' : 0 }} block style={styles.jenerusButton}><BlackLogoNoBgSvg width={30} height={30} /><Text style={styles.jenerusButtonText}>Sync Now</Text></Button>
				</Animated.View>
		</>
		);
}

function GlowButton( { onPress, disabled, full } ) {
	const textStyle = {
		'color' : "white"
	};

	if( full )
		textStyle.fontSize = 16;
	
	// If the cart isn't shown then no buttn for now
	if( !full )
		return( null );

	return( <Button onPress={onPress} style={disabled ? styles.jenerusButtonDisabled : styles.jenerusButton}><BlackLogoNoBgSvg width={30} height={30} /><Text style={styles.jenerusButtonText}>{full ? "Sync To Jenerus Now" : "Sync Now"}</Text></Button> );

	// The fade animation value
	  /*
	  const fadeAnim = useRef( new Animated.Value( 0 ) ).current  // Initial value for opacity: 0

	  let animateIn, animateOut;

	  animateIn = ()=>{
	    Animated.timing(
	      fadeAnim,
	      {
	      	fromValue : 0,
		toValue: 1,
		duration: 2000,
		'useNativeDriver' : false
	      }
	    ).start( animateOut )
	  };
	  animateOut = ()=>{
	    Animated.timing(
	      fadeAnim,
	      {
	      	fromValue : 1,
		toValue: 0,
		duration: 2000,
		'useNativeDriver' : false
	      }
	    ).start( animateIn )
	  };

		// Use an effect
	  useEffect( ()=>{
		animateIn();
	  }, [ fadeAnim ] );

	const backgroundColor = fadeAnim.interpolate({
		inputRange: [0, 1],
		outputRange: ['rgba(30, 67, 67, 1)', 'rgba(41, 93, 0, 1)']  // 0 : 150, 0.5 : 75, 1 : 0
	      } );

	  //debug( "Got bg value", backgroundColor );

	return(
			<Animated.View style={{ 'backgroundColor' : disabled ? "grey" : backgroundColor }}>
				<Button onPress={onPress} transparent><Text style={{ 'color' : "white" }}>Sync Now</Text></Button>
			</Animated.View>
		);
		*/
}

/**
 * The sync helper
 */
function SyncHelperView() {
	const [ showing, setShowing ] = useState( true );
	//
	// The fade animation value
	//const animation = useRef( new Animated.Value( 0 ) ).current  // Initial value for opacity: 0

	// Hider
	const hide = ()=>setShowing( false );

	return(
	      <Modal
		animationType="fade"
		transparent={true}
		visible={showing}
		onRequestClose={hide}
	      >
			<View style={styles.modalBackground}>
				<View style={styles.modalView}>
					<View style={{ 'alignItems' : "center", 'justifyContent' : "center", 'marginBottom' : 16 }}>
						<Text style={styles.popupText}>When done shopping,</Text>
						<Text style={styles.popupText}>open your cart, then</Text>
						<Text style={[ styles.popupText, { 'color' : JenerusBlue } ]}>Sync Now</Text>
						<Text style={styles.popupText}>to Jenerus.</Text>
					</View>
					<Button block onPress={hide}><Text>Got it!</Text></Button>
				</View>
			</View>
	      </Modal>
	);
}

export default function ShopScreen( { navigation, route } ) {
/*
    const runFirst = `
      document.body.style.backgroundColor = 'red';
			debug( "Runned." );
			window.ReactNativeWebView.postMessage( "We runned!" );
			document.addEventListener("message", debug );
			window.addEventListener("message", debug );
      true; // note: this is required, or you'll sometimes get silent failures
    `;
	*/

	// We'll need this
	const webViewRef = useRef();
	const [ noCartItemsModalVisible, setNoCartItemsModalVisible ] = useState( false );
	const [ isLoading, setIsLoading ] = useState( false );
	const [ canGoBack, setCanGoBack ] = useState( false );
	const [ canGoForward, setCanGoForward ] = useState( false );
	const [ isCartVisible, setIsCartVisible ] = useState( false );
	const [ currentURL, setCurrentURL ] = useState();
	const [ site ] = useState( route.params.site );
	const [ store ] = useState( getStore( site ) );
	const [ webViewSourceURI, setWebViewSourceURI ] = useState( store.url );
	const [ atCheckoutVisible, setAtCheckoutVisible ] = useState( false );
	const [ atWalmartCom, setAtWalmartCom ] = useState( false );
	const [ isSlideUpSyncVisible, setIsSlideUpSyncVisible ] = useState( false );

	// Shorthand
	const { 'current' : webView } = webViewRef;

	// On mount
	useEffect( ()=>{
		navigation.setOptions( {
			'headerLeft' : <Button transparent onPress={()=>navigation.goBack()}><Icon type="FontAwesome" name="times" /></Button>
		} );
	}, [] );
	
	// When isCartVisible changes
	useEffect( ()=>{
		// Also set this other thing
		setIsSlideUpSyncVisible( isCartVisible );
	}, [ isCartVisible ] );

	useEffect( ()=>{
		debug( "webViewSourceURI=", webViewSourceURI );
	}, [ webViewSourceURI ] );

	// When the URL changes
	useEffect( ()=>{
		// Cart is not visible if the URL changes
		setIsCartVisible( false );

		// Is it the checkout URL?
		if( store.checkoutRegExp.test( currentURL ) ) {
			debug( "Is checkout. Not good." );

			// Show the modal
			setAtCheckoutVisible( true );
		}
		// Specific
		else {
			// Specific URL issues with walmart grocery
			if( site=="grocery.walmart.com" ) {
				debug( "We are debugging walmart grocery." );
				// Must be walmart grocery
				if( /walmart.com\/(?!grocery|account)/i.test( currentURL ) ) {
					debug( "Is regular Walmart. Not good." );

					// Set the at walmart.com
					setAtWalmartCom( true );
				}
			}
		}
	}, [ currentURL ] );

	// On command
	const onCommand = o=>{

		switch( o.get( "command" ) ) {
			case "cartVisibilityChange":
				setIsCartVisible( o.get( "visible" ) );
				break;
		}

		/*
		if( o.get( 'command' )=="documentReady" ) {
			// Sending bum command for timeout purpose
			webViewRef.current.command( "tester" ).sendPromise().then( debug ).catch( debug );

		}
		*/
	};

	// We will sync now
	const syncNow = async ()=>{
		try {
			const result = await webViewRef.current.command( "syncCart" ).sendPromise();

			debug( "Got this cart", result );

			// Get the cart
			const cart = result.get( "cart" );

			if( !cart || !(cart.items instanceof Array) || cart.items.length===0 ) {
				Toast.show( {
					'text' : "Could not find any items in your shopping cart. Please make sure the cart is visible and that there are items in it.",
					'buttonText' : "Dismiss",
					'type' : "danger",
					'duration' : 0
				      } );
				return;
			}

			// Total up the items
			const totalCost = sumBy( cart.items, "total_price" );

			// Do we need more groceries?
			if( totalCost<MinimumCartAmount ) {
				Toast.show( {
					'text' : `Please fill your cart with a minimum of ${formatCurrency( MinimumCartAmount )} worth of groceries.`,
					'buttonText' : "Dismiss",
					'type' : "danger",
					'duration' : 5000
				      } );
				return;
			}

			navigation.navigate( "SelectDeliveryMethod", { cart } );
		}
		catch( e ) {
			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
		      } );
		}
	};

	// When we are touched (oooh lala)
	const onBubbleTouch = async ()=>{
		try {
			// Do we have a cart?
			const result = await webViewRef.current.command( "isCartVisible" ).sendPromise();

			debug( "Result is", result );

			const isVisible = result.get( "isVisible" );

			debug( "We got thing", isVisible );

			if( isVisible )
				syncNow();
			else
				setNoCartItemsModalVisible( true );


			/*
			const result = await webViewRef.current.command( "syncCart" ).sendPromise();

			debug( "Got this cart", result );
			*/
		}
		catch( e ) {
			debug( "Error syncing cart", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
		      } );
		}
	};

  return (
  	<Container>
			<View style={{ 'height' : 1, 'width' : "100%", 'borderWidth' : 1 }} />
				<SyncHelperView />
		
		<Container style={{ 'flex' : 1 }}>
    {/*<SafeAreaView style={{ 'flex' : 1 }}>*/}
    			<View style={{ 'flex' : 1 }}>
				<MessagingWebView
					ref={webViewRef}
					source={{ 'uri' : webViewSourceURI }}
					onCommand={onCommand}
					onError={e=>debug( "Errored", e )}
					onLoadStart={()=>setIsLoading( true )}
					onLoadEnd={()=>setIsLoading( false )}
					onNavigationStateChange={( { canGoBack, canGoForward, url } )=>{
						debug( "New current URL", url );

						// Their current URL
						setCurrentURL( url );
						// Set the back and forward stats
						setCanGoBack( canGoBack );
						setCanGoForward( canGoForward );
					}}


					injectedJavaScript={InjectedJavaScript}


				/>
			</View>
			<View style={{ 'height' : 1, 'width' : "100%", 'borderWidth' : 1 }} />
			<Footer>
				<FooterTab>
					{isCartVisible ? <GlowButton full={true} disabled={!isCartVisible} onPress={onBubbleTouch} /> :

					<>
						<Button transparent disabled={!canGoBack} onPress={()=>webView.native.goBack()}><Icon name="arrow-back" type="Ionicons" style={canGoBack ? styles.barColor : styles.barColorDisabled } /></Button>
						{isLoading ?
						<Button transparent><Spinner /></Button>
						:
						<Button transparent><Icon name="refresh" style={styles.barColor} onPress={()=>webView.native.reload()} type="Ionicons" /></Button>}
						<Button transparent disabled={!canGoForward}><Icon name="arrow-forward" onPress={()=>webView.native.goForward()} type="Ionicons" style={canGoForward ? styles.barColor : styles.barColorDisabled } /></Button>
						<GlowButton disabled={!isCartVisible} onPress={onBubbleTouch} />
					</>}
				</FooterTab>
			</Footer>

			{/*
			<Draggable
				x={75}
				y={100}
				renderSize={56}
				renderColor='black'
				renderText='A'
				isCircle
				onShortPressRelease={onBubbleTouch}
			/>
			*/}

			 <Dialog
					visible={noCartItemsModalVisible}
					footer={
						<DialogFooter>
							<DialogButton
								text="Okay"
								onPress={()=>setNoCartItemsModalVisible( false )}
							/>
						</DialogFooter>
					}
				>
					<DialogContent style={{ 'padding' : 16 }}>
						<Text style={{ 'fontSize' : 16}}>I can't see your shopping cart. Please open up your shopping cart and try your sync again.</Text>
					</DialogContent>
				</Dialog>

			{/* When they went too far to checkout by accident */}
			<CustomPopupModal onCancel={()=>setAtCheckoutVisible( false )} visible={atCheckoutVisible} title="Whoops!" innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>It looks like you've gone to checkout!</Text>
				<Text style={styles.dialogText}>We're not ready to checkout just yet. First, sync your cart to Jenerus so you can get funded.</Text>
				<Button block onPress={()=>{
					// Set the browse URL back to main
					setWebViewSourceURI( store.url+"?="+Date.now() );
					// Hude
					setAtCheckoutVisible( false );
				}}><Text>Take Me Back</Text></Button>
			</CustomPopupModal>
			{/* We accidentally went to walmart.com */}
			<CustomPopupModal onCancel={()=>setAtWalmartCom( false )} visible={atWalmartCom} title="Whoops!" innerStyle={styles.dialogInner}>
				<Text style={styles.dialogText}>It looks like you've opened Walmart.com.</Text>
				<Text style={styles.dialogText}>Right now, Jenerus only supports Walmart Grocery, a subset of Walmart.com.</Text>
				<Button block onPress={()=>{
					debug( "Changing the source URI", webViewSourceURI, store.url );

					// Go back to walmart
					setWebViewSourceURI( store.url+"?="+Date.now() );
					// Hude
					setAtWalmartCom( false );
				}}><Text>Take Me Back</Text></Button>
			</CustomPopupModal>
		</Container>
				<SlideUpSync onSyncNow={onBubbleTouch} onClose={()=>setIsSlideUpSyncVisible( false )} visible={isSlideUpSyncVisible} />
 	</Container> 
  );
}
