import React, { useState } from 'react';

import { Container, Button, Grid, Col, Text, View, Form, Toast } from 'native-base';
import { StyleSheet } from 'react-native';

import InputHelper from '../../../../InputHelper';
import CustomButton from '../../../../CustomButton';

import request from '../../../../request';

const debug = require( '../../../../debug' )( "views:shop:SyncDialog" );

import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';

import { Formik } from 'formik';
import * as Yup from 'yup';

const styles = StyleSheet.create( {
	'margin' : { 'marginTop' : 8, 'marginBottom' : 8 }
} );


// Form validation stuff
const cartFormSchema = Yup.object( {
	'name' : Yup.string().required( "Please name your cart to make it easier to find later on." )
} );

/**
 * View for choosing grocery type
 */
export default function NameCart( { navigation, route } ) {
	// Whether or not we shoudl show the dilaog
	const [ successDialogVisible, setSuccessDialogVisible ] = useState( false );
	const [ cart, setCart ] = useState();
	
	/**
	 * When we submit
	 */
	const onSubmit = async values=>{
		try {
			debug( "About to push params and values.", route.params, values );

			let { cart, deliveryMethod } = route.params;

			// Map everything
			values = {
				...cart,
				...values,
				'delivery_method' : deliveryMethod
			};

			debug( "About to post values", values );

			// Now sync
			( { cart } = await request.post( "cart/add/me", values ) );

			debug( "We got cart", cart );

			// Yayzer
			setCart( cart );
			setSuccessDialogVisible( true );
		}
		catch( e ) {
			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	};

	// Return the delivery method instructions
	return(
		<Container>
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignSelf' : "center" }}>

    <Formik
      validationSchema={cartFormSchema}
      onSubmit={onSubmit}
      initialValues={{ 'name' : "" }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting
      }) => (

    <Form>
    	<Text style={styles.margin}>Let's name this cart so we can identify it later.</Text>
			<InputHelper
				style={styles.margin}
				label="Cart Name"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="name"
				values={values}
				itemProps={{ 'floatingLabel' : true, 'last' : true }}
			/>
	<Text style={{ 'color' : "#c0c0c0", 'marginLeft' : 8 }}>Ex: Thanksgiving List</Text>

        
<CustomButton
				style={styles.margin}
	isLoading={isSubmitting}
	block 
	onPress={handleSubmit}>
        <Text>Sync Now</Text>
      </CustomButton>
		</Form>
			
			)}
			</Formik>

					</View>
				</Col>
			</Grid>

			 <Dialog
					visible={successDialogVisible}
					footer={
						<DialogFooter>
							<DialogButton
								text="Okay"
								onPress={()=>{
									// Hide this
									setSuccessDialogVisible( false );
									// Buggo
									navigation.navigate( "ShopSyncDetails", {
										'cartID' : cart&&cart.id,
										'goNext' : true
									} );
								}}
							/>
						</DialogFooter>
					}
				>
					<DialogContent style={{ 'padding' : 16 }}>
						<Text style={{ 'fontSize' : 16}}>Yay! You have successfully synced your shopping cart with Jenerus.com!</Text>
					</DialogContent>
				</Dialog>
		</Container>
	);
}

