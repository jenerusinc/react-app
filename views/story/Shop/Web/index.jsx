import React, { useState } from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../../../../DefaultHeader';

import SelectDeliveryMethod from './SelectDeliveryMethod';
import NameCart from './NameCart';
import Site from './Site';

import { Button, Text } from 'native-base';

const Stack = createStackNavigator();


/**
 * Our sync dialoger
 */
export default function() {
	return(
		<Stack.Navigator {...defaultNavigationOptions}>
			<Stack.Screen name="Shop"
				options={{
					'title' : "Shopping",
				}}
				component={Site}
				/>
			<Stack.Screen name="SelectDeliveryMethod"
				options={{ 'title' : "Delivery Method" }}
				component={SelectDeliveryMethod}
				/>
			<Stack.Screen name="NameCart"
				component={NameCart}
				options={{ 'title' : "Your Cart" }}
				/>
		</Stack.Navigator>
	);
}
