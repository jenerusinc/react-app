import React from 'react';

import { Container, Button, Grid, Col, Text, View } from 'native-base';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create( {
	'margin' : { 'marginTop' : 8, 'marginBottom' : 8 }
} );

const debug = require( '../../../../debug' )( "views:shop:SyncDialog" );

/**
 * View for choosing grocery type
 */
export default function SelectDeliveryMethod( { navigation, route } ) {
	debug( "We have params", route.params );
	

	// Which delivery mesod
	const onSuccess = deliveryMethod=>{
		// Remember to forward all other params too
		navigation.navigate( "NameCart", { ...route.params, deliveryMethod } );
	};

	// Return the delivery method instructions
	return(
		<Container>
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignSelf' : "center" }}>
						<Text style={styles.margin}>Please tell us how your groceries will be getting to you.</Text>
						<Button style={styles.margin} block light onPress={()=>onSuccess( "delivery" )}><Text>Delivery</Text></Button>
						<Button style={styles.margin} block light onPress={()=>onSuccess( "pickup" )}><Text>Pick-Up</Text></Button>
					</View>
				</Col>
			</Grid>
		</Container>
	);
}

