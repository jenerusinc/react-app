import React from 'react';

import { View, Text } from 'native-base';

import { JenerusAquamarine, JenerusBlue } from '../../constants';

import * as Progress from 'react-native-progress';

const totalSteps = 2;

export default function( { step } ) {
	return(
		<View style={{ 'padding' : 8, 'backgroundColor' : "white", 'borderBottomColor' : JenerusBlue, 'borderBottomWidth' : 1 }}>
			<View style={{ 'flexDirection' : "row", 'justifyContent' : "space-between" }}>
				<Text>Confirming Account</Text>
				<Text>Step {Math.min( step+1, totalSteps )} of {totalSteps}</Text>
			</View>
			<Progress.Bar progress={step/totalSteps} width={null} />
		</View>
	);
}
