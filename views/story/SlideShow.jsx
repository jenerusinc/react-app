import React, { useRef } from 'react';

import AddToCart from '../../assets/pngs/1slide.png';
import OpenCart from '../../assets/pngs/2slide.png';
import TellYourStory from '../../assets/pngs/3slide.png';

import { SafeAreaView, Image } from 'react-native';

import ViewPager from '@react-native-community/viewpager';

import { StyleSheet } from 'react-native';

import * as atoms from '../../atoms';
import { useSetRecoilState } from 'recoil';

import { Container, View, Text, Col, Grid, Row, H1, Button, Icon } from 'native-base';

import { JenerusAquamarine, JenerusBlue } from '../../constants';

const debug = require( '../../debug' )( "views:story:SlideShow" );

const styles = StyleSheet.create( {
	'background' : {
		'backgroundColor' : "white",
		'flex' : 1
	},
	'text' : {
		'color' : "black"
	}
} );

/**
 * The buttons
 */
function Buttons( { onPrevious, onNext, onFinish } ) {
	return(

		<View style={{ 'flexDirection' : "row", 'alignContent' : "space-between", 'alignSelf' : "stretch" }}>
			{onPrevious ? <Button onPress={onPrevious} transparent style={{ 'flex' : 1, 'flexDirection' : "row", 'justifyContent' : "flex-start" }}><Icon type="FontAwesome" name="chevron-left" style={styles.text} /><Text style={styles.text}>Back</Text></Button> : null}
			{onNext ? <Button onPress={onNext} transparent style={{ 'flex' : 1, 'flexDirection' : "row", 'justifyContent' : "flex-end" }}><Text style={styles.text}>Next</Text><Icon type="FontAwesome" name="chevron-right" style={styles.text} /></Button> : null}
			{onFinish ? <Button onPress={onFinish} transparent style={{ 'flex' : 1, 'flexDirection' : "row", 'justifyContent' : "flex-end" }}><Text style={styles.text}>Let's Go!</Text><Icon style={styles.text} type="FontAwesome" name="check" /></Button> : null}
		</View>
	);
}

/**
 * Actual component creator
 */
function Page( { source, title, text, onPrevious, onNext, onFinish } ) {
	return(

		<View style={{ 'flex' : 1 }}>
			<View style={{ 'flex' : 1 }} />
			<View stlye={{ 'flex' : 8 }} />
					<View>
						<Text style={[styles.text, { 'alignSelf' : "center", 'fontSize' : 40, 'fontWeight' : "bold" } ]}>{title}</Text>
					</View>

					<View style={{ 'alignItems' : "center", 'marginBottom' : 16 }}>
						<Text style={[styles.text, { 'fontSize' : 20 }]}>{text}</Text>
					</View>

					<View style={{ 'flex' : 1, 'alignItems' : "center" }}>
						<View style={{ 'flex' : 1, 'flexDirection' : "row", 'alignItems' : "center" }}>
							<Image source={source} style={{height:"100%", width:"100%" }} resizeMode="contain" />
						</View>
					</View>
			<View style={{ 'flex' : 1 }} />

				<Buttons onPrevious={onPrevious} onNext={onNext} onFinish={onFinish} />
				</View>
	);
}

/**
 * Got it page
 */
function GotIt() {
		return(
		<Grid style={{ 'alignItems' : "center" }}>
				<Col style={{ 'alignItems' : "center" }}>
					<Icon type="FontAwesome" name="check" style={styles.text} />
					<Text style={styles.text}>Let's go!</Text>
				</Col>
		</Grid>
		);
}

/**
 * Title
 */
function Title( props) {
	return(
		
		<View style={{ 'flex' : 1 }}>
			<Grid style={{ 'alignItems' : "center" }}>
					<Col style={{ 'marginRight' : 16, 'alignItems' : "flex-end" }}>
						<Text style={[styles.text, { 'fontSize' : 50, 'fontWeight' : "bold",'fontFamily' : "Pier-Bold" }]}>Shop with</Text>
						<Text style={[styles.text, { 'fontSize' : 55, 'fontWeight' : "bold", 'color' : JenerusBlue, 'fontFamily' : "Pier-Bold" }]}>Jenerus</Text>
						<Text style={[styles.text, { 'fontSize' : 50, 'fontWeight' : "bold",'fontFamily' : "Pier-Bold"  }]}>in 3 easy</Text>
						<Text style={[styles.text, { 'fontSize' : 50, 'fontWeight' : "bold",'fontFamily' : "Pier-Bold"  }]}>steps.</Text>
					</Col>
			</Grid>
			<Buttons {...props} />
		</View>
	)
}

/**
 * Do it
 */
export default function( { navigation, route } ) {
	// So we can set this
	const setSeenSlideShow = useSetRecoilState( atoms.SeenSlideShow );

	const pagerRef = useRef( null );

	// Safe set page because of crashies
	const setPage = page=>{
		if( pagerRef.current )
			pagerRef.current.setPage( page );
		else
			debug( "Warning: pagerRef is undefined." );
	};

	const onDone = ()=>{
		debug( "We are done." );

		// We did
		setSeenSlideShow( true );

		// Dismiss us
		navigation.navigate( route.params.screen );
	};

	const onPageSelected = e=>{
		const { position } = e.nativeEvent;

		debug( "New position is", position );

		if( position==4 )
			onDone();
	};

	return(
		<SafeAreaView style={styles.background}>
			<Container>
			    <ViewPager style={{ 'backgroundColor' : "green" }} onPageSelected={onPageSelected} ref={ref=>pagerRef.current=ref} style={{ 'flex' : 1 }} initialPage={0}>
			      <View key="1">
				<Title onNext={()=>setPage( 1 )} />
			      </View>
			      <View key="2">
				<Page source={AddToCart} title="Step 1" text="Fill your online cart with groceries." onNext={()=>setPage( 2 )} onPrevious={()=>setPage( 0 )} />
			      </View>
			      <View key="3">
				<Page source={OpenCart} title="Step 2" text="Open your cart and send to Jenerus." onNext={()=>setPage( 3 )} onPrevious={()=>setPage( 1 )} />
			      </View>
			      <View key="4">
				<Page source={TellYourStory} title="Step 3" text="Tell your story. Get funded. That's it." onFinish={onDone} onPrevious={()=>setPage( 2 )} />
			      </View>
			      <View key="5">
				<GotIt />
			      </View>
			    </ViewPager>
			</Container>
		</SafeAreaView>
	)
}
