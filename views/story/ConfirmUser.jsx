import React, { useEffect, useState, useRef } from 'react';

import { Animated, Easing } from 'react-native';

import { View, H2, Form, Container, Header, Content, Card, CardItem, Text, Body, Button, Icon, Toast, Badge, Footer } from "native-base";

import CustomButton from '../../CustomButton';
import InputHelper from '../../InputHelper';

import { useRecoilState } from 'recoil';
import * as atoms from '../../atoms';
import wrapForRecoil from '../../wrapForRecoil';

import { cleanPhoneNumber } from '../../util';

import { Formik } from 'formik';
import * as Yup from 'yup';

import request from '../../request';

import StepProgress from './StepProgress';

const debug = require( '../../debug' )( "story:ConfirmUser" );

// Form validation stuff
const enterMobileNumberFormSchema = Yup.object( {
	'mobile' : Yup.string().required( "Please enter your phone number." ).trim().transform( v=>cleanPhoneNumber( v ) ).length( 10, "Please enter a valid phone number." )
} );

const enterMobileNumberCodeFormSchema = Yup.object( {
	'code' : Yup.number().required( "Please enter the code you received" )
} );

/**
 * The good thing
 */
function VerifiedMark() {
	// The fade animation value
	  const fadeAnim = useRef( new Animated.Value( 0 ) ).current  // Initial value for opacity: 0

		// Use an effect
	  useEffect( ()=>{
			// Do the animation
	    Animated.timing(
	      fadeAnim,
	      {
		toValue: 1,
		duration: 1000,
					easing : Easing.bezier( 0.34, 5, 0.64, 1 ),
					'useNativeDriver' : true
	      }
	    ).start();
	  }, [ fadeAnim ] );


	return(
			<Animated.View 
			style={{ 'transform' : [ { 'scale' : fadeAnim } ] }}
			>
				{/*
				<Badge success>
					<Text>Complete</Text>
				</Badge>
				*/}
				<Icon name="check" type="FontAwesome" style={{ 'color' : "green" }} />
			</Animated.View>
	);
}

/**
 * The bad mark
 */
function UnverifiedMark() {
	return( null );
	/*
	return(
		<Badge warning>
			<Text>Unverified</Text>
		</Badge>
	);
	*/
}

/**
 * Verify an e-mail wagonshire
 */
function VerifyEmail( { user, onSuccess, ...rest } ) {
	debug( "User infor", user );

	const [ loading, setLoading ] = useState( false );

	const {
		emailVerified,
		id,
		email
	} = user;

	/**
	 * Quick function to send e-mail verification
	 */
	const sendVerificationEmail = async ()=>{
		setLoading( true );

		try {
			// Send it
			await request.get( "user/resendVerificationEmail" );

			// Let's toast
			Toast.show( {
				'text' : `We have sent you a verification e-mail to ${email}. Please check your spam box if you do not see it.`,
				'buttonText' : "Dismiss",
				'type' : "success",
				'duration' : 0
			} );
		}
		catch( e ) {
			debug( "Error sending verification e-mail", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}

		setLoading( false );
	};

	return( 
	    <Card {...rest}>
	      <CardItem header bordered>
	      	<Text>Verify Your E-mail Address {emailVerified ?
			<VerifiedMark /> :
			<UnverifiedMark />}</Text>
	      </CardItem>
	      <CardItem bordered>
	      	<Body>
	      {emailVerified ?
		      <Text>Thank you for verifying your e-mail address {email}.</Text> :
			      <>
			      <View style={{ 'flexDirection' : "row", 'flexWrap' : "wrap", 'marginBottom' : 16 }}>
				      <Text>We sent you an e-mail at </Text>
				      <Text style={{ 'textDecorationLine' : "underline", 'fontWeight' : "bold" }}>{email}</Text>
			      </View>
				      <Text>Didn't get it? Click below.</Text>
				      <CustomButton style={{ 'marginTop' : 16 }} block onPress={sendVerificationEmail} isLoading={loading}><Text>Send me another verification e-mail</Text></CustomButton>
			      </>
	}
			</Body>
		</CardItem>
	    </Card>
	);
}

/**
 * Verify an mobile
 */
function VerifyMobile( { user, onSuccess, ...rest } ) {
	debug( "User infor", user );

	const {
		mobileVerified,
		id,
		mobile
	} = user;

	return( 
	    <Card {...rest}>
	      <CardItem header bordered>
	      	<Text>Verify your mobile number {mobileVerified ?
			<VerifiedMark /> :
			<UnverifiedMark />}</Text>
	      </CardItem>
	      <CardItem bordered>
	      	<Body>
	      {mobileVerified ?
		      <Text>Thank you for verifying your phone number.</Text> : <VerifyMobileNumberForm user={user} onSuccess={onSuccess} />}
			</Body>
		</CardItem>
	    </Card>
	);
}

/**
 * Verify the mobile number jammie
 */
function VerifyMobileNumberForm( { user, onSuccess } ) {
	// Our mobile state
	const [ mobile, setMobile ] = useState( user.mobile );
	const [ step, setStep ] = useState( 0 );

	if( step==0 )
		return( <GetMobileNumberForm mobile={mobile} onSuccess={mobile=>{
			// Set the mobile number
			setMobile( mobile );
			// Set the step to next
			setStep( 1 );
		}} /> );
	else
		return( <ConfirmMobileNumberForm
			onSuccess={onSuccess}
			mobile={mobile}
			onBack={()=>setStep( 0 )}
			/> );
}


/**
 * The verification bajorn
 */
function ConfirmMobileNumberForm( { mobile, onSuccess, onBack } ) {
	const [ isResending, setIsResending ] = useState( false );

	const onSubmit = async values=>{
		try {
			// Get the code
			const { code } = values;

			// Request to send the code
			const { user } = await request.post( "user/confirmMobileCode", { code } );
			
			// We are success
			if( onSuccess instanceof Function )
				onSuccess( user );
		}
		catch( e ) {
			debug( "Error confirming code", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	}

	  // Our toaster
	const onResend = async ()=>{
		setIsResending( true );

		try {
			// Request to send the code
			await request.post( "user/sendMobileCode", { mobile } );

			Toast.show( {
				'text' : "We have resent your code to your mobile number.",
				'buttonText' : "Dismiss",
				'type' : "success",
				'duration' : 5000
			} );
		}
		catch( e ) {
			debug( "Error resending code", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}

		setIsResending( false );
	};


	return(
	    <Formik
	      validationSchema={enterMobileNumberCodeFormSchema}
	      onSubmit={onSubmit}
	      initialValues={{ 'code' : "" }}
	    >
	      {({
		handleSubmit,
		handleChange,
		handleBlur,
		values,
		touched,
		isValid,
		errors,
		isSubmitting
	      }) => (

	      <Form style={{ alignSelf: 'stretch' }}>

			<InputHelper
				label="Enter the code"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="code"
				values={values}
				inputProps={{ 'keyboardType' : "numeric" }}
				itemProps={{ 'floatingLabel' : true, 'last' : true }}
			/>
				<View style={{ 'flexDirection' : "row", 'justifyContent' : "space-between" }}>
					<Button transparent onPress={onBack}><Text>Go Back</Text></Button>
					<CustomButton isLoading={isResending} transparent onPress={onResend}><Text>Resend</Text></CustomButton>
				</View>

				<CustomButton
					style={{ 'marginTop' : 16 }}
					isLoading={isSubmitting}
					onPress={handleSubmit}
					block
				>
					<Text>Confirm</Text>
				</CustomButton>
	      </Form>
	     )}
	    </Formik>
	);
}

/**
 * The form to enter the mobile number
 */
function GetMobileNumberForm( { mobile, onSuccess } ) {
	  // Our toaster
	const onSubmit = async values=>{
		try {
			// Get the mobile
			const { mobile } = values;

			// Request to send the code
			await request.post( "user/sendMobileCode", { mobile } );
			
			// We are success
			if( onSuccess instanceof Function )
				onSuccess( mobile );
		}
		catch( e ) {
			debug( "Error sending code", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	};

	return(
	    <Formik
	      validationSchema={enterMobileNumberFormSchema}
	      onSubmit={onSubmit}
	      initialValues={{ 'mobile' : mobile||"" }}
	    >
	      {({
		handleSubmit,
		handleChange,
		handleBlur,
		values,
		touched,
		isValid,
		errors,
		isSubmitting
	      }) => (

	      <Form style={{ alignSelf: 'stretch' }}>
	      	{/* , "style" : { 'borderWidth'  : 1, 'borderColor' : "black" } */}
	      	<View style={debug( "Errors are", errors )} />

			<InputHelper
				label="Please enter your mobile number"
				errors={errors}
				touched={touched}
				handleBlur={handleBlur}
				handleChange={handleChange}
				name="mobile"
				values={values}
				inputProps={{ 'autoCompleteType' : "tel", 'keyboardType' : "phone-pad" }}
				itemProps={{ 'floatingLabel' : true, 'last' : true }}
			/>

				<CustomButton
					style={{ 'marginTop' : 16 }}
					isLoading={isSubmitting}
					onPress={handleSubmit}
					block
				>
					<Text>Send Code</Text>
				</CustomButton>
	      </Form>
	     )}
	    </Formik>
	);
}

/**
 * Confirm our user
 */
export default wrapForRecoil( function( { navigation } ) {
	// Get the user
	const [ user, setUser ] = useRecoilState( atoms.User );

	debug( "Got user", user );

	/**
	 * When we are done
	 */
	const weAreDone = ()=>{
		debug( "We are done." );

		navigation.replace( "ApplicationForm" );
	};

	/**
	 * Check to see if we have all we need with the new user info
	 */
	const checkDone = user=>{
		// Set in recoil
		setUser( user );
		// Set the garbagy
		if( user.emailVerified && user.mobileVerified )
			weAreDone();
	};

	// Keep checking to see if there are any changes not reported
	// This is called "polling"
	const checkForChanges = async function() {
		// Keep looking
		if( user.mobileVerified&&user.emailVerified )
			return; // Don't call

		try {
			debug( "Looping again to fetch user." );

			// Fetch the new me
			const { 'user' : myUser } = await request.get( "user/me" );

			// Do we have any changes
			if( myUser.mobileVerified!=user.mobileVerified ||
				myUser.emailVerified!=user.emailVerified ) {
				// Set the new user info
				checkDone( myUser );
			}
				
		}
		catch( e ) {
			debug( "Error fetching user me", e );
		}

		debug( "Dropped out of checking." );
	};

	// Do on start
	useEffect( ()=>{
		// Are we already?
		if( user.emailVerified&&user.mobileVerified ) {
			// Don't even bother
			weAreDone();
		}
		else {
			// Check for changes
			const timer = setInterval( checkForChanges, 10000 );

			// Kill the timer
			return( ()=>clearInterval( timer ) );
		}
	}, [] );
	
	return(
		<Container>
			<StepProgress step={0} />
			<Content>
				{user ?
				<>
					<H2 style={{ 'margin' : 16 }}>Verify It's You</H2>
					<VerifyEmail user={user} onSuccess={checkDone} />
					<VerifyMobile user={user} onSuccess={checkDone} />
				</>
				: <Text>Please log in</Text>}
			</Content>

		</Container>
	);
} )
