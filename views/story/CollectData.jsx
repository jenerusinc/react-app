import React, { useState, useEffect } from 'react';

import LoadingScreen from '../../LoadingScreen';
import ErrorRetryScreen from '../../ErrorRetryScreen';

import * as atoms from '../../atoms';
import { useSetRecoilState } from 'recoil';

import request, { fetchAsk, fetchUser } from '../../request';

const debug = require( '../../debug' )( "views:story:CollectData" );

/**
 * Collect user and other datas
 */
export default function( { navigation } ) {
	// Our setters for after
	const setUser = useSetRecoilState( atoms.User );
	const setAsk = useSetRecoilState( atoms.Ask );
	const setApplication = useSetRecoilState( atoms.UserApplication );

	// Do we have an error?
	const [ error, setError ] = useState();


	// Our actual load function
	const loadAll = async ()=>{
		try {
			// No more error
			setError( null );

			// Try them all
			const [
				{ user },
				{ ask },
				{ application }
			] = await Promise.all( [
				fetchUser(),
				fetchAsk(),
				request.get( "user/me/application" )
			] );

			debug( "Got user,ask", user, ask );

			// Set them
			setUser( user );
			setAsk( ask );
			setApplication( application );

			// Now move on
			debug( "We finished." );

			// Now figure out where we are going
			const isValid = user.mobileVerified && user.emailVerified;

			// What we're resetting to
			let routes;

			// Now go where we go
			// Do we have an ask
			if( ask ) {
				// Bump all the way to edit the ask
				routes = [ { 'name' : "AskForm", 'screen' : "TellYourStory1" } ];
			}
			else if( !isValid ) {
				// Get valid
				routes = [ { 'name' : "ConfirmUser" } ];
			}
			else if( !application ) {
				// We need an application
				routes = [ { 'name' : "ApplicationForm" } ];
			}
			else {
				// Let's shop
				routes = [ { 'name' : "ShopSyncCart" } ];
			}

			// Now call
			navigation.reset( { 'index' : 0 , routes } );
		}
		catch( e ) {
			// We errored!
			setError( e );
		}
	};

	/**
	 * Load them all when we get in
	 */
	useEffect( ()=>{
		// We have to do this here because otherwise react complains
		// that we're returning a promise in a function that
		// is supposed to return a thunk. Whatevs.
		loadAll();
	}, [] );

	if( error )
		return( <ErrorRetryScreen onRetry={loadAll} error={error} /> );

	// Return these
	return( <LoadingScreen text="Collecting some infos..." /> );
}
