import React, { useState, useEffect } from 'react';

import { useRecoilState } from 'recoil';

import request from '../../request';
import * as atoms from '../../atoms';

const debug = require( '../../debug' )( "story:ApplicationForm" );

import { Button, Container, Content, Form, Text, Textarea, H2, Icon, Item } from 'native-base';

import CustomButton from '../../CustomButton';
import InputHelper from '../../InputHelper';

import wrapForRecoil from '../../wrapForRecoil';

import StepProgress from './StepProgress';

import TextareaHelper from '../../TextareaHelper';

import { Formik } from 'formik';
import * as Yup from 'yup';

import WeirdHighFiveSvg from '../../svgs/weird-high-five.svg';
import ImagePopupModal from '../../ImagePopupModal';

// Form validation stuff
const applicationFormSchema = Yup.object( {
	'income' : Yup.number( "Please enter your income as a number." ).required( "Please enter your income." ).min( 0 ),
	'peopleInHousehold' : Yup.number( "Please enter the NUMBER of people in your household." ).required( "Please make sure this is not empty." ).positive( "Please enter at least 1." ),
	'foodBudgetPerMonth' : Yup.number( "Please enter your monthly food budget as a number." ).required( "Please enter this value." ).positive( "Please enter a number greater than 0." ),
	'foodBudgetPerWeek' : Yup.number( "Please enter your weekly food budget as a number." ).required( "Please enter this value." ).positive( "Please enter a number greater than 0." ),
	'reason' : Yup.string().required( "Please tell us why you are applying for Jenerus." ).trim()
} );

/**
 * The application form
 */
export default wrapForRecoil( function ApplicationForm( { navigation } ) {
	// We have the application?
	const [ userApplication, setUserApplication ] = useRecoilState( atoms.UserApplication );
	const [ isFinished, setShowModal ] = useState( false );

	debug( "We have an application", userApplication );

	/**
	 * When we are successful
	 */
	const weAreDone = ()=>{
		debug( "We are done." );

		navigation.replace( "ShopSyncCart" );
	};

	/**
	 * Do we already have an application
	 */
	useEffect( ()=>{
		// Do we already have? Skip.
		if( userApplication )
			weAreDone();
	}, [] );

	/**
	 * When we submitted
	 */
	const onSubmit = async values=>{
		try {
			// Post the application
			const { application } = await request.post( "/api/user/me/application", values );

			// Set the application
			setUserApplication( application );

			// Now success
			//onSuccess instanceof Function && onSuccess( application );
			//weAreDone();
			setShowModal( true );
		}
		catch( e ) {
			debug( "Got error", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	};

	return(
	<Container>

		<ImagePopupModal
			visible={isFinished}
			control={WeirdHighFiveSvg}
			text="Your account is now confirmed!"
			buttonText="Let's Shop!"
			title="Amazing!"
			onClick={weAreDone}
		/>

			<StepProgress step={isFinished ? 2 : 1} />

	<Container style={{ 'padding' : 8 }}>
		<Content>
    <Formik
      validationSchema={applicationFormSchema}
      onSubmit={onSubmit}
      initialValues={{ 'income' : "", 'peopleInHousehold' : "", 'foodBudgetPerMonth' : "", 'foodBudgetPerWeek' : "", 'reason' : "" }}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting
      }) => (

      <Form>
                  <H2>Tell us a little bit about your situation</H2>
									<Text style={{ 'fontSize' : 14, 'paddingTop' : 8, 'paddingBottom' : 8 }}>We collect this information only for our records. We do not share it with any third party. Your privacy is very important to us.</Text>

					<InputHelper
						label="What is your yearly income?"
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						name="income"
						values={values}
						inputProps={{ 'keyboardType' : "decimal-pad" }}
						itemProps={{ 'stackedLabel' : true }}
					/>

					<InputHelper
						label="How many people in your household?"
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						name="peopleInHousehold"
						values={values}
						inputProps={{ 'keyboardType' : "numeric" }}
						itemProps={{ 'stackedLabel' : true }}
					/>

					<InputHelper
						label="What is your household food budget per MONTH?"
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						name="foodBudgetPerMonth"
						values={values}
						inputProps={{ 'keyboardType' : "decimal-pad" }}
						itemProps={{ 'stackedLabel' : true }}
					/>

					<InputHelper
						label="What is your household food budget per WEEK?"
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						name="foodBudgetPerWeek"
						values={values}
						inputProps={{ 'keyboardType' : "decimal-pad" }}
						itemProps={{ 'stackedLabel' : true }}
					/>

					<TextareaHelper
						textareaProps={{ 'rowSpan' : 5, 'bordered' : true, 'placeholder' : "Please tell us a little bit about why you're applying on Jenerus." }}
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						name="reason"
						values={values}
						style={{ 'paddingTop' : 8 }}
					/>


				<CustomButton
					style={{ 'marginTop' : 16 }}
					isLoading={isSubmitting}
					onPress={handleSubmit}
					block
				>
					<Text>Save and next</Text>
				</CustomButton>

				<Button
					onPress={weAreDone}
					style={{ 'marginTop' : 16 }}
					block
					success
				>
					<Text>
						Skip for now
					</Text>
				</Button>
		</Form>

      )}
    </Formik>
		</Content>
	</Container>

	</Container>
	);
} )
