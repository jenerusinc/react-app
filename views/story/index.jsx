import React, { useEffect, useRef } from 'react';

import { Container, Header, Content, Card, CardItem, Text, Body, Button, Icon } from "native-base";

import { useRecoilValue } from 'recoil';
import * as atoms from '../../atoms';

import ConfirmUser from './ConfirmUser';
import ApplicationForm from './ApplicationForm';
import Shop from './Shop';
import AskForm from './AskForm';
import EnsureLoggedIn from '../EnsureLoggedIn';
import CollectData from './CollectData';

import { CommonActions } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../../DefaultHeader';
import wrapForRecoil from '../../wrapForRecoil';

const debug = require( '../../debug' )( "views:story:index" );

//import SlideShow from './SlideShow';

const Stack = createStackNavigator();

/**
 * Container
 */
export default wrapForRecoil( function( { navigation } ) {
	//return( <SlideShow /> );
	// Get the user key
	const userKey = useRecoilValue( atoms.UserKey );
	// Keep track of whether or not we've been edited
	const initialMount = useRef( true );

	// Did the user somehow get logged out?
	useEffect( ()=>{
		// Is this the first time we're called?
		if( initialMount.current )
			initialMount.current = false;
		else if( !userKey ) { // User logged out 
			debug( "User logged out. Resetting here." );

			const resetAction = {
				...CommonActions.reset({
					index: 0,
					routes: [{ name: "EnsureLoggedIn" }]
				})
			};

			// Reset this baby
			navigation.dispatch( resetAction ); // Navigate back here
		}
	}, [ userKey ] );

	return(
		<Stack.Navigator initialRouteName={userKey ? "CollectData" : "EnsureLoggedIn"} {...defaultNavigationOptions}>
			<Stack.Screen name="EnsureLoggedIn"
				options={{ 'headerShown' : false }}
				component={EnsureLoggedIn}
				initialParams={{ 'screen' : "CollectData" }}
				/>
			<Stack.Screen name="CollectData"
				options={{ 'title' : "Collecting Infos" }}
				component={CollectData}
				/>
			<Stack.Screen name="ConfirmUser"
				options={{ 'title' : "Confirm Account" }}
				component={ConfirmUser}
				/>
			<Stack.Screen name="ApplicationForm"
				options={{ 'title' : "About You" }}
				component={ApplicationForm}
				/>
			<Stack.Screen name="ShopSyncCart"
				options={{ 'headerShown' : false }}
				component={Shop}
				/>
			<Stack.Screen name="AskForm"
				options={{ 'headerShown' : false }}
				component={AskForm}
				/>
		</Stack.Navigator>
	);
} )
