import React, { useEffect, useState, useRef } from 'react';

const debug = require( '../../debug' )( "views:story:AskForm" );

import request, { createCookieRequest, fetchAskCategories } from '../../request';

import Dialog, { DialogFooter, DialogButton, DialogContent, DialogTitle } from 'react-native-popup-dialog';

import _ from 'lodash';

import { StyleSheet } from 'react-native';

import { View, Spinner, Form, Content, Container, H2, Text, Toast, Switch, Picker, Button, Item, Label } from 'native-base';

import DatePicker from 'react-native-datepicker';

import CustomButton from '../../CustomButton'
import InputHelper from '../../InputHelper';
import TextareaHelper from '../../TextareaHelper';
import PickerHelper from '../../PickerHelper';

import LoadingScreen from '../../LoadingScreen';

import { MaximumNeededByDays, MinimumNeededByDays } from '../../constants';

import { getStoreName } from '../../accepted-stores';

import * as atoms from '../../atoms';
import { useRecoilState, useRecoilValue } from 'recoil';

import moment from 'moment';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import addDays from 'date-fns/addDays';
import formatISO from 'date-fns/formatISO';
import addMinutes from 'date-fns/addMinutes';
import startOfDay from 'date-fns/startOfDay'
import endOfDay from 'date-fns/endOfDay'
import formatReadable from 'date-fns/format'

import { Formik } from 'formik';
import * as Yup from 'yup';

import { createStackNavigator } from '@react-navigation/stack';
import { defaultNavigationOptions } from '../../DefaultHeader';

const Stack = createStackNavigator();

// Styles
const styles = StyleSheet.create( {
  'label' : {
  	'fontWeight' : "bold"
  },
  'marginBottom' : {
  	'marginBottom' : 16
  },
  'successBottomBorder' : {
	  'borderBottomWidth' : 1, 'borderBottomColor' : "green"
  },
  'failureBottomBorder' : {
	  'borderBottomWidth' : 1, 'borderBottomColor' : "red"
  },
  'successColor' : {
	'color' : "green"
  },
  'failureColor' : {
	'color' : "red"
  }
} );

styles.datePickerSuccess = StyleSheet.compose( styles.successBottomBorder, styles.successColor );
styles.datePickerFailure = StyleSheet.compose( styles.failureBottomBorder, styles.failureColor );

const momentDateFormat = "ll";
const dateFnsFormat = "PP";
	
// The base schema
const storySchema = Yup.object( {
	'categoryKeys' : Yup.string().required( "Please select at least one" ),
	'story' : Yup.string().required( "Please enter your story." ).trim().min( 50, "Please enter at least ${min} characters." ).max( 240, "Your story can only be a max of ${max} characters." )
} );

/**
 * React component to get the form schemers for an ask
 */
function useAskFormSchema( modifying, now ) {
	// The base schema
	const AskFormObject = {
		'anonymous' : Yup.boolean().required(),
	};

	const format = d=>formatReadable( d, dateFnsFormat );

	// Helper functions
	const getMinDate = ()=>startOfDay( addDays( now, MinimumNeededByDays ) );
	const getMaxDate = ()=>endOfDay( addDays( now, MaximumNeededByDays ) );

	// Get the schema
	const getSchema = ( modifying, minDate, maxDate )=>{
		debug( "getSchema(", modifying, minDate, maxDate, ")" );

		// Start with the base
		let o = AskFormObject;

		if( !modifying ) {
			o = {
				...o,
				'cartID' : Yup.string().required( "Please select a cart." ).notOneOf( [ "select" ], "Please select a cart." ),
				'neededBy' : Yup.date().required( "Please enter your time to be fulfilled." )
				.min( minDate, `Must be on or after ${format( minDate )}.` )
				.max( maxDate, `Must be on or before ${format( maxDate )}.` ),
				'onBehalf' : Yup.boolean().default( false )
			};
		}

		return( Yup.object( o ) );
	};

	const [ minDate, setMinDate ] = useState( getMinDate() );
	const [ maxDate, setMaxDate ] = useState( getMaxDate() );

	const [ schema, setSchema ] = useState( getSchema( modifying, minDate, maxDate ) );

	/**
	 * When now is modified
	 */
	useEffect( ()=>{
		debug( "Setting the mins and max dates" );

		setMinDate( getMinDate() );
		setMaxDate( getMaxDate() );
	}, [ now ] );

	/**
	 * When anything else is modified
	 */
	useEffect( ()=>{
		debug( "Setting the schema." );

		setSchema( getSchema( modifying, minDate, maxDate ) );
	}, [ minDate, maxDate, modifying ] );

	return( [ schema, minDate, maxDate ] );
}

/**
 * Our categories control
 */
function CategoriesControl( { values, name, style, errors, setFieldValue, categories, touched } ) {
	// Shorthandies
	const error = errors[ name ];
	const value = values[ name ]||[];
	touched = touched[ name ];
	const isInvalid = error&&touched;

	/**
	 * Toggle this key
	 */
	const toggle = key=>{
		// Our new one
		let newValue;

		// Does it exist?
		if( value.indexOf( key )==-1 )
			newValue = [ ...value, key ];
		else
			newValue = value.filter( k=>k!=key );

		debug( "Categories changed to", newValue );

		// Now do it
		setFieldValue( name, newValue );
	};

	return(
		<View style={[ isInvalid ? { 'borderColor' : "red", 'borderWidth' : 1 } : null, style ]}>
			<View style={{ 'flexWrap' : "wrap", 'flexDirection' : "row", 'justifyContent'  : "center", 'flex' : 1 }}>
				{categories.map( c=>
					<Button key={c.id} style={{ 'margin' : 4, 'borderRadius' : 20 }} bordered={value.indexOf( c.key )==-1} onPress={()=>toggle( c.key )}><Text>{c.name}</Text></Button>
				)}
			</View>

			{isInvalid && <Text style={{ 'color' : "#721c24", 'fontSize' : 12, 'textAlign' : "center", 'marginLeft' : 16, 'marginBottom' : 8, 'alignSelf' : "stretch" }}>{error}</Text>}
		</View>
	);
}

/**
 * The actual form
 */
function JustTheStory( { navigation, route } ) {
	// Get the recoil statey
	const [ ask, setAsk ] = useRecoilState( atoms.Ask );
	const haveAskCategories = useRecoilValue( atoms.HaveAskCategories );
	const [ askCategories, setAskCategories ] = useRecoilState( atoms.AskCategories );

	// Are we loading categories?
	const [ isLoadingCategories, setIsLoadingCategories ] = useState( false );

	debug( "JustTheStory route=", route );

	/**
	 * When we submitted
	 */
	const onSubmit = async values=>{
		try {
			debug( "Values", values );

			// Add the params we got and the values and go to the next
			navigation.navigate( "TellYourStory2", { ...route.params, ...values } );
		}
		catch( e ) {
			debug( "Got error", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	};

	/**
	 * Load categories
	 */
	const loadCategories = async ()=>{
		debug( "We are loading categories." );

		// We are loading
		setIsLoadingCategories( true );

		try {
			// Get the new categories
			const c = await fetchAskCategories();

			// Set it
			setAskCategories( c );
		}
		catch( e ) {
			debug( "Got error", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}

		// We aren't loading
		setIsLoadingCategories( false );
	};

	/**
	 * On mount
	 */
	useEffect( ()=>{
		if( !haveAskCategories )
			loadCategories();
	}, [] );

	// Are we loading?
	if( isLoadingCategories )
		return( <LoadingScreen /> );

	// Our initial valuer
	let initialValues;

	if( ask ) {
		// Get only some values
		initialValues = _.pick( ask, "story", "categoryKeys" );
	}
	else {
		// Set ours
		initialValues = { 'story' : "", 'categoryKeys' : [] };
	}

	return(
		<Container style={{ 'padding' : 8 }}>
			<Content>

    <Formik
      validationSchema={storySchema}
      onSubmit={onSubmit}
      initialValues={initialValues}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting,
	setFieldValue
      }) => (

      <Form onSubmit={handleSubmit}>
					<H2 style={{ 'marginBottom' : 16 }}>A little bit of background</H2>
					<Text style={{ 'marginBottom' : 8 }}>Which one of these best describes your situation. You can select more than one.</Text>

					<CategoriesControl
						errors={errors}
						touched={touched}
						setFieldValue={setFieldValue}
						name="categoryKeys"
						values={values}
						style={{ 'paddingTop' : 8 }}
						categories={askCategories}
					/>
						
					<H2 style={{ 'marginBottom' : 16 }}>A little bit more about you</H2>
					<Text style={{ 'marginBottom' : 8 }}>A quick message to tell people a little bit more.</Text>

					<TextareaHelper
						textareaProps={{ 'rowSpan' : 5, 'bordered' : true, 'placeholder' : "A little more." }}
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						name="story"
						values={values}
						style={{ 'paddingTop' : 8 }}
					/>
					<Text style={{ 'marginBottom' : 16, 'fontSize' : 12 }}>{values.story.trim().length}/240</Text>

				<CustomButton
					style={{ 'marginTop' : 16 }}
					isLoading={isSubmitting}
					onPress={handleSubmit}
					block
				>
					<Text>Next</Text>
				</CustomButton>
		</Form>

      )}
    </Formik>

	</Content>
</Container>
	);
}

/**
 * Optioneer
 */
function OnBehalfControl( { values, name, style, errors, setFieldValue, categories, touched } ) {
	// Shorthandies
	const error = errors[ name ];
	const value = values[ name ]||[];
	touched = touched[ name ];
	const isInvalid = error&&touched;

	const set = v=>setFieldValue( name, v );

	return(
		<View style={style}>
			<View style={{ 'flex' : 1, 'flexDirection' : "row" }}>
				<Button style={{ 'margin' : 4, 'borderRadius' : 20, 'flex' : 1, 'justifyContent' : "center" }} bordered={value==true} onPress={()=>set( false )}><Text>Myself</Text></Button>
				<Button style={{ 'margin' : 4, 'borderRadius' : 20, 'flex' : 1, 'justifyContent' : "center" }} bordered={value==false} onPress={()=>set( true )}><Text>Someone Else</Text></Button>
			</View>

			{isInvalid && <Text style={{ 'color' : "#721c24", 'fontSize' : 12, 'textAlign' : "center", 'marginLeft' : 16, 'marginBottom' : 8, 'alignSelf' : "stretch" }}>{error}</Text>}
		</View>
	);
}

/**
 * The actual form
 */
function TheRest( { navigation, route } ) {
	// Get the recoil statey
	const [ ask, setAsk ] = useRecoilState( atoms.Ask );
	// Did we have an error saving the application?
	const [ error, setError ] = useState();
	// Our carts
	const [ carts, setCarts ] = useState( [] );
	// Are our carts loading
	const [ cartsLoading, setCartsLoading ] = useState( false );
	// Should we show the dialog on more info?
	const [ isDialogVisible, setIsDialogVisible ] = useState( false );

	const [ now, setNow ] = useState( new Date() );
	const [ formSchema, minDate, maxDate ] = useAskFormSchema( Boolean( ask ), now );

	debug( "TheRest route=", route );

	const datePickerRef = useRef();

	//debug( "AskForm", formSchema, minDate, maxDate );

	// Load the chopping carts
	const loadCarts = async ()=>{
		try {
			// Load
			setCartsLoading( true );

			// Load my kerts
			const { carts } = await request.get( "cart/user/me", {
				'filter' : {
					'completed' : false, // We only wants carts not completed
					'hasAsk' : false // We only want carts that don't have an ask
				}
			} );

			debug( "Loaded carts", carts );

			// Now set them
			setCarts( carts );

			// No more loading
			setCartsLoading( false );
		}
		catch( e ) {
			debug( "Error loading carts", e );
		}
	};

	// When we mount
	useEffect( ()=>{
		// Do we not have an ask?
		if( !ask )
			loadCarts();
	}, [ ask ] );

	/**
	 * When we submitted
	 */
	const onSubmit = async values=>{
		try {
			// Make sure to add the story itself from the previous screen
			values = { ...values, ..._.pick( route.params, "story", "categoryKeys" ) };

			debug( "Values", values );

			// Create a cookie request
			const cookieRequest = createCookieRequest();

			// Are we not modifying?
			if( !ask ) {
				// Do one minute in the future so we don't get an error from the endpoint that checks
				// to see if we are within minimum time
				const nowPlusTime = addMinutes( new Date(), 1 );
				// Now so we can get the time
				const [ , currentTime ] = nowPlusTime.toISOString().split( 'T' );
				const [ date ] = values.neededBy.toISOString().split( 'T' );

				// Now current time should be the ISO Date time
				// values.neededBy should be the ISO Date date
				// Combining them with a T in the middle should get the date selected with the exact time
				
				const s = date+'T'+currentTime;

				debug( "Needed by string", s );

				// Let's set this properly
				values.neededBy = new Date( s );
			}

			debug( "Values are", values );

			// Post the application
			const result = await cookieRequest.postCaptcha( "ask", `ask/${ask ? "modify" : "add"}/me`, values );

			// Now success
			//onSuccess instanceof Function && onSuccess( response.ask );

			// Let's toast
			Toast.show( {
				'text' : ask ? "Your story was successfully updated!" : "Congratulations! Your story is now live!",
				'buttonText' : "Dismiss",
				'type' : "success",
				'duration' : 0
			} );

			// Set the ask on recoil
			setAsk( result.ask );

			// Go back to the editing page
			navigation.navigate( "TellYourStory1" );

			// Get out of here
			navigation.navigate( "Home" );
		}
		catch( e ) {
			debug( "Got error", e );

			Toast.show( {
				'text' : e.message||e,
				'buttonText' : "Dismiss",
				'type' : "danger",
				'duration' : 0
			} );
		}
	};

	// Our initial valuer
	let initialValues;

	if( ask ) {
		// Get only some values
		initialValues = _.pick( ask, "anonymous", "onBehalf" );

		// FOrmat this propertly
		// This shouldn't be visible on editing
		//initialValues.neededBy = formatISO( new Date( ask.neededBy ), { 'representation' : "date" } )

		// Make this into a date for the pickker
		initialValues.neededBy = new Date( ask.neededBy );

		debug( "We have initial values", initialValues );
	}
	else {
		// Set ours
		initialValues = { 'anonymous' : false, 'neededBy' : minDate, 'cartID' : route&&route.params&&route.params.cartID||"", 'onBehalf' : false };

		debug( "Setting the initial values.", initialValues );
	}

	/**
	 * Workaround for this stupid control not having a value property
	 */
	/*
	useEffect( ()=>{
		if( datePickerRef.current ) {
			debug( "Setting date." );
			datePickerRef.current.setDate( initialValues.neededBy );
		}
	}, [ datePickerRef ] );
	*/

	debug( "We have an ask", ask );

	return(
		<Container style={{ 'padding' : 8 }}>
			<Content>

    <Formik
      validationSchema={formSchema}
      onSubmit={onSubmit}
      initialValues={initialValues}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
	isSubmitting,
	setFieldValue
      }) => (

      <Form onSubmit={handleSubmit}>
				{ask ?
                  <H2 style={{ 'marginBottom' : 8 }}>Edit your story</H2>
								:
								<>
                  <H2 style={{ 'marginBottom' : 16 }}>Now, let's tell your story.</H2>
								</>}

				{ask ? null :
				<View style={{ 'marginBottom' : 16 }}>
					<Label>When do you need this in?</Label>

					 <View style={{ 'paddingTop' : 8 }}>

					<DatePicker
					  style={{ 'width' : "100%" }}
					  date={values.neededBy} //initial date from state
					  mode="date" //The enum of date, datetime and time
					  placeholder="Select Date"
					  format={momentDateFormat}
					  minDate={minDate}
					  maxDate={maxDate}
					  confirmBtnText="Confirm"
					  cancelBtnText="Cancel"
					  customStyles={{
					    dateIcon: {
					      //display: 'none',
					      position: 'absolute',
					      left: 0,
					      top: 4,
					      marginLeft: 0,
					    },
					    dateInput: {
					      marginLeft: 36,
					    },
					  }}
					  onDateChange={v=>{
					  	// To date
						v = moment( v, momentDateFormat );

						// Log
					  	debug( "Date is changed to", v );

						// Set
					  	setFieldValue( "neededBy", v );
						}}
					/>

					 {/*
				         <DatePicker
            defaultDate={values.neededBy}
	    ref={datePickerRef}
            minimumDate={minDate}
            maximumDate={maxDate}

            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select date"
            textStyle={ errors.neededBy ? styles.datePickerFailure : styles.datePickerSuccess }
            placeHolderTextStyle={[ { color: "#d3d3d3",  }, styles[ ( errors.neededBy ? "failure" : "success" )+"BottomBorder" ] ]}
            onDateChange={v=>setFieldValue( "neededBy", v )}
            disabled={false}

	    formatChosenDate={d=>formatReadable( d, "PP" )}
            />
	    			*/}
	    		</View>

		    <Button block transparent onPress={()=>setIsDialogVisible( true )}><Text>Why do we ask this question?</Text></Button>
	    </View>}

      <View style={styles.marginBottom}>
	      <Label style={{ 'marginBottom' : 8 }}>I'm doing this on behalf of</Label>

	    <OnBehalfControl
	      values={values}
	      touched={touched}
	      setFieldValue={setFieldValue}
	      errors={errors}
	      name="onBehalf"
	    />
      </View>

      <View style={styles.marginBottom}>
	      <Label style={{ 'marginBottom' : 8 }}>Anonymous</Label>

	      <View style={{ 'flexDirection' : 'row', 'alignItems' : "center" }}>
		      <Switch
		      	style={{ 'marginRight' : 8 }}
			onValueChange={v=>setFieldValue( "anonymous", v )}
			value={values.anonymous}
			onBlur={handleBlur( "anonymous" )}
		      /><Text style={{ flexShrink: 1 }} onPress={()=>setFieldValue( "anonymous", !values.anonymous )}>I would like my username shown and not my real name</Text>
	      </View>
      </View>

				{/* Other bother with cards if we're creating */}
				{ask ?
					<View style={{ 'marginBottom' : 16 }}>
					<Label>Your selected shopping cart</Label>
								{ask.cart ?
									<Text style={{ 'color' : "teal", 'marginTop' : 8 }}>{ask.cart.name||formatDistanceToNow( new Date( ask.cart.createdAt ), { 'addSuffix' : true } )} - {getStoreName( ask.cart )}</Text>
									: null}
					</View>
				:
					<View style={{ 'marginBottom' : 16 }}>
					<Label>Select your shopping cart</Label>
					<PickerHelper
						pickerProps={{ 'placeholder' : "Select Your Cart" }}
						values={values}
						touched={touched}
						setFieldValue={setFieldValue}
						errors={errors}
						name="cartID"
					>
							{
								carts.map( cart=><Picker.Item key={cart.id} value={cart.id} label={`${cart.name||formatDistanceToNow( new Date( cart.createdAt ), { 'addSuffix' : true } )} - ${getStoreName( cart )}`} /> )
							}
					</PickerHelper>


						{cartsLoading ? <Spinner />
						 : null}
					</View>
				}

				<CustomButton
					style={{ 'marginTop' : 16 }}
					isLoading={isSubmitting}
					onPress={handleSubmit}
					block
				>
					<Text>{ask ? "Update" : "Complete"}</Text>
				</CustomButton>
		</Form>

      )}
    </Formik>

			 <Dialog
			 dialogStyle={{ 'margin' : 16 }}
					visible={isDialogVisible}
					    dialogTitle={<DialogTitle title="Tell people how quickly you need this by." />}
					footer={
						<DialogFooter>
							<DialogButton
								text="Okay"
								onPress={()=>setIsDialogVisible( false )}
							/>
						</DialogFooter>
					}
				>
					<DialogContent style={{ 'padding' : 16 }}>
						<Text style={{ 'fontSize' : 16, 'paddingBottom' : 8 }}>This helps people know how quickly your cart needs to be fulfilled. Also, it helps you get your funds sooner.</Text>
						<Text style={{ 'fontSize' : 16 }}>A virtual card with your funds is issued as soon as your cart is fully funded. However, if your cart is not fully funded and the date you put here passes, you will be manually able to get your virtual card issued.</Text>
					</DialogContent>
				</Dialog>
    	
	</Content>
</Container>
	);
}

/**
 * Navigate through our stuff too
 */
export default function( { navigation, route } ) {

	debug( "Got initial params", route );

	return(
		<Stack.Navigator {...defaultNavigationOptions}>
			<Stack.Screen name="TellYourStory1"
				options={{ 'title' : "Your Story" }}
				component={JustTheStory}
				initialParams={route.params}
				/>
			{/* Can we add this? */}
			<Stack.Screen name="TellYourStory2"
				options={{ 'title' : "Your Story" }}
				component={TheRest}
				/>
		</Stack.Navigator>
	);
}
