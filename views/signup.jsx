import * as React from 'react';
import {  Container, Content, Button, Form, Text, Label, Item, Input  } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


export default function SignupScreen({ navigation }) {
  return (
    <Container>
       <Header/>
        <Content>

        <Form>
            <Item fixedLabel>
              <Label>Full name</Label>
              <Input />
            </Item>
            <Item fixedLabel last>
              <Label>User name</Label>
              <Input />
            </Item>
            <Item fixedLabel>
              <Label>Email</Label>
              <Input />
            </Item>
            <Item fixedLabel last>
              <Label>Password</Label>
              <Input />
            </Item>
          
  

    <Button block  onPress={() => navigation.navigate('Details')}>
            <Text>Sign up</Text>
          </Button>
          </Form>

    </Content>
  </Container>
   
  
  );
}
