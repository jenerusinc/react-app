import React from 'react';

import { Button, Spinner, Text } from 'native-base';

/**
 * Our custom button
 */
export default function( { isLoading, disabled, children, loadingText, ...others } ) {
	let myChilds;

	// Use loading childs
	if( isLoading ) {
		// Let's do an array
		myChilds = [
			<Spinner key="spinner" />
		];

		// Now text
		if( loadingText )
			myChilds.push( <Text key="text">{loadingText}</Text> );
	}
	else
		myChilds = children; // Use the user defined

	return(

			<Button
				{...others}
				disabled={isLoading||disabled}
			>
				{myChilds}
			</Button>
	);
}
