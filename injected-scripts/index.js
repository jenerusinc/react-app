import $ from 'jquery';

import messenger from './messenger';

import engine from './engine';

const debug = require( '../debug' )( "index" );

// Only run on page loadies
$( document ).ready( ()=>{
	debug( "Ready" );

	(async function(){
		// Send a noop
		const r = await messenger
		.command( "noop" )
		.sendPromise();

		console.log( "We had this response.", r );
	})();

	// Run the engine
	const runner = engine.start();

	// Hook and use the runner
	if( runner ) {
		runner
		.on( "onPaymentPageVisibilityChange", o=>messenger.command( "paymentPageVisibilityChange" ).merge( o ).send() )
		.on( "onCartVisibilityChange", visible=>messenger.command( "cartVisibilityChange" ).merge( { visible } ).send() );
	}
} );
