import Base from './base';

const debug = require( '../../debug' )( 'engine:js.stripe.com' );

import { formatExpiresDate } from '../../util';

import messenger from '../messenger';

import setSelectorValue from '../setSelectorValue';


/**
 * Our walmart grocery Jammie
 */
export default class Engine extends Base {
	/** 
	 * Hook events
	 */
	constructor() {
		super();

		// On the message interpreter
		messenger.on( 'command', this.onCommand );
	}

	/**
	 * Bring me home please
	 */
	onCommand = o=>{
		// Get the command
		switch( o.get( "command" ) ) {
			case 'stripeAutoCompletePayment':
				this.autoCompletePayment( o );
				break;
		}
	};

	/**
	 * Autocomplete all of the payment infers
	 */
	autoCompletePayment( o ) {
		try {
			// Get the info
			const info = o.get( 'issuedCard' );
			const obfuscate = Boolean( o.get( 'obfuscate' ) );

			// Try to set the selector value
			const [ first, last ] = info.name.split( ' ' );

			// Our success response
			const response = { 'success' : true };

			// Set the stripies
			response.number = setSelectorValue( `[autocomplete="cc-number"]`, info.number, { obfuscate } );
			response.expiration = setSelectorValue( `[autocomplete="cc-exp"]`, formatExpiresDate( info.expiration, "MMYY" ), { obfuscate } ); // Also replace any non decimals
			response.cvv = setSelectorValue( `[autocomplete="cc-csc"]`, info.cvv, { obfuscate } );

			// Send our response
			o.merge( response ).send();
		}
		catch( e ) {
			// We've failed
			o.sendFailure( e.message||e );

			debug( "Error autocompleting", e );
		}
	}

	/**
	 * Start us
	 */
	async start() {
		debug( "Start placeholder." );
	}
}
