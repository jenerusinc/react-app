import Base from './base';

const debug = require( '../../debug' )( 'engine:instacart.com' );

import { timeout, priceToFloat } from '../../util';

import messenger from '../messenger';

import setSelectorValue from '../setSelectorValue';

import omit from 'lodash';

/**
 * Our instacart Jammie
 */
export default class Engine extends Base {
	/** 
	 * Hook events
	 */
	constructor() {
		super();

		// On the message interpreter
		messenger.on( 'command', this.onCommand );
	}

	/**
	 * Bring me home please
	 */
	onCommand = o=>{
		// Get the command
		switch( o.get( "command" ) ) {
			case "syncCart":
				// We have a sync cart command
				this.syncCart( o );
				break;
			case "isCartVisible":
				o
				.set( "success", true )
				.set( "isVisible", Boolean( this.cartVisible ) )
				.send();
				break;
			case 'autoCompletePayment':
				this.autoCompletePayment( o );
				break;
		}
	};

	/**
	 * Autocomplete all of the payment infers
	 */
	async autoCompletePayment( o ) {
		try {
			// Get the info
			const info = o.get( 'issuedCard' );
			const obfuscate = Boolean( o.get( 'obfuscate' ) );

			// Relay through stripe
			const p = messenger
				.command( "relay" )
				.set( "relay", "stripeAutoCompletePayment" )
				.set( "issuedCard", info )
				// Send and wait
				.sendPromise();

			// Yes
			debug( "Got response from stripe", response );

			// Create this
			const response = {};

			// Fill these
			response.address1 = setSelectorValue( `[name="address_line_1"]`, info.address1, { obfuscate } );
			response.zip = setSelectorValue( `[name="postal_code"]`, info.zip, { obfuscate } );

			// Wait
			const r = await p;

			// Just want the incoming
			// Don't include the seq 'cause we are going to use the other one
			Object.assign( response, omit( r.getIncoming(), "seq" ) );

			// Autocomplete serxess
			o.merge( response ).send();
		}
		catch( e ) {
			// We've failed
			o.sendFailure( e.message||e );

			debug( "Error autocompleting", e );
		}
	}

	/**
	 * The cart items
	 */
	syncCart( o ) {
		// Our final properties
		const itemProperties = [];

		// Shortcut the cart items
		const $cartItems = this.$shoppingCart.find( `div[aria-label="product"]` );
		//const $cartItems = this.$cartElement.find( `div[data-testid="cart-item-parent"]` );
		
		// Got the store source
		const store_source = this.$shoppingCart.find( "div.rmq-3e89696d" ).find( `div[role="heading"]` ).first().text();
		const store_name = "instacart.com";

		debug( "We found %d cart items", $cartItems.length );

		// Loop through each cart item
		for( const item of $cartItems ) {
			try {
				// The properties
				const properties = {};

				// Make to jquery
				const $item = $( item );

				// Get the elements we need
				const $image = $item.find( "a" ).first().find( "img" ); // This is the best way?
				const $name = $item.find( `div.clamped-name` ).first(); // Well
				const $quantity = $item.find( `button[aria-label^="Quantity"]` ).first();
				const $price = $item.find( `div[aria-label^="Price"]` ).first();
				const $link = $item.find( 'a.rmq-9484430c' ).first();

				// Get all of them
				properties.picture = $image.prop( 'src' );
				properties.name = $name.text();
				properties.total_quantity = $quantity.text();
				properties.total_price = priceToFloat( $price.text() );
				//prperties.link_to_item = $link.attr( 'href' )
				
				// Get the link
				if( $link.length ) {
					// Get the href
					const href = $link.prop( 'href' );

					// Now match 
					const matches = href.match( /[^\?]+/ );

					// Do we have any?
					if( matches )
						properties.link_to_item = matches[ 0 ];
				}

				// Push
				itemProperties.push( properties );
			}
			catch( e ) {
				debug( "Error checking item", e );
			}
		}

		debug( "We got items", itemProperties );

		// We did it! Set all items.
		o
		.set( 'success', true )
		.set( 'cart', {
			'items' : itemProperties,
			store_name,
			store_source
		} )
		.send();
	}

	/**
	 * Get the cart
	 */
	async getCart() {
		// Find the cart
		try {
			// Are we on the checkout page?
			// Our shopping cart
			const $shoppingCart =
				this.$shoppingCart = await Engine.waitForCart( 'div.cart-container' );

			debug( "Found %d cart", $shoppingCart.length );

			// The current visibility
			this.cartVisible = false;

			// The visible element change
			const onShoppingCartAttributesChanged = async ()=>{
				// Wait a little bit
				await timeout( 1000 );

				// Get the CSS visibility
				const visibility = $shoppingCart.css( "visibility" )=="visible";

				// Did we have a visibility change?
				if( this.cartVisible!=visibility ) {
					// Set the new one
					this.cartVisible = visibility;

					debug( "Cart visibility changed.", visibility );

					// Alert about the cart visibility change
					this.emit( "onCartVisibilityChange", visibility );
				}
			};

			// Observe it coming visible and invisible
			const observer = new MutationObserver( onShoppingCartAttributesChanged );

			// Observe the attributes of the shopping cart
			$shoppingCart.each( function() {
				observer.observe( this, {
					'attributes' : true
				} )
			} );
		}
		catch( e ) {
			debug( "Error getting instacart.com cart.", e );
		}
	}

	/**
	 * When the module renderer for the checkout has changed
	 */
	checkoutObserverEvent( $moduleRenderer ) {
		debug( "We've detected a changed in checkout." );

		// Do we have a stripe elementer?
		const checkoutVisible = $( '.checking-out' ).css( "visibility" )=="visible";
		const stripeVisible = $( ".StripeElement" ).length>0;

		debug( "Stripe is visible", stripeVisible );
		debug( "Checkout is visible", checkoutVisible );

		let changed=false;

		// Store a variable on our class
		if( Boolean( this.lastStripeVisible )!=stripeVisible ) {
			debug( "stripe visible change." );

			// SEt the last
			this.lastStripeVisible = stripeVisible;

			changed = true;
		}
		if( Boolean( this.checkoutVisible )!=checkoutVisible ) {
			debug( "checkout visible change." );

			// SEt the last
			this.checkoutVisible = checkoutVisible;

			changed = true;
		}

		if( changed ) {
			// Toggley
			this.emit( "onPaymentPageVisibilityChange", {
				'visible'          : checkoutVisible,
				'canAutoComplete'  : stripeVisible
			} );
		}
	}

	/**
	 * The checkout helper
	 */
	async initializeCheckoutHelper() {
		try {
			// This is the best I got
			// Monitor the whole shebang. Slower but can't do better.
			const $element = await Engine.waitForControl( ".outside-container", 30 );

			debug( "We have received module renderer", $element );

			if( !$element ) {
				debug( "Could not find module renderer for checkout." );
				return;
			}

			// Observe it coming visible and invisible
			//const observer = new MutationObserver( debug );
			const observer = new MutationObserver( ( ...opts )=>this.checkoutObserverEvent( $element, ...opts ) );

			// Now obserer!
			$element.each( function() {
				observer.observe( this, {
					'childList' : true,
					'subtree' : true
				} );
			} );
		}
		catch( e ) {
			debug( "Error initializing the checkout helper.", e );
		}
	}

	/**
	 * Start us
	 */
	async start() {
		try {
			// Get our controls
			await Promise.all( [
				this.getCart(),
				this.initializeCheckoutHelper()
			] );
		}
		catch( e ) {
			debug( "Error starting instacart.com engine.", e );
		}
	}
}
