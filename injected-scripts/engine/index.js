import InstacartCom from './instacart.com';
import GroceryWalmartCom from './grocery.walmart.com';
import JsStripeCom from './js.stripe.com';

const debug = require( '../../debug' )( "engine" );

/**
 * our engine
 */
class Engine {
	/**
	 * Our list of sites
	 */
	static extensions = [
		[ new RegExp( "^https?://(www\\.)?instacart\\.com/.*$", "i" ), InstacartCom ],
		[ new RegExp( "^https?://(www\\.)?walmart\\.com/grocery.*$", "i" ), GroceryWalmartCom ],
		[ new RegExp( "^https?://js.stripe.com/.*$", "i" ), JsStripeCom ],
	];

	/**
	 * Start us
	 */
	start() {
		// Get the URL
		const url = window.location.href;

		debug( "Testing URL", url )

		// Which site do we have?
		for( const [ regexp, Class ] of Engine.extensions ) {
			// Is this the URL?
			if( regexp.test( url ) ) {
				debug( "Found a class.", Class );

				// Start it
				const runner = new Class();

				// Start the runnr
				runner.start();

				// Don't process anymore
				return( runner );
			}
		}

		debug( "Cannot find URL." );
	}
}

export default new Engine();
