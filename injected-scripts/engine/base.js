import $ from 'jquery';

import { MaxShoppingCartWait } from '../../constants';

const debug = require( '../../debug' )( "engine:base" );

import { timeout } from '../../util';

import { EventEmitter } from 'events';


/**
 * Our base class
 */
export default class Base extends EventEmitter {
	/**
	 * Wait for a control. Maybe aptly named?
	 */
	static async waitForControl( selector, loops ) {

		// Wait until we can find out element
		for( let i=0; i<=loops; ++i ) {
			// Hook our elements
			const $control = $( selector );

			// Found it?
			if( $control.length>0 ) {
				debug( "Pass %d found %s.", i, selector );

				// Return it
				return( $control );
			}

			debug( "Pass %d did NOT FIND %s.", i, selector );

			// Wait
			await timeout( 1000 );
		}

		// Nuffin
		return( null );
	}

	/**
	 * Wait until we can find a cart and return the cart
	 * @return The found cart
	 */
	static async waitForCart( selector ) {
		// Just call the other guy
		const cart = await Base.waitForControl( selector, MaxShoppingCartWait );

		// The cart was not found
		if( !cart )
			throw new Error( "Cart not found." );

		// Give it back
		return( cart );
	}
}
