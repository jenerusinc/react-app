import Base from './base';

const debug = require( '../../debug' )( 'engine:grocery.walmart.com' );

import { timeout, priceToFloat, formatExpiresDate } from '../../util';

import messenger from '../messenger';

import setSelectorValue from '../setSelectorValue';

import $ from 'jquery';


/**
 * Our walmart grocery Jammie
 */
export default class Engine extends Base {
	/** 
	 * Hook events
	 */
	constructor() {
		super();

		// On the message interpreter
		messenger.on( 'command', this.onCommand );
	}

	/**
	 * Bring me home please
	 */
	onCommand = o=>{
		// Get the command
		switch( o.get( "command" ) ) {
			case "syncCart":
				// We have a sync cart command
				this.syncCart( o );
				break;
			case "isCartVisible":
				o
				.set( "success", true )
				.set( "isVisible", Boolean( this.overallVisibility ) )
				.send();
				break;
			case 'autoCompletePayment':
				this.autoCompletePayment( o );
				break;
		}
	};

	/**
	 * Autocomplete all of the payment infers
	 */
	autoCompletePayment( o ) {
		try {
			// Get the info
			const info = o.get( 'issuedCard' );
			const obfuscate = Boolean( o.get( 'obfuscate' ) );

			// Try to set the selector value
			const [ first, last ] = info.name.split( ' ' );

			// Set the values as we know them
			setSelectorValue( `input[autocomplete*="given-name"]`, first, { obfuscate } );
			setSelectorValue( `input[autocomplete*="family-name"]`, last, { obfuscate } );

			setSelectorValue( `input[autocomplete*="cc-number"]`, info.number, { obfuscate } );

			// Format and split to get
			const [ month, year ] = formatExpiresDate( info.expiration, "MM YYYY" ).split( ' ' );

			setSelectorValue( `select[autocomplete*="cc-exp-month"]`, month );
			setSelectorValue( `select[autocomplete*="cc-exp-year"]`, year );

			setSelectorValue( `input[autocomplete*="cc-csc"]`, info.cvv, { obfuscate } );

			// Gravity
			setSelectorValue( `input[autocomplete*="address-line1"]`, info.address1, { obfuscate } );
			setSelectorValue( `input[autocomplete*="address-level2"]`, info.city, { obfuscate } );
			setSelectorValue( `input[autocomplete*="postal-code"]`, info.zip, { obfuscate } );
			setSelectorValue( `select[autocomplete*="region"]`, info.state );

			// In case they are confirming a credit card
			setSelectorValue( `input[data-automation-id="input-payment-cvv-code"]`, info.cvv, { obfuscate } );

			// Autocomplete serxess
			o.sendSuccess();
		}
		catch( e ) {
			// We've failed
			o.sendFailure( e.message||e );

			debug( "Error autocompleting", e );
		}
	}

	/**
	 * Get the cart items
	 */
	syncCart( o ) {
		try {
			// Our final properties
			const itemProperties = [];

			// Get the cart element
			const $cartElement = [
				this.$shoppingCart,
				this.$headerContainerShoppingCart
			].find( $e=>$e && $e.length>0 );

			// Do we have an element?
			if( !$cartElement )
				throw new Error( "No cart found." );

			// Shortcut the cart items
			const $cartItems = $cartElement.find( `div[data-automation-id="cartItem"]` );

			// Loop through each cart item
			for( const item of $cartItems ) {
				try {
					// The properties
					const properties = {};

					// Make to jquery
					const $item = $( item );

					// Get the elements we need
					const $image = $item.find( `img[data-automation-id="image"]` );
					const $nameA = $item.find( `a[data-automation-id="name"]` );
					const $name = $nameA.find( "span" ).first(); // The first one only
					const $link = $nameA.first(); // The first one only
					const $quantity = $item.find( `[data-automation-id="quantityDisplay"]` ).first();
					const $price = $item.find( `span[data-automation-id="priceUnit"]` ).first();

					properties.link_to_item = $link.prop( 'href' )

					// Get all of them
					properties.picture = $image.attr( 'src' );

					// Get the link
					if( properties.picture ) {
						// Now match 
						const matches = properties.picture.match( /[^\?]+/ );

						// Do we have any?
						if( matches )
							properties.picture = matches[ 0 ];
					}

					properties.name = $name.text();
					properties.total_quantity = $quantity.val();
					properties.total_price = priceToFloat( $price.text() );

					// Push
					itemProperties.push( properties );
				}
				catch( e ) {
					debug( "Error checking item", e );
				}
			}

			debug( "We got items", itemProperties );

			// We did it! Set all items.
			o
			.set( 'success', true )
			.set( 'cart', {
				'items' : itemProperties,
				'store_name' : "grocery.walmart.com"
			} )
			.send();
		}
		catch( e ) {
			// We've failed
			o.sendFailure( e.message||e );

			debug( "Error syncing grocery.walmart.com", e );
		}
	}

	/**
	 * The visibility has changed
	 */
	visibilityChanged() {
		// Get the new visibility variable
		const visibility = this.headerCartVisible || this.$shoppingCart && this.$shoppingCart.css( "visibility" )=="visible";

		// Is it the same?
		if( this.overallVisibility==visibility ) {
			debug( "Overall visibility did not change." );
			return;
		}

		// Set it
		this.overallVisibility = visibility;

		// Log it
		debug( "Overall cart visibility is", visibility );

		// Alert about the cart visibility change
		this.emit( "onCartVisibilityChange", visibility );
	}

	/**
	 * Check the shopping cart
	 */
	async getPersistentCart() {
		try {
			// Our shopping cart
			const $shoppingCart =
				this.$shoppingCart = await Engine.waitForCart( '#shoppingCart' );

			debug( "We have found %d carts", $shoppingCart.length );
		}
		catch( e ) {
			debug( "Error getting shoppingCart grocery.walmart.com", e );
		}
	}

	/**
	 * Initialize the chopping cart
	 */
	async getCartFromContainer() {
		try {
			// Our shopping cart
			const $headerContainer =
				this.$headerContainer = await Engine.waitForCart( 'header[data-automation-id="global-header-standard"]' );

			debug( "We have found %d header containers", $headerContainer.length );

			// The current visibility
			this.headerCartVisible = false;

			// The visible element change
			const onShoppingCartAttributesChanged = async ()=>{
				// Wait a little bit
				await timeout( 1000 );

				// Now get the cart
				const $shoppingCart =
					this.$headerContainerShoppingCart = $headerContainer.find( '[data-automation-id="cart-flyout"]' );

				//debug( "Shopping attribues changed %d", $shoppingCart.length );

				// Get the CSS visibility
				const visibility = $shoppingCart.css( "visibility" )=="visible";

				// Did we have a visibility change?
				if( this.headerCartVisible!=visibility ) {
					// Set the new one
					this.headerCartVisible = visibility;

					debug( "Cart visibility changed.", visibility );

					// Set a visibility change
					this.visibilityChanged();
				}
			};

			// Observe it coming visible and invisible
			const observer = new MutationObserver( onShoppingCartAttributesChanged );

			// Observe the attributes of the shopping cart
			$headerContainer.each( function() {
				observer.observe( this, {
					'childList' : true,
					'subtree' : true
				} )
			} );
		}
		catch( e ) {
			debug( "Error getting headerContainer grocery.walmart.com", e );
		}
	}

	/**
	 * The checkout helper
	 */
	async initializeCheckoutHelper() {
		try {
			// Monitor changes on this control
			const $element = await Engine.waitForControl( ".js-content", 30 );

			if( !$element ) {
				debug( "Could not find our monitor element." );
				return;
			}

			// Observe it coming visible and invisible
			//const observer = new MutationObserver( debug );
			const observer = new MutationObserver( ( ...opts )=>this.checkoutObserverEvent( $element, ...opts ) );

			// Observe the attributes of the shopping cart
			//const moduleRenderer = $moduleRenderer[ 0 ]; // First onnly


			// Now obserer!
			$element.each( function() {
				observer.observe( this, {
					'childList' : true,
					'subtree' : true
				} );
			} );
		}
		catch( e ) {
			debug( "Error initializing the checkout helper.", e );
		}
	}

	/**
	 * When the module renderer for the checkout has changed
	 */
	checkoutObserverEvent( $element ) {
		debug( "We've detected a changed in checkout." );

		// Do we have a stripe elementer?
		const $editCreditCardWrapper = $( ".edit-credit-card-wrapper" );
		const $editFormWrapper = $( ".edit-form-wrapper" );
		const $inputPaymentCVVCode = $( `input[data-automation-id="input-payment-cvv-code"]` );

		const editCreditCardVisible = $editCreditCardWrapper.css( "visibility" )=="visible";
		const editFormWrapperVisible = $editFormWrapper.css( "visibility" )=="visible";
		const inputPaymentCVVCodeVisible = $inputPaymentCVVCode.css( "visibility" )=="visible";

		debug( "$editCreditCardWrapper css visibility", editCreditCardVisible );
		debug( "$editFormWrapper css visibility", editFormWrapperVisible );
		debug( "$inputPaymentCVVCode css visibility", inputPaymentCVVCodeVisible );

		// Do we have a change?
		let changed=false;

		// Do we have a change in values?
		if( this.editCreditCardVisible!=editCreditCardVisible ) {
			this.editCreditCardVisible = editCreditCardVisible;
			changed = true;
		}
		if( this.editFormWrapperVisible!=editFormWrapperVisible ) {
			this.editFormWrapperVisible = editFormWrapperVisible;
			changed = true;
		}
		if( this.inputPaymentCVVCodeVisible!=inputPaymentCVVCodeVisible ) {
			this.inputPaymentCVVCodeVisible = inputPaymentCVVCodeVisible;
			changed = true;
		}

		// Changed?
		// Emit
		if( changed ) {
			this.emit( "onPaymentPageVisibilityChange", {
				'visible'          : editCreditCardVisible,
				'canAutoComplete'  : editFormWrapperVisible || inputPaymentCVVCodeVisible
			} );
		}
	}

	/**
	 * Start us
	 */
	async start() {
		try {
			// Get our controls
			await Promise.all( [
				this.getPersistentCart(),
				this.getCartFromContainer(),
				this.initializeCheckoutHelper()
			] );
		}
		catch( e ) {
			debug( "Error starting instacart.com engine.", e );
		}
	}
}
