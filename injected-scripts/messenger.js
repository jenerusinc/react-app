import Base from '../messenger';

import $ from 'jquery';

const debug = require( '../debug' )( "messenger" );

/**
 * Our class
 */
class Messenger extends Base {
	/**
	 * C-tor
	 */
	constructor() {
		super();

		// When the document is ready send a command good command
		$(document).ready( ()=>this.command( "documentReady" ).send() );

		// Add some event listeners
		[ document, window ].forEach( c=>c.addEventListener( "message", this.onMessage ) );
	}

	/**
	 * Asswhoopious
	 */
	onMessage = m=>{
		try {
			debug( "Received message", m );

			this.onReceived( m.data );
		}
		catch( e ) {
			debug( "Error relaying data to message subsystem.", e );
		}
	};

	/**
	 * Our send
	 */
	_send( o ) {
		debug( "_send", o );

		try {
			// Needs to be as a JSON string
			window.ReactNativeWebView.postMessage( o );
		}
		catch( e ) {
			debug( "Could not send message", e );
		}
	}
}

// Singleton simpleton
export default new Messenger();
