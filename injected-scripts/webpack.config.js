const path = require( 'path' );
const webpack = require( 'webpack' );
const fs = require( 'fs' );

const { ConcatSource } = require( 'webpack-sources' );

//const WebpackExtensionManifestPlugin = require( 'webpack-extension-manifest-plugin' );

class WrapJSON {
	/**
	 * Cintizzle
	 */
	constructor( opts ) {
		this.debug = opts.debug;
	}

	wrap( compiler, chunks ) {
			for (const chunk of chunks) {
				if (!chunk.rendered) {
					// Skip already rendered (cached) chunks
					// to avoid rebuilding unchanged code.
					continue;
				}

				for (const fileName of chunk.files) {
					//console.log( "Got filename asset", fileName, compiler.assets[ fileName ].source() );

					//console.log( "JSON source", JSON.stringify( { 'code' : compiler.assets[ fileName ].source() } ) );

					const code = compiler.assets[ fileName ].source();

					// Stringify it
					compiler.assets[ fileName ] = new ConcatSource( JSON.stringify( { code } ) );
				}
			}
		}

	/**
	 * Compiler applier
	 */
  apply( compiler ) {
			compiler.hooks.compilation.tap('WrapJSON', (compilation) =>
				compilation.hooks.afterOptimizeChunkAssets.tap('WrapperPlugin', (chunks) => {
					this.wrap( compilation, chunks );
				})
			);
  }
}

function config( env ) {
		//const { debug } = env;
		const debug = env && env.debug;

		/*
		// Get the manifest base
		const manifestBase = JSON.parse( fs.readFileSync( './chrome/manifest.base.json' ) );

		// Grab the settings and settings volatile
		const settings = JSON.parse( fs.readFileSync( './common/settings.json' ) );
		// Settings volatile
		const settingsVolatile = JSON.parse( fs.readFileSync( './common/settings.volatile.json' ) );

		// Update the settings volatile
		settingsVolatile.version.minor++;
		*/

		console.log( "Debug is", debug );

		return( {
			entry : "./injected-scripts/index.js",
			plugins  :[
			new webpack.DefinePlugin({
				'process.env': {
					'DEBUG': JSON.stringify( debug )
				}
			}),
			new WrapJSON( { debug } )
	] ,
			output: {
				'filename' : "script.json",
				'path' : __dirname,
			},
			'devtool' : debug ? "inline-source-map" : undefined,
				resolve: {
					modules: [ 'node_modules', path.join( __dirname, '../node_modules' ) ],
					extensions: [ '.web.tsx', '.web.ts', '.web.jsx', '.web.js', '.ts', '.tsx', '.js', '.jsx', '.json' ],
				},
				module: {
							 rules: [
									{
											test: /.(js|jsx)$/,
											exclude: /node_modules/,
											use: {
												loader: 'babel-loader',
												options: {
												'presets' : [
													[ "@babel/preset-env", { 'modules' : "commonjs", 'useBuiltIns' : "usage", "corejs" : 3 } ]
												],
													'plugins': [
														"@babel/plugin-transform-runtime",
														[ "@babel/plugin-proposal-class-properties", { 'corejs' : 3 } ]
													]
												}
										}
									},
									{
										test: /\.css$/,
										use: [ 'style-loader', 'css-loader' ]
									},
									{
											test: /\.less$/,
											use: [
												"style-loader", // creates style nodes from JS strings
												"css-loader", // translates CSS into CommonJS
												{
													loader: "less-loader", // compiles Less to CSS
													options: {
														lessOptions : {
															javascriptEnabled: true,
															 paths: [path.resolve(__dirname, 'node_modules')]
														 }
													}
											}]
									},

									{
										test: /\.s[ac]ss$/i,
										use: [
											// Creates `style` nodes from JS strings
											'style-loader',
											// Translates CSS into CommonJS
											'css-loader',
											// Compiles Sass to CSS
											'sass-loader',
										],
									},

									{
										test: /\.(png|jpg|gif|woff|woff2|eot|svg|ttf)$/i,
										use: [ {
											loader: 'url-loader',
											options: {
												'limit' : 1024*1024,
												'fallback' : "responsive-loader"
											}
										}]
									}
							 ]
					 }
		} );
}

module.exports = config;
