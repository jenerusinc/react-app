import $ from 'jquery';

const debug = require( '../debug' )( "setSelectorValue" );

//import { timeout } from './util';

export default function( selector, value, opts ) {
	const {
		useJQuery,
		obfuscate
	} = { ...opts };

	debug( "Searching for selector %s", selector );

	// Find it
	const $element = $( selector )
	// Make sure it's visible
	.filter( function() {
			return( $( this ).css( 'visibility' )=="visible" );
	} )
	// Only one
	.first()
	;

	// Do we have any?
	if( $element.length==0 ) {
		debug( "Element with selector %s not found.", selector );
		return( false );
	}

	//debug( "Selector is", $element[ 0 ], "value is", $element.val() );

	// Should we use native or Jquery
	if( !useJQuery ) {
		debug( "Setting using native." );

		// What event are we sending?
		const eventType = $element.is( "input" ) ? "input" : "change";

		debug( "Sending eventType=", eventType );

		// Split out the element
		const element = $element[ 0 ];

		// Now do the chain
		element.focus();

		// The last value
		let lastValue = element.value;
		element.value = value;

		// Create a native event
		//const event = document.createEvent( "HTMLEvents" );

		const event = new Event( 'input', { 'target' : element, 'bubbles' : true } );

    // React 15
    event.simulated = true;
    // React 16
    let tracker = element._valueTracker;
    if( tracker )
        tracker.setValue( lastValue );
		// Initialize it
		//event.initEvent( 'change', true, false );

		// Dispatch the event
		element.dispatchEvent( event );

		// Blue it
		element.blur();
	}
	else {
		debug( "Setting using jquery" );

		// Focus
		$element.focus();

		debug( "Element focused." );

		//await timeout( 1000 );

		// Now set
		$element.val( value );
		// Trigger change
		$element.change(); //.trigger( 'input', { bubbles: true } );

		debug( "Value set and change event." );

		//await timeout( 1000 );

		// Blur
		$element.blur();

		debug( "Blurred" );
	}

	if( obfuscate ) {
		debug( "Obfuscating element." );

		$element.attr( "type", "password" );
	}
	
	return( true );
};
