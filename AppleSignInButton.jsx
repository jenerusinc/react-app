import React, { useEffect, useState } from 'react';

import * as AppleAuthentication from 'expo-apple-authentication';

import { Buffer } from 'buffer';

import { createCookieRequest } from './request';

import variables from './native-base-theme/variables/platform';

import _ from 'lodash';

const debug = require( './debug' )( "AppleSignInButton" );

import { Toast } from 'native-base';

import { setItemAsync, getItemAsync } from 'expo-secure-store';

import { getFullNameFromAppleObject } from './util';
import { getRandomBytesAsync } from 'expo-random';

//import AppleSignInButton from '../AppleSignInButton';

import LoadingPopupModal from './LoadingPopupModal';
import { useOnSuccess, LoginSuccessPopup } from './views/LoginOrRegister';

import { HasAppleAuthentication } from './atoms';
import { useRecoilValue } from 'recoil';

const KeyPrefix = "AppleSignIn.";

/**
 * Quick helper function to make sure that
 * we can merge any new sign in data
 * with old sign in data
 */
function mergeWithHelper( a, b ) {
	debug( "Merging a with b", a, b );

	// Is it already null?
	if( a==null )
		return( b );
	
	// Are we merging objects?
	if( _.isObject( b ) )
		return( _.mergeWith( {}, a, b, mergeWithHelper ) );

	// Don't merge a null
	if( b==null )
		return( a );
	
	// Otherwise return b
	return( b );
}

/**
 * Sign in with the apples
 */
export default function( { navigation, route, style, ...others } ) {
	// Determine if they have apple authentication
	const hasAppleAuthentication = useRecoilValue( HasAppleAuthentication );
	
	/**
	 * Do they have it?
	 */
	if( !hasAppleAuthentication )
		return( null ); // Nonce component

	// Whether or not we are actioning
	const [ isLoading, setIsLoading ] = useState( false );
	
	// On our success
	const [ onSuccess, modalProps ] = useOnSuccess();

	/**
	 * The Apple Sign in Process
	 */
	const onPress = async () => {
		// Loading
		setIsLoading( true );

		try {
			// Get 32 random bytes to string
			let nonce = await getRandomBytesAsync( 32 );

			debug( "Got random bytes", nonce.length );

			nonce = Buffer.from( nonce );

			debug( "Got buffer", nonce );

			nonce = nonce.toString( 'base64' );

			debug( "Got nonce", nonce );

		  const credential = await AppleAuthentication.signInAsync( {
		    requestedScopes: [
		      AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
		      AppleAuthentication.AppleAuthenticationScope.EMAIL,
		    ],
			// Include a nonce. Who cares though???
		  	nonce
		  } );

		  // signed in
		  debug( "Apple gave us credentials", credential );

		  // SPlit the values
		  let {
		  	user,
				identityToken,
				email,
				fullName
			} = credential;

			// Get form the database
			const key = KeyPrefix+user;

			debug( "Trying to find info by key", key );
			
			// Get it
			let previousCredential = await getItemAsync( key );

			// Parse it
			previousCredential = JSON.parse( previousCredential||null );

			debug( "Got previous credential", previousCredential );

			// Now combine the two
			// But don't merge undefineds or nulls
			const actualCredential = _.mergeWith(
				{},
				previousCredential,
				{ email, fullName },
				mergeWithHelper
			);

			debug( "Got actual crdentials", actualCredential );

			// Now save it
			await setItemAsync( key, JSON.stringify( actualCredential ) );

			// Attempt to log in ourselves.
			// Values
			const values = {
				'email' : actualCredential.email,
				'name' : getFullNameFromAppleObject( actualCredential.fullName ),
				nonce,
				identityToken
			};

			debug( "Trying to apple log in with these", values );


			// Try so we don't overall throw
			try {
				// Create a cookie request
				const cookieRequest = createCookieRequest();

				const result = await cookieRequest.post( "auth/apple/loginWithIdToken", values );

				debug( "Got result", result );

				// Call on success
				await onSuccess( result, cookieRequest, navigation, route );
			}
			catch( e ) {
				// Is it not a "need more info" error
				if( e.code!="NEED_MORE_INFO" )
					throw e; // Rethrow and catch below
					
				// Just get more info!
				const params = {
					...( route && route.params || null ),
					'fullName' : actualCredential.fullName,
					'email' : actualCredential.email,
					identityToken,
					user,
					nonce
				};

				// Now navigation!
				navigation.navigate( "LoginOrRegister", {
					'screen' : "AppleRegisterFormScreen",
					params
				} );
			}
		}
		catch( e ) {
		  if( e.code==='ERR_CANCELED' ) {
		    debug( "Sign in flow cancelled." );

			// Pop up
			Toast.show({
				'text': "The login process has been cancelled.",
				'buttonText': "Dismiss",
				'type': "danger",
				'duration': 0
			});
		  }
		  else {
		    debug( "Error", e );

			// Pop up
			Toast.show({
				'text': e.message || e,
				'buttonText': "Dismiss",
				'type': "danger",
				'duration': 0
			});
		  }
		}

		// No loading
		setIsLoading( false );
	};

  return (
  	<>
	    <AppleAuthentication.AppleAuthenticationButton
	      buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
	      buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
	      cornerRadius={variables.borderRadiusBase}
	      style={[ { width: "100%", 'height' : 45 }, style ]}
	      onPress={onPress}
	    />
	    <LoadingPopupModal text="Working..." visible={isLoading} />
	    <LoginSuccessPopup {...modalProps} />
	</>
  );
}
