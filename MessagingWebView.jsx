import React from 'react';

import WebView from 'react-native-webview';
import Messenger from './messenger';

const debug = require( './debug' )( "MessagingWebView" );

import { IsIOS } from './constants';

// Get the useragent here
//const userAgent = IsIOS ? "Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1" : "Mozilla/5.0 (Linux; U; Android 2.2) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
const userAgent = undefined;

debug( "We got the userAgent.", userAgent );

/**
 * Our messaging controller
 */
export default class extends React.Component {
	/**
	 * When we are created
	 */
	constructor( props ) {
		super( props );

		// Our context
		const that = this;

		// Our messenger class
		const M = class extends Messenger {
			/**
			 * When it's time to send
			 */
			_send( o ) {
				// Get the native
				const { native } = that;

				if( native ) {
					native.postMessage( o );
				}
			}
		};

		// Our messnger instance
		this.messenger = new M();

		// Do we have an onCommand?
		const { onCommand } = props;

		if( onCommand instanceof Function ) {
			debug( "Making initial onCommand." );
			this.messenger.on( 'command', onCommand );
		}

		// Process some common commands
		this.messenger.on( 'command', this.onCommonCommand );
	}

	/**
	 * Process common commandings
	 */
	onCommonCommand = o=>{
		// Process
		switch( o.get( "command" ) ) {
			case "noop": // Process a noop
				o.sendSuccess();
				break;
			case "relay": // Relay a command right back
				// Our new commmand
				const c = this
					// Create a new command with undefiend since it'll be overwritten anyway
					.command()
					// Merge the incoming data
					.merge( o.getIncoming() )
					// Now actually set the command
					.set( "command", o.get( "relay" ) )
					// Send it
					.send();
				break;
		}
	};

	/**
	 * We've mountied
	 */
	componentWillUnmount() {
		// Clear this
		this.native = null;
		// Remove all listeners
		this.messenger.removeAllListeners();
		// Shoot the messenger
		this.messenger.destroy();
	}

	/**
	 * Expose the command
	 */
	command = ( ...args )=>this.messenger.command( ...args );

	// Expose our emitter
	on = ( ...args )=>this.messenger.on( ...args );

	/**
	 * on the message
	 */
	onMessage = ( { nativeEvent } )=>{
		// Got a message. Now get the data
		this.messenger.onReceived( nativeEvent.data );
	};

	
	/**
	 * Our custom on load
	 */
	onLoad = ( ...params )=>{
		const { onLoad } = this.props;

		// Send a command
		this.command( "loaded" ).send()

		// Call up
		if( onLoad instanceof Function )
			onLoad( ...params );
	};

	/**
	 * When we've updated
	 */
	componentDidUpdate( prevProps ) {
		// Get the previous
		let { onCommand } = prevProps;

		// Do we have a previous onCommand?
		if( onCommand instanceof Function ) {
			debug( "Removing previous onCommand." );
			this.messenger.removeListener( 'command', onCommand );
		}

		// New one
		( { onCommand } = this.props );

		// Is the new one a new one?
		if( onCommand instanceof Function ) {
			debug( "Using new onCommand." );
			this.messenger.on( 'command', onCommand );
		}
	}

	/**
	 * Now the render
	 */
	render() {
		// our props
		const {
			onCommand,
			onLoad,
			...others
		} = this.props;

		return( <WebView 

				sharedCookiesEnabled={true}
				userAgent={userAgent}

				injectedJavaScriptBeforeContentLoadedForMainFrameOnly={false}
				injectedJavaScriptForMainFrameOnly={false}

				onLoad={this.onLoad}

				{...others}

				ref={r=>this.native=r}
				onMessage={this.onMessage}

			/> );
	}
}
