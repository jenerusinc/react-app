import request from './server';

const debug = require( '../debug' )( "request:common" );

// The active requests
const activeRequests = {};

// Create an active request helper
function createActiveRequest( key, fn ) {
	// Create it
	return( async function( ...params ) {
		// DO we have it?
		let promise = activeRequests[ key ];

		// Do we already have in progress one?
		if( promise ) {
			debug( "We already have an ask promise for", key );
			return( promise ); // Return it
		}

		debug( "Fetching off an ask fetch for", key );

		// Kick off
		promise = activeRequests[ key ] = fn( ...params );

		// Wait for it
		const result = await promise;

		// Now clear it so we can do it again
		delete( activeRequests[ key ] );

		// Return the result
		return( result );
	} );
}

/**
 * Fetch the ask
 */
export const fetchAsk = createActiveRequest( "fetchAsk", ()=>request.get( "ask/user/me" ) );
export const fetchUser = createActiveRequest( "fetchUser", ()=>request.get( "user/me" ) );
export const fetchIssuedCard = createActiveRequest( "fetchIssuedCard", ()=>request.get( "card/active" ) );
export const fetchAskCategories = createActiveRequest( "fetchAskCategories", ()=>request.get( "ask/categories" ) );
