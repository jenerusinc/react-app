/**
 * Thin wrapper about request which is a wraper around axios
 */
import request from './request';

import * as SecureStore from 'expo-secure-store';
import { useRef } from 'react';
import { PersistentKeyStorageKey } from '../constants';

const debug = require( '../debug' )( 'request:server' );

//const debug = require( './debug' )( "request" );

import { Buffer } from 'buffer';

import { merge } from 'lodash';

import URL from 'url';

import { JenerusAPIURL } from '../constants';


// Create ours
const serverRequest = [ 'get', 'post' ].reduce( function( that, method ) {
	// Replace the functions
	const originalFunction = that[ method ].bind( that ); // Make sure to keep the scope

	// Create the function
	that[ method ] = async function( resource, data, extra ) {
		debug( "Entering fetch method" );

		const url = URL.resolve( JenerusAPIURL, resource );

		debug( "making %s to url '%s' v", method, url );

		try {
			// Get the user key from the DB
			const userKey = await SecureStore.getItemAsync( PersistentKeyStorageKey );

			// Merge the headers with t he user key
			if( userKey )
				extra = merge( extra, { 'headers' : { 'Authorization' : `Bearer ${userKey}` } } );
		}
		catch( e ) {
			debug( "Error getting userKey.", e );
		}

		// Call up and just get the data
		( { data } = await originalFunction( url, data, extra ) );

		debug( "Got data from request", data );

		// Now do we have success?
		if( !data.success ) {
			// Do we have a new error?
			const e = new Error( data.message||"Unknown Error" );

			// Include a code if we have
			e.code = data.code;

			// The original response
			e.response = data;

			// Now throw
			throw e;
		}

		// Return the data
		return( data );
	};

	// Recaptcha values
	that[ method+"Captcha" ] = async function( action, resource, data, opts ) {
		// Make a recaptcha query
		const { id, token } = await that.get( "auth/scopeToken/captcha", { action } );

		// This can't be good
		if( !id || !token )
			throw new Error( "No scope token returned in auth request." );

		// Make tolkien
		const XScopeToken = Buffer.from( id+" "+token, "utf8" ).toString( "base64" );

		debug( "Scope token together", id, token, XScopeToken );

		// The opts for our header
		opts = merge( opts, { 'headers' : { 'X-Scope-Token' : XScopeToken } } );

		debug( "Calling up", method, resource, data, opts );

		// Now call up
		return( that[ method ]( resource, data, opts ) );
	};

	// Always return our accumulator
	return( that );
}, request.create() );

export default serverRequest;

/**
 * Just a helper function to create a request that does cookies
 */
export function createCookieRequest( opts ) {
	debug( "Use server request.", serverRequest );

	// Gotta return it
	return( serverRequest.create( { 'withCredentials' : true } ) );
}

/**
 * Cookie request hook that stays with the thing
 */
export function cookieRequestHook( opts ) {
	const r = useRef( createCookieRequest( opts ) );

	debug( "After create" );

	// Return it
	return( r.current );
}

// Cookie version
//export const cookieRequest = serverRequest.create( { 'withCredentials' : true } );
