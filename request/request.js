//jshint esversion:9
//jshint node:true
const axios = require( 'axios' );
const qs = require( 'qs' );
const { size } = require( 'lodash' );

// Set the exports
//const that = module.exports = {};

// Our default axios
const defaultOpts = { 'responseType' : "json" };

const debug = require( '../debug' )( "request:request" );

// This is our base object for clonering
const fetcher =
	
// Export each type
[ 'post', 'get' ]
.reduce( function( result, method ) {
	result[ method ] = async function( url, data, opts ) {
			// Are we get or post?
			if( method=="get" ) {
				if( size( data ) )
					url+= '?'+qs.stringify( data );

					debug( "Requesting URL", url, data, opts );

				return( await this.fetcher.get( url, opts ) );
			}
			else
				return( await this.fetcher.post( url, data, opts ) );
	}; // End function post/get
	
	//return it
	return( result );
}, {} );

// Our create
fetcher.create = function( opts ) {
	// Clone all of our functions except the fetcher which we will overwrite
	
	const f = {
		...this,
		'fetcher' : axios.create( { ...defaultOpts, ...opts } ),
		'createOriginal' : axios.create
	};

	return( f );
};

// Return
export default fetcher.create();
