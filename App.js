import Bugsnag from './bugsnag';

import React, { Suspense } from 'react';
import { AppLoading } from 'expo';

import AppLoader from './AppLoader';
import AppMain from './AppMain';

import { Root, StyleProvider } from 'native-base';

import commonColor from './native-base-theme/variables/jenerus';
import getTheme from './native-base-theme/components';

import { RecoilRoot } from 'recoil';

//import Signin from "./views/signin"
import RegisterForPushes from './RegisterForPushes';
import AnalyticsHelper from './AnalyticsHelper';
import DataLoader from './DataLoader';

import ErrorBoundary from './ErrorBoundary';

import 'react-native-gesture-handler';

/**
 * The main chain
 */
function ActualApp() {
	return(
		<StyleProvider style={getTheme( commonColor )}> 
			{/* Recoil global scope */}
			<RecoilRoot>
				{/* NativeBase root for modals */}
				<Root>
					{/* The app itself */}
					{/* Wrap for Recoil suspense
						* otherwise recoil complains.
						* WARNING: Don't use this as a fallback.
						* If your component uses a recoil atom
						* that causes suspension, wrap your own component.
						* Otherwise you may break the whole app
						*/}
						<ErrorBoundary>
							<Suspense fallback={<AppLoading />}>
								{/* Load certain things on reload */}
								<DataLoader />
								{/* Our push notification listener/handler */}
								<RegisterForPushes />
								{/* For google analytics */}
								<AnalyticsHelper />
								{/* Our actual app */}
								<AppMain />
							</Suspense>
						</ErrorBoundary>
				</Root>
			</RecoilRoot>
		</StyleProvider>
	);
}

/**
 * Anonymous wrapper
 */
export default function() {
	return(
		<AppLoader>
			<ActualApp />
		</AppLoader>
	);
}
