import React from 'react';

import { Grid, Col, View, Spinner, Container, Text } from 'native-base';

/**
 * Simple loading view
 */
export default function( { text } ) {
	return(
		<Container>
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignItems' : "center" }}>
							<Spinner />
							<Text>{text||"Working..."}</Text>
					</View>
				</Col>
			</Grid>
		</Container>
	);
}
