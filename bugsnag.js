import Bugsnag from '@bugsnag/expo';

const debug = require( './debug' )( "bugsnag" );

const config = {
	'releaseStage' : __DEV__ ? "debug" : "production",
	'enabledReleaseStages' : [ "production" ]
};

debug( "Running bugsnag with config.", config );

Bugsnag.start( config );

// Use the default
export default Bugsnag;

// The react plugin
export const reactPlugin = Bugsnag.getPlugin( 'react' );
