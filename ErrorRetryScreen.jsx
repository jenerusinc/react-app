import React from 'react';

import { H1, H4, Text, Grid, Col, View, Icon, Container, Button } from 'native-base';

const debug = require( './debug' )( "ErrorBoundary" );

/**
 * Our stuff
 */
export default function( { error, onRetry } ) {
	return(
		<Container>
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignItems' : "center" }}>
						<H1>Uh oh!</H1>
						<Text>We hit a snag!</Text>
						<Icon type="FontAwesome5" name="sad-tear" style={{ 'color' : "blue" }} />
						<Text style={{ 'color' : "red", 'marginTop' : 8 }}>{error.message||error||"Unknown Error"}</Text>
						{onRetry instanceof Function ?
							<Button style={{ 'alignSelf' : "center", 'marginTop' : 8 }} transparent onPress={onRetry}><Text>Please tap here to try again</Text></Button>
							: null}
					</View>
				</Col>
			</Grid>
		</Container>
	);
}
