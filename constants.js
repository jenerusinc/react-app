import { Platform } from 'react-native';

import * as Yup from 'yup';

import { resolve } from 'url';

const debug = require( './debug' )( "constants" );


export const JenerusWebMessageProp = "IsJenerusWebMessage";
export const JenerusWebMessageValue = true;
export const MessageExpiresAfter = 1000*60*2; //!< 2 mins?
export const MaxShoppingCartWait = 60; //!< 60 seconds

export const JenerusBaseURL = process.env.JENERUS_BASE_URL||"https://jenerus.com/api/";
export const JenerusAPIURL = resolve( JenerusBaseURL, "/api/" );
export const JenerusAvatarURL = resolve( JenerusBaseURL, "/avatar/" );

export const PersistentKeyStorageKey = "UserPersistentKey";
export const SlideShowSeenKey = "SlideShowSeenKey";

// TODO these need to be taken from server
export const MinimumNeededByDays = 2; //!< How many days minimum an ask can be needed by
export const MaximumNeededByDays = 31; //!< How many days maximum an ask can be needed by

export const MinimumCartAmount = 50; //!< You must be this tall to ride the ride

debug( "JenerusBaseURL=", JenerusBaseURL, "JenerusAPIURL=", JenerusAPIURL );


export const UserNameConfirmRegExp = new RegExp( "^[a-z0-9\.-_]+$", "i" );
export const UserNameMinSize = 3;

export const JenerusAquamarine = "#7FFFD4";
export const JenerusBlue = "#0dceda";

export const IsIOS = Platform.OS=="ios";
export const IsAndroid = Platform.OS=="android";

export const GoogleSignInOpts = {
				'androidClientId' : "199961999369-oaouq3hlf0vlc1kanukjidrnfr84rhn0.apps.googleusercontent.com",
				'iosClientId' : "199961999369-2mlghc4h44j4sb965dcgj7he22ii86h3.apps.googleusercontent.com",
				'iosStandaloneAppClientId' : "199961999369-r3n1sqgj9tlb1fgdgitnbqmp5jgd5hro.apps.googleusercontent.com",
				'androidStandaloneAppClientId' : "199961999369-ljlordcu3eg6pke3n9bndlr7fae1hl4k.apps.googleusercontent.com",
				'scopes' : [ "profile", "email" ]
};

/**
 * Some common schemas
 */
export const FormSchemas = {
  'email'    : Yup.string().email().required( "Please enter an e-mail" ).trim(),
  'password' : Yup.string().required( "Please enter a password" ),
	'name' : Yup.string().required( "Please enter your full name" ).matches( /^[a-zA-Z]+( [a-zA-Z]+)+/, "Please specify at least a family and given name." ).trim(),
	'username' : Yup.string().matches( UserNameConfirmRegExp, "Usernames can only contain letters, numbers, periods, underscores, and hyphens." ).required( "Please select a username" ).trim().min( UserNameMinSize )
};
