import React from 'react';

import { View, Left, Body, Title, Icon, Button, Text, Header, Right } from "native-base";

import { createStackNavigator } from './custom-navigation';

const Stack1 = createStackNavigator();
const Stack2 = createStackNavigator();

function Route1() {
	return(
		<View>
			<Text>Route 1</Text>
		</View>
	);

}

function NumericRoute() {
	return(
		<Stack2.Navigator>
			<Stack2.Screen name="Route1" component={Route1} options={{ 'title' : "Route 1" }} />
		</Stack2.Navigator>
	);
}

function RouteA( { navigation } ) {
	return(
		<View>
			<Button onPress={()=>navigation.navigate( "NumericRoute", { 'screen' : "Route1" } )}><Text>Go Numeric</Text></Button>
		</View>
	);
}

/**
 * Container
 */
export default function() {
	return(
		<Stack1.Navigator>
			<Stack1.Screen name="RouteA" component={RouteA} options={{ 'title' : "Route A" }} />
			<Stack1.Screen name="NumericRoute" component={NumericRoute} options={{ 'headerShown' : false }} />
		</Stack1.Navigator>
	);
}
