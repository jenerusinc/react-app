import React from "react";
import { Body, Header, Left, Right, Icon, Title, Button, Text } from "native-base";

import { useNavigation } from '@react-navigation/native';

const debug = require( './debug' )( "DefaultHeader" );

/**
 * The left button
 */
export function BackButton( props ) {
	return(

	    <Button
	      transparent
	      {...props}
	      >
	      <Icon name="arrow-back" />
	    </Button>
	);
}

/**
 * Our default header moduley
 */
export default function DefaultHeader( { title, left, right, body, back } ) {
	// Get the nav
	const navigation = useNavigation();

	//debug( "We have back?", back );
	
	// By default do the menu
	if( left==undefined ) {
		left = 
		  <Left>
		    {back ? 
		    <BackButton
		      onPress={()=>{
		      	debug( "Going back" );
		      	navigation.goBack();
			}}
		    />
		    :
		    <Button
		      transparent
		      onPress={()=>{
		      	debug( "Opening drawere" );
		      	navigation.openDrawer();
			}}>
		      <Icon name="menu" />
		    </Button>}
		  </Left>;
	}
	else
		left = <Left>{left}</Left>;

	if( right==undefined )
		right = <Right />
	else
		right = <Right>{right}</Right>

	if( body==undefined )
		body = <Body>{title ? <Title>{title}</Title> : null}</Body>;
	else
		body = <Body>{body}</Body>;

	return(
		<Header>
		  {left}
		  {body}
		  {right}
		</Header>
	);
}

export const headerScreenOptions = {
	'header' : ( { scene, previous, navigation } ) => {
	  const { options } = scene.descriptor;

	  //debug( "We have options", options );

	  const title =
	    options.headerTitle !== undefined
	      ? options.headerTitle
	      : options.title !== undefined
	      ? options.title
	      : scene.route.name;

	  const { headerLeft, headerRight } = options;

	  //debug( "We have previous", previous, headerLeft, headerRight );

	  return (
	    <DefaultHeader
	      title={title}
	      back={previous}
	      left={headerLeft}
	      right={headerRight}
	      style={options.headerStyle}
	    />
	  );
	}
};

export function createHeaderScreenOptions( opts ) {
	return( {
		...headerScreenOptions,
		...opts
	} );
}

export const defaultNavigationOptions = {
	'screenOptions' : headerScreenOptions,
	'headerMode' : "screen"
};
