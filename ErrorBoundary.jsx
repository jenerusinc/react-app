import React from 'react';

import { Card, CardItem, Body, Text, Grid, Col, View, Icon, Container } from 'native-base';

import { reactPlugin } from './bugsnag';

const debug = require( './debug' )( "ErrorBoundary" );

const BugsnagErrorBoundary = reactPlugin.createErrorBoundary( React );

/**
 * The fallback component
 */
function FallbackComponent( { error } ) {
	return(
		<Container>
			<Grid style={{ 'alignItems' : 'center' }}>
				<Col>
					<View style={{ 'alignItems' : "center" }}>
							<Icon type="FontAwesome" name="exclamation-triangle" style={{ 'color' : "red" }} />
							<Text style={{ 'color' : "red" }}>{error&&error.message||error}</Text>
					</View>
				</Col>
			</Grid>
		</Container>
	);
}

/**
 * Wrap the bugsnag nagger
 */
export default function( props ) {
	return(
		<BugsnagErrorBoundary FallbackComponent={FallbackComponent} {...props} />
	);
}
