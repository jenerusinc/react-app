/**
 * Extend common color
 */
import commonColor from './commonColor';

import { merge } from 'lodash';


// Override just what we want
export default merge( commonColor, {
  'titleFontfamily' : "Pier"
} );
