// @flow

import variable from './../variables/platform';

export default (variables /* : * */ = variable) => {
  const h3Theme = {
    color: variables.textColor,
    fontSize: variables.fontSizeH3,
    lineHeight: variables.lineHeightH3,
		// Added the custom font family for Jenerus
		'fontFamily' : "Pier"
  };

  return h3Theme;
};
