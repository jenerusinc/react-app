import _ from 'lodash';

/**
 * Timeout when we're done
 */
export function timeout( millis ) {
	return( new Promise( r=>setTimeout( r, millis ) ) );
}

/**
 * Clean a phone number
 */
export function cleanPhoneNumber( phoneNumber ) {
	// Grab just the numbers
	phoneNumber = String( phoneNumber ).replace( /\D/g, '' ); // Take out all non-numbers

	// Do we have the country code also?
	if( phoneNumber.length>10 )
		phoneNumber = phoneNumber.substr( phoneNumber.length-10 );
	
	return( phoneNumber );
};


/**
 * Parse a price
 */
export function priceToFloat( price ) {
	// Make it
	if( typeof( price )==="string" )
		return( parseFloat( price.replace( /[^\.\d]/g, "" ) ) );
	return( price );
}

/**
 * Takes the location from geoip and returns a string formatted the way we want for location
 */
export function locationToString( location ) {
	// Set the location
	if( location ) {
		const parts = [];

		if( location.city )
			parts.push( location.city );
		if( location.region )
			parts.push( location.region );

		if( parts.length )
			return( parts.join( ", " ) );
	}

	return( "Location not available" );
};

/**
 * Credit card formatter
 */
export function formatCreditCard( num ) {
   return( String( num ).match( /(\d{1,4})/g ).join( ' ' ) );
}

/**
 * Currency formatter.
 * Change when other languages and currencies are added.
 */
export function formatCurrency( num ) {
   return( '$' + ( Number( num )||0 ).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') );
}

/**
 * Get a full name from the apple name object
 */
export function getFullNameFromAppleObject( fullName ) {
	// Morth the full  name
	if( _.isObject( fullName ) ) {
		// Grab these
		const {
			familyName,
			givenName,
			middleName
		} = fullName;

		// Morph
		// Put them together
		// Make sure they are not empty or null or something
		// Join them all together
		return( [ givenName, middleName, familyName ].filter( Boolean ).join( ' ' ) );
	}

	// Great
	return( null );
}


/**
 * Takes an expires date like 02/2020 and exports it the way that you want
 * Currently only takes MM, YY, and YYYY
 */
export function formatExpiresDate( string, format ) {
	// Now match it
	const [ , month, year ] = string.match( /^(\d{1,2})\/((?:\d{4}|\d{2}))$/ );

	// Our replacers
	const replaces = [
		[ "MM", ()=>_.padStart( month, 2, "0" ) ],
		[ "YYYY", ()=>year.length==4 ? year : "20"+year ],
		[ "YY", ()=>year.substr( -2 ) ]
	];

	// Replace and return
	for( const [ subject, fn ] of replaces ) {
		//console.log( "Replacing", subject, "with", fn );
		format = format.replace( new RegExp( subject, "g" ), fn );
	}
	
	return( format );
}
