import _ from 'lodash';

import { JenerusWebMessageProp, JenerusWebMessageValue, MessageExpiresAfter } from './constants';

import { EventEmitter } from 'events';

import addMilliseconds from 'date-fns/addMilliseconds';
import isAfter from 'date-fns/isAfter';

const debug = require( './debug' )( "messenger" );

/**
 * Our message class
 */
class Message {
	/**
	 * Go secular
	 */
	constructor( messenger, incoming ) {
		this.messenger = messenger;
		this.incoming = incoming
	}

	/**
	 * The data we are sending
	 */
	outgoing = {};

	/**
	 * Set the command
	 */
	command( command ) {
		// Set the command
		this.outgoing.command = command;

		return( this );
	}

	/**
	 * The incoming
	 */
	getIncoming() {
		return( this.incoming );
	}

	/**
	 * Get from the incoming
	 */
	get( key, defaultValue ) {
		return( _.get( this.incoming, key, defaultValue ) );
	}

	/**
	 * Get the data
	 */
	getOutgoing() {
		return( outgoing );
	}


	/**
	 * The setter
	 */
	set( key, value ) {
		_.set( this.outgoing, key, value );
		
		return( this );
	}

	/**
	 * Assign the values
	 */
	merge( object ) {
		_.merge( this.outgoing, object );

		return( this );
	}

	/**
	 * Sender
	 */
	send( o, promise ) {
		// Send to the wrapper
		this.messenger.send( o||this.outgoing, this.get( "seq" ), promise );

		return( this );
	}

	/**
	 * Send with a promise
	 */
	sendPromise( o ) {
		// We want to wait for it
		return( new Promise( ( resolve, reject )=>{
			// Do the same but save the promise
			this.send( o, [ resolve, reject ] );
		} ) );
	}

	/**
	 * Send a message
	 */
	sendMessage( success, message ) {
		const o = { success };

		if( message!=null )
			o.message = message;

		this.send( o );

		return( this );
	}

	/**
	 * Success/
	 */
	sendSuccess( message ) {
		this.sendMessage( true, message );

		return( this );
	}

	/**
	 * Not success
	 */
	sendFailure( message ) {
		this.sendMessage( false, message );
		
		return( this );
	}
}

/**
 * Our message wrapper
 */
export default class Messenger extends EventEmitter {
	/**
	 * Our promise list
	 */
	promises = {};

	/**
	 * Sequence
	 */
	seq = 1;

	/**
	 * Our timer event
	 */
	timerEvent = ()=>{
		// Now date
		const now = new Date();

		// Each it
		this.promises = _.reduce( this.promises, ( result, value, seq )=>{
			// Expand
			const [ expires, resolve, reject ] = value;

			// Text
			if( isAfter( now, expires ) ) {
				// Expired
				setImmediate( ()=>reject( new Error( "Timeout" ) ) );
				// Don't put back
			}
			else // Not expired
				result[ seq ] = value;

			// NEver foret to retrnit
			return( result );
		}, {} );
	};

	/**
	 * Our timer
	 */
	timer = setInterval( this.timerEvent, 60000 );

	/**
	 * New command
	 */
	command( c ) {
		// Create class
		const m = new Message( this );

		// Set command
		m.command( c );

		// Return
		return( m );
	}

	/**
	 * When we've received
	 */
	onReceived( o ) {
		debug( "onReceived", o );

		// Is it a string?
		if( !typeof( o )==="string" )
			return; // Not a string
		
		// Unjson
		try {
			o = JSON.parse( o );
		}
		catch( e ) {
			// TODO debug?
			return;
		}

		// Is this our message?
		if( o[ JenerusWebMessageProp ]!==JenerusWebMessageValue )
			return; // Not our message

		debug( "We got this object", o );

		// Shrothand
		const { seq, command } = o;

		// We have a sequence
		if( command==null ) {
			// Destructure
			const promise = this.promises[ seq ];

			if( !(promise instanceof Array) )
				return;

			// See if we have a promise?
			const [ , resolve, reject ] = this.promises[ seq ];

			// Call it next tick
			setImmediate( ()=>{
				// Shorthand
				const { success, message } = o;

				// Are we a success?
				if( success )
					resolve( new Message( this, o ) );
				else
					reject( message||"Unknown Error" );
			} );

			// Now finish cleaning up
			delete( this.promises[ seq ] );
		}
		else { // We have a command
			// Handle it
			this.handleCommand( o );
		}
	}

	/**
	 * Handle the message
	 */
	handleCommand( o ) {
		debug( "This came in", o );

		// Wrap it
		const message = new Message( this, o );

		// Get the command
		if( message.get( "command" )=="noop" )	
			message.sendSuccess(); // We did it!
		else // Let somebody else handle it
			this.emit( 'command', message );
	}

	/**
	 * Destroy us
	 */
	destroy() {
		// Do all clean up stuff here
		// Like get rid of all listeners
		this.removeAllListeners();
		// Kill the timer
		if( this.timer ) {
			clearInterval( this.timer )
			this.timer = null;
		}
	}

	/**
	 * Dummy function
	 */
	send( o, seq, promise ) {
		// Make sure we are a Jenerus web message
		o = { ...o, [ JenerusWebMessageProp ] : JenerusWebMessageValue };

		debug( "Sending from here", o );

		// Mutate the object
		if( seq )
			o.seq = seq;
		else {
			// We are outgoing
			const s = o.seq = this.seq++;

			// Do we have a promise array?
			if( promise instanceof Array && promise.length>=2 )
				this.promises[ s ] = [ addMilliseconds( new Date(), MessageExpiresAfter ), ...promise ];
		}

		// Now call the send
		if( this._send instanceof Function ) {
			// Make sure it's JSON
			o = JSON.stringify( o );

			debug( "Dispatching", o );

			this._send( o );
		}
		else
			throw new Error( "_send function not implemented in subclass." );
	}
}
