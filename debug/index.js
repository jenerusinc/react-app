//jshint esversion:6
/**
 * Simple debugger script
 */

import { shuffle } from 'lodash';

// The colors we fancy
const colors = shuffle( [
  'lightseagreen',
	'lightskyblue',
	'firebrick',
	'darkcyan',
  'forestgreen',
  'goldenrod',
  'dodgerblue',
  'darkorchid',
  'crimson',
	'blueviolet',
	'lightcoral',
	'yellowgreen'
] );

// Debug wagon
let isDebug;
// The next color
let nextColorIndex=0;
// Do we have native
let isNative=false;

// Do we have the DEV variable?
try {
	isNative = isDebug = __DEV__;
}
catch( e ) {
	// Ignore
}

// Do we hae a process environment
try {
	isDebug = isDebug || Boolean( process.env.DEBUG );
}
catch( e ) {
	// Ignore
}

module.exports = function( name ){
	if( !isDebug )
		return( function(){} ); // NoOp
	else {
		// What's the next color index?
		if( nextColorIndex>=colors.length )
			nextColorIndex = 0; // Loop back

		const color = "color: "+colors[ nextColorIndex ];
		let lastDate;

		return( function( string, ...opts )  {
			const now = new Date();
			const diffMS = now-(lastDate||new Date());
			lastDate = now;

			let timeString;

			if( diffMS>60*60*1000 ) // One hour
				timeString = `+${(diffMS/(60*60*1000))|0}h`;
			else if( diffMS>60*1000 ) // One minute
				timeString = `+${(diffMS/(60*1000))|0}m`;
			else if( diffMS>1000 ) // One second
				timeString = `+${(diffMS/1000)|0}s`;
			else
				timeString = `+${diffMS}ms`;

			if( isNative )
				console.log( `${name} ${string}`, ...opts, timeString );
			else
				console.log( `%c${name} %c${string}`, color, "color: inherit", ...opts, timeString );
		} );
	}
};
